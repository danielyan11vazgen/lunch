var abstractModalWindowId = "abstractModalWindow";


function showOrganizationModalWindow() {
    jQuery('#' + abstractModalWindowId).modal('show');
}

function closeOrganizationModalWindow() {
    jQuery('#' + abstractModalWindowId).modal('hide');
}

function disableFormFields(containerId, isDisabled) {
    var tagNames = ["INPUT", "SELECT", "TEXTAREA", "SPAN"];
    var container = document.getElementById(containerId);

    for (var i = 0; i < tagNames.length; i++) {
        var elems = container.getElementsByTagName(tagNames[i]);
        for (var j = 0; j < elems.length; j++) {
            elems[j].disabled = isDisabled;
        }
    }
}