var map;
var markers = [];
var geocoder;

var address;
var postalCode;

function initialize() {

    address = document.getElementById('Address');
    postalCode = document.getElementById('zipCode');

    var mapOptions = {
        zoom: 17,

        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.TOP_LEFT
        },

        center: new google.maps.LatLng(40.181300, 44.514185),
        scrollwheel: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false
    };

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    geocoder = new google.maps.Geocoder();

    var input = (document.getElementById('pac-input'));

    var types = document.getElementById('type-selector');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

    google.maps.event.addListener(map, 'click', function (event) {
        $('#longitude').val(event.latLng.k);
        $('#latitude').val(event.latLng.A);
        addMarker(event.latLng);
    });

    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            return;
        }

        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }

        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        $('#longitude').val(place.geometry.location.k);
        $('#latitude').val(place.geometry.location.A);

        processLocation(place.geometry.location, infowindow, marker);
    });
}

function addMarker(location) {

    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        position: location,
        map: map
    });

    processLocation(location, infowindow, marker);
}

function processLocation(location, infowindow, marker) {

    geocoder.geocode({latLng: location}, function (results, status) {

        if (status == google.maps.GeocoderStatus.OK) {

            clearMarkers();

            if (results[1]) {
                var formattedAddress = results[0].formatted_address;

                getPointInfoFromAddress(results);

                infowindow.setContent(formattedAddress);
                infowindow.open(map, marker);
                markers.push(marker);
            }

        } else {
            markers.push(marker);
        }

    });

}

var Types = {
    sublocality: 'sublocality',
    sublocality_level_1: 'sublocality_level_1',
    locality: 'locality',
    administrative_area_level_1: 'administrative_area_level_1',
    postal_code: 'postal_code'
};

function getPointInfoFromAddress(results) {

    var addressComponents = results[0].address_components;

    address.value = "";

    $('#region option[value=""]').prop('selected', true);
    $('#city option[value=""]').prop('selected', true);
    $('#district option[value=""]').prop('selected', true);

    postalCode.value = "";

    address.value = results[0].formatted_address.split(',')[0];

    addressComponents.forEach(function (o) {
        var type = o.types;

        type.forEach(function (t) {
            switch (t) {
                case Types.sublocality:
                {
                    district.value = o.long_name;
                    $('#district option[value=' + o.long_name + ']').prop('selected', true);
                    break;
                }

                case Types.sublocality_level_1:
                {
                    district.value = o.long_name;
                    $('#district option[value=' + o.long_name + ']').prop('selected', true);
                    break;
                }
                case Types.locality:
                {
                    city.value = o.long_name;
                    $('#city option[value=' + o.long_name + ']').prop('selected', true);
                    break;
                }
                case Types.administrative_area_level_1:
                {
                    $('#region option[value=' + o.long_name + ']').prop('selected', true);
                    break;
                }
                case Types.postal_code:
                {
                    postalCode.value = o.long_name;
                    break;
                }
            }
        });

    });
}

function setAllMap(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

function clearMarkers() {
    setAllMap(null);
}

function showMarkers() {
    setAllMap(map);
}

function deleteMarkers() {
    clearMarkers();
    markers = [];

    address.value = "";

    $('#region option[value=""]').prop('selected', true);
    $('#city option[value=""]').prop('selected', true);
    $('#district option[value=""]').prop('selected', true);

    $('#longitude').val("");
    $('#latitude').val("");

    postalCode.value = "";

    document.getElementById('pac-input').value = "";
}

google.maps.event.addDomListener(window, 'load', initialize);




function addMarkerFromDB(location) {

    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        position: location,
        map: map
    });

    geocoder.geocode({latLng: location}, function (results, status) {

        if (status == google.maps.GeocoderStatus.OK) {

            clearMarkers();

            if (results[1]) {
                var formattedAddress = results[0].formatted_address;

                infowindow.setContent(formattedAddress);
                infowindow.open(map, marker);
                markers.push(marker);
            }

        } else {
            markers.push(marker);
        }

    });
}