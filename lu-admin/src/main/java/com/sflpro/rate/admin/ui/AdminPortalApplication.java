package com.sflpro.rate.admin.ui;

import com.sflpro.rate.admin.ui.pages.AdminBasePage;
import com.sflpro.rate.admin.ui.pages.AdminDashboardPage;
import com.sflpro.rate.admin.ui.pages.organization.AbstractBranchPage;
import com.sflpro.rate.admin.ui.pages.organization.bank.BankBranchPage;
import com.sflpro.rate.admin.ui.pages.organization.bank.BankDetailsPage;
import com.sflpro.rate.admin.ui.pages.organization.bank.BankPage;
import com.sflpro.rate.admin.ui.pages.organization.central.CentralBankPage;
import com.sflpro.rate.admin.ui.pages.organization.credit.CreditBranchPage;
import com.sflpro.rate.admin.ui.pages.organization.credit.CreditDetailsPage;
import com.sflpro.rate.admin.ui.pages.organization.credit.CreditPage;
import com.sflpro.rate.admin.ui.pages.organization.exchange.ExchangeBranchPage;
import com.sflpro.rate.admin.ui.pages.organization.exchange.ExchangeDetailsPage;
import com.sflpro.rate.admin.ui.pages.organization.exchange.ExchangePage;
import com.sflpro.rate.admin.ui.pages.organization.investment.InvestmentBranchPage;
import com.sflpro.rate.admin.ui.pages.organization.investment.InvestmentDetailsPage;
import com.sflpro.rate.admin.ui.pages.organization.investment.InvestmentPage;
import com.sflpro.rate.admin.ui.pages.system.EditSystemTranslationsPage;
import com.sflpro.rate.admin.ui.pages.system.SystemTranslationsPage;
import org.apache.wicket.RuntimeConfigurationType;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.Url;
import org.apache.wicket.request.resource.CssResourceReference;
import org.apache.wicket.request.resource.JavaScriptResourceReference;
import org.apache.wicket.request.resource.UrlResourceReference;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/20/14
 * Time: 1:45 PM
 */
public class AdminPortalApplication extends WebApplication {

    /**
     * JQuery JS
     */
    public static final JavaScriptResourceReference JQUERY_JS_URL = new JavaScriptResourceReference(AdminBasePage.class, "resources/js/jquery-2.0.3.min.js");
    public static final JavaScriptResourceReference JQUERY_TREE_GRID_JS_URL = new JavaScriptResourceReference(AdminBasePage.class, "resources/js/jquery.treegrid.js");
    public static final JavaScriptResourceReference JQUERY_TREE_GRID_FOR_BOOTSTRAP3_JS_URL = new JavaScriptResourceReference(AdminBasePage.class, "resources/js/jquery.treegrid.bootstrap3.js");

    /**
     * JQuery CSS
     */
    public static final CssResourceReference JQUERY_TREE_GRID_CSS_URL = new CssResourceReference(AdminBasePage.class, "resources/css/jquery.treegrid.css");


    /**
     * TWITTER BOOTSTRAP JS
     */
    public static final JavaScriptResourceReference BOOTSTRAP_JS_DATEPICKER_URL = new JavaScriptResourceReference(AdminBasePage.class, "resources/js/bootstrap-datepicker.js");
    public static final JavaScriptResourceReference BOOTSTRAP_JS_URL = new JavaScriptResourceReference(AdminBasePage.class, "resources/js/bootstrap.js");


    /**
     * TWITTER BOOTSTRAP CSS
     */
    public static final CssResourceReference BOOTSTRAP_CSS_URL = new CssResourceReference(AdminBasePage.class, "resources/css/bootstrap.css");
    public static final CssResourceReference BOOTSTRAP_CSS_THEME_URL = new CssResourceReference(AdminBasePage.class, "resources/css/bootstrap-theme.css");
    public static final CssResourceReference BOOTSTRAP_CSS_DATEPICKER3_URL = new CssResourceReference(AdminBasePage.class, "resources/css/datepicker.css");


    /**
     * Jasny Bootstrap for file upload field JS
     */
    public static final JavaScriptResourceReference JASNY_BOOTSTRAP_JS_URL = new JavaScriptResourceReference(AdminBasePage.class, "resources/js/jasny-bootstrap.js");


    /**
     * Jasny Bootstrap for file upload field CSS
     */
    public static final CssResourceReference JASNY_BOOTSTRAP_CSS_URL = new CssResourceReference(AdminBasePage.class, "resources/css/jasny-bootstrap.css");


    /**
     * AddBranch JS
     */

    public static final UrlResourceReference GOOGLE_MAP_API = new UrlResourceReference(Url.parse("https://maps.googleapis.com/maps/api/js?v=3.16&libraries=places"));
    public static final JavaScriptResourceReference BRANCH_JS = new JavaScriptResourceReference(AdminBasePage.class, "resources/js/branch.js");


    /**
     * ADMIN JS
     */
    public static final JavaScriptResourceReference ADMIN_JS = new JavaScriptResourceReference(AdminBasePage.class, "resources/js/admin.js");

    /**
     * ADMIN CSS
     */
    public static final CssResourceReference ADMIN_CSS = new CssResourceReference(AdminBasePage.class, "resources/css/admin.css");


    @Autowired
    private ApplicationContext ctx;

    @Override
    public Class<AdminDashboardPage> getHomePage() {
        return AdminDashboardPage.class;
    }

    @Override
    protected void init() {
        super.init();

        getDebugSettings().setAjaxDebugModeEnabled(false);
        getMarkupSettings().setStripWicketTags(true);

        mountPages();

        if (ctx != null) {
            getComponentInstantiationListeners().add(new SpringComponentInjector(this, ctx, true));
        } else {
            getComponentInstantiationListeners().add(new SpringComponentInjector(this));
        }
    }

    @Override
    public RuntimeConfigurationType getConfigurationType() {

        if (ctx != null) {
            return ctx.getBean(AdminPortalConfiguration.class).getRuntime();
        }

        return RuntimeConfigurationType.DEVELOPMENT;

    }

    private void mountPages() {
        mountPage("/translations", SystemTranslationsPage.class);
        mountPage("/translations/edit/${translationId}", EditSystemTranslationsPage.class);

        mountPage("/organization/banks", BankPage.class);
        mountPage("/organization/banks/details/${organizationId}/${organizationType}/${menuActiveItem}", BankDetailsPage.class);
        mountPage("/organization/banks/details/bank-branch/${organizationId}/${pageType}/${organizationType}/${menuActiveItem}/#{branchId}", BankBranchPage.class);

        mountPage("/organization/central-bank", CentralBankPage.class);

        mountPage("/organization/credit", CreditPage.class);
        mountPage("/organization/credit/details/${organizationId}/${organizationType}/${menuActiveItem}", CreditDetailsPage.class);
        mountPage("/organization/credit/details/bank-branch/${organizationId}/${pageType}/${organizationType}/${menuActiveItem}/#{branchId}", CreditBranchPage.class);

        mountPage("/organization/exchange", ExchangePage.class);
        mountPage("/organization/exchange/details/${organizationId}/${organizationType}/${menuActiveItem}", ExchangeDetailsPage.class);
        mountPage("/organization/exchange/details/bank-branch/${organizationId}/${pageType}/${organizationType}/${menuActiveItem}/#{branchId}", ExchangeBranchPage.class);


        mountPage("/organization/investment", InvestmentPage.class);
        mountPage("/organization/investment/details/${organizationId}/${organizationType}/${menuActiveItem}", InvestmentDetailsPage.class);
        mountPage("/organization/investment/details/bank-branch/${organizationId}/${pageType}/${organizationType}/${menuActiveItem}/#{branchId}", InvestmentBranchPage.class);

    }
}
