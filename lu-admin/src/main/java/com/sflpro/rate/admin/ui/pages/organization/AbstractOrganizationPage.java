package com.sflpro.rate.admin.ui.pages.organization;


import com.sflpro.rate.admin.ui.pages.AdminBasePage;
import com.sflpro.rate.admin.ui.pages.organization.bank.BankDetailsPage;
import com.sflpro.rate.admin.ui.pages.organization.event.BankFilterFieldType;
import com.sflpro.rate.admin.ui.pages.organization.event.FilterChangeEvent;
import com.sflpro.rate.admin.ui.pages.organization.event.FilterFieldType;
import com.sflpro.rate.admin.ui.panels.modal.organization.AbstractModalWindow;
import com.sflpro.rate.admin.ui.panels.modal.organization.event.ModalWindowChangeEvent;
import com.sflpro.rate.admin.ui.panels.modal.organization.type.ModalWindowType;
import com.sflpro.rate.models.datatypes.OrganizationStatus;
import com.sflpro.rate.models.datatypes.OrganizationType;
import com.sflpro.rate.models.dto.Organization;
import com.sflpro.rate.services.organization.OrganizationService;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.ajax.markup.html.navigation.paging.AjaxPagingNavigator;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.markup.html.navigation.paging.IPageable;
import org.apache.wicket.markup.html.navigation.paging.IPagingLabelProvider;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigation;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/26/14
 * Time: 4:07 PM
 */
public abstract class AbstractOrganizationPage extends AdminBasePage {

    @SpringBean
    private OrganizationService organizationService;

    public static final int ITEM_PER_PAGE = 21;

    private AbstractModalWindow bankModalWindow;
    private DataViewContainer dataViewContainer;

    private String pageTitle;
    private OrganizationType organizationType;
    private String menuActiveItem;


    public AbstractOrganizationPage(String pageTitle, String menuActiveItem, String ajaxButtonText, OrganizationType organizationType) {
        super(pageTitle, menuActiveItem);

        this.menuActiveItem = menuActiveItem;
        this.pageTitle = pageTitle;
        this.organizationType = organizationType;

        initOrganizationPage(ajaxButtonText);
    }


    /**
     * Initialising Organization Page
     *
     * @param ajaxButtonText ajax button text
     */
    private void initOrganizationPage(final String ajaxButtonText) {

        final AddOrganizationAjaxLink addOrganizationAjaxLink = new AddOrganizationAjaxLink("addOrganizationAjaxLink") {
            @Override
            protected void onComponentTag(ComponentTag tag) {
                super.onComponentTag(tag);
                tag.put("title", ajaxButtonText);
            }
        };

        addOrganizationAjaxLink.add(new Label("ajaxButtonText", ajaxButtonText));
        add(addOrganizationAjaxLink);

        dataViewContainer = new DataViewContainer("dataViewContainer");
        add(dataViewContainer);

        add(new FiltersContainer("filterContainer", dataViewContainer));

        bankModalWindow = new AbstractModalWindow("bankModalWindow", pageTitle, null, organizationType, new ModalWindowChangeDetector(), ModalWindowType.CREATE);
        add(bankModalWindow);
    }


    /**
     * Add Organization Ajax Link
     */
    private class AddOrganizationAjaxLink extends AjaxLink<String> {

        public AddOrganizationAjaxLink(String id) {
            super(id);
        }

        @Override
        public void onClick(AjaxRequestTarget target) {
            bankModalWindow.updateForm(target, null);
            target.appendJavaScript("showOrganizationModalWindow();");
        }
    }


    /**
     * Filters Container
     */
    private class FiltersContainer extends WebMarkupContainer {

        private TextField<String> nameFilter;
        private TextField<Integer> branchFilter;
        private DropDownChoice<OrganizationStatus> statusFilter;

        public FiltersContainer(String id, FilterChangeEvent event) {
            super(id);

            initFilterContainer(event);
        }


        /**
         * Initialize Filter Container
         */
        private void initFilterContainer(final FilterChangeEvent event) {

            nameFilter = new TextField<>("nameFilter", new Model<String>());
            add(nameFilter);

            branchFilter = new TextField<>("branchFilter", new Model<Integer>());
            add(branchFilter);

            List<OrganizationStatus> organizationStatusList = new ArrayList<>(EnumSet.allOf(OrganizationStatus.class));
            organizationStatusList.remove(OrganizationStatus.PENDING);

            statusFilter = new DropDownChoice<>("statusFilter", new Model<>(OrganizationStatus.ALL), organizationStatusList);
            add(statusFilter);

            nameFilter.add(new AjaxFormComponentUpdatingBehavior("change") {
                @Override
                protected void onUpdate(AjaxRequestTarget target) {
                    event.onChangeFilter(target, nameFilter.getModelObject(), BankFilterFieldType.NAME);
                }
            });

            branchFilter.add(new AjaxFormComponentUpdatingBehavior("change") {
                @Override
                protected void onUpdate(AjaxRequestTarget target) {
                    event.onChangeFilter(target, branchFilter.getModelObject(), BankFilterFieldType.BRANCH);
                }
            });

            statusFilter.add(new AjaxFormComponentUpdatingBehavior("change") {
                @Override
                protected void onUpdate(AjaxRequestTarget target) {
                    event.onChangeFilter(target, statusFilter.getModelObject(), BankFilterFieldType.STATUS);
                }
            });
        }

    }


    /**
     * Data View Container
     */
    private class DataViewContainer extends WebMarkupContainer implements FilterChangeEvent {

        private BankDataView bankDataView;

        private String nameFilterValue = "";
        private Integer branchesFilterValue = -1;
        private OrganizationStatus statusFilterValue = OrganizationStatus.ALL;


        public DataViewContainer(String id) {
            super(id);
            setOutputMarkupId(true);

            initDataViewContainer("", -1, OrganizationStatus.ALL);
        }


        private void initDataViewContainer(String nameFilterValue, Integer branchesFilterValue, OrganizationStatus statusFilterValue) {
            removeAll();

            bankDataView = new BankDataView("bankDataView", new BanksDataProvider(nameFilterValue, branchesFilterValue, statusFilterValue));
            add(bankDataView);

            add(new CustomAjaxPagingNavigator("pagingNavigator", bankDataView));
        }


        @Override
        public void onChangeFilter(AjaxRequestTarget target, Object value, FilterFieldType type) {
            final BankFilterFieldType filterFieldType = (BankFilterFieldType) type;
            switch (filterFieldType) {
                case NAME: {
                    nameFilterValue = value != null ? (String) value : "";
                    break;
                }
                case BRANCH: {
                    branchesFilterValue = value != null ? Integer.valueOf((String) value) : -1;
                    break;
                }
                case STATUS: {
                    statusFilterValue = value != null ? (OrganizationStatus) value : OrganizationStatus.ALL;
                    break;
                }
            }


            initDataViewContainer(nameFilterValue, branchesFilterValue, statusFilterValue);
            target.add(this);
        }


        /**
         * Bank Data View
         */
        private class BankDataView extends DataView<Organization> {

            protected BankDataView(String id, IDataProvider<Organization> dataProvider) {
                super(id, dataProvider);
                setItemsPerPage(ITEM_PER_PAGE);
                setOutputMarkupId(true);
            }

            @Override
            protected void populateItem(Item<Organization> item) {
                final Organization o = item.getModelObject();


                item.add(new Label("serialNumber", ((BanksDataProvider) getDataProvider()).getRowNum() + item.getIndex()));

                item.add(new WebMarkupContainer("logo") {
                    @Override
                    protected void onComponentTag(ComponentTag tag) {
                        super.onComponentTag(tag);

                        if (o.getLogoUrl() != null) {
                            //TODO: fix this dirty work
                            tag.put("src", "/resource/" + (o.getLogoUrl().split("\\.")[0] + "_51x41." + o.getLogoUrl().split("\\.")[1]));
                        }
                    }
                });

                item.add(new Label("name", o.getName()));

                int size;

                if (o.getBranches() != null) {
                    size = o.getBranches().size();
                } else {
                    size = 0;
                }

                item.add(new Label("branchesCount", size));
                item.add(new Label("status", o.getStatus().name()));
                item.add(new Label("created", o.getCreated().toDate()));
                item.add(new AjaxLink<String>("viewBankDetails") {
                    @Override
                    public void onClick(AjaxRequestTarget target) {

                        PageParameters parameters = new PageParameters();
                        parameters.add("organizationId", o.getId());
                        parameters.add("organizationType", organizationType);
                        parameters.add("menuActiveItem", menuActiveItem);

                        onViewDetailsPage(parameters);
                    }
                });
            }
        }


        /**
         * Bank Data Provider
         */
        private class BanksDataProvider extends SortableDataProvider<Organization, Organization> {

            private String nameFilterValue = "";
            private Integer branchesFilterValue = -1;
            private OrganizationStatus statusFilterValue = OrganizationStatus.ALL;

            private long rowNum;

            private BanksDataProvider(String nameFilterValue, Integer branchesFilterValue, OrganizationStatus statusFilterValue) {
                this.nameFilterValue = nameFilterValue;
                this.branchesFilterValue = branchesFilterValue;
                this.statusFilterValue = statusFilterValue;
            }

            @Override
            public Iterator<Organization> iterator(long first, long count) {
                rowNum = first + 1;
                return organizationService.getAllOrganizationsByFilter((int) first, (int) (count), nameFilterValue, branchesFilterValue, statusFilterValue, organizationType);
            }

            @Override
            public long size() {
                return organizationService.count(
                        nameFilterValue != null ? nameFilterValue : "",
                        branchesFilterValue != null ? branchesFilterValue : -1,
                        statusFilterValue,
                        organizationType
                );
            }

            @Override
            public IModel<Organization> model(Organization object) {
                return new Model<>(object);
            }

            public long getRowNum() {
                return rowNum;
            }
        }


        /**
         * Custom Ajax Paging Navigator
         */
        private class CustomAjaxPagingNavigator extends AjaxPagingNavigator {

            public CustomAjaxPagingNavigator(String id, IPageable pageable) {
                super(id, pageable);
                setOutputMarkupId(true);
            }

            @Override
            protected AbstractLink newPagingNavigationIncrementLink(String id, IPageable pageable, int increment) {
                return super.newPagingNavigationIncrementLink(id, pageable, increment);
            }

            @Override
            protected AbstractLink newPagingNavigationLink(String id, IPageable pageable, int pageNumber) {
                return super.newPagingNavigationLink(id, pageable, pageNumber);
            }

            @Override
            protected PagingNavigation newNavigation(String id, IPageable pageable, IPagingLabelProvider labelProvider) {
                return super.newNavigation(id, pageable, labelProvider);
            }

            @Override
            public boolean isVisible() {
                return ((DataView) getPageable()).getItemCount() > ITEM_PER_PAGE;
            }

        }

    }


    /**
     * Modal Window Change Detector
     */
    private class ModalWindowChangeDetector implements ModalWindowChangeEvent {

        @Override
        public void change(AjaxRequestTarget target) {
            dataViewContainer.initDataViewContainer("", -1, OrganizationStatus.ALL);
            target.add(dataViewContainer);
        }
    }


    public abstract void onViewDetailsPage(PageParameters parameters);
}
