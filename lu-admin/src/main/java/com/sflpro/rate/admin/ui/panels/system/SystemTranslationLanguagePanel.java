package com.sflpro.rate.admin.ui.panels.system;

import com.sflpro.rate.admin.ui.dto.TranslationDTO;
import com.sflpro.rate.admin.ui.panels.language.DynamicLanguagePanel;
import com.sflpro.rate.models.datatypes.LanguageKeyType;
import org.apache.wicket.markup.html.form.AbstractTextComponent;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.model.Model;
import wicket.contrib.tinymce.TinyMceBehavior;

/**
 * User: Mher Sargsyan
 * Company: SFL LLC
 * Date: 7/16/2014
 * Time: 2:46 PM
 */
public class SystemTranslationLanguagePanel extends DynamicLanguagePanel {


    public SystemTranslationLanguagePanel(String id, String entityName, Long entityId, String fieldName, LanguageKeyType languageKeyType) {
        super(id, entityName, entityId, fieldName, languageKeyType);
    }

    @Override
    protected AbstractTextComponent getAbstractTextComponent(final TranslationDTO l, final String fieldModel, final String languageCode) {

        final TextArea textArea = new TextArea<>("field", new Model<String>(fieldModel) {
            @Override
            public String getObject() {
                return l.getFieldValue();
            }

            @Override
            public void setObject(String object) {
                l.setFieldValue(object);
            }
        });

        textArea.add(new TinyMceBehavior());

        return textArea;
    }
}
