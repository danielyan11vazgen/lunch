package com.sflpro.rate.admin.ui.pages.organization.credit;

import com.sflpro.rate.admin.ui.pages.organization.AbstractOrganizationDetailsPage;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.request.mapper.parameter.PageParameters;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/14/14
 * Time: 9:24 PM
 */
public class CreditDetailsPage extends AbstractOrganizationDetailsPage {

    public CreditDetailsPage(PageParameters pageParameters) {
        super("Credit Details", pageParameters);
    }

    @Override
    public void onBackLinkClick(AjaxRequestTarget target) {
        setResponsePage(CreditPage.class);
    }

    @Override
    public void onAddBranchLinkClick(PageParameters parameters) {
        setResponsePage(CreditBranchPage.class, parameters);
    }

    @Override
    public void onEditDetailsLinkClick(PageParameters parameters) {
        setResponsePage(CreditBranchPage.class, parameters);
    }

    @Override
    public void onSetRatesLinkClick(AjaxRequestTarget target) {

    }

    @Override
    public void onParsersLinkClick(AjaxRequestTarget target) {

    }
}
