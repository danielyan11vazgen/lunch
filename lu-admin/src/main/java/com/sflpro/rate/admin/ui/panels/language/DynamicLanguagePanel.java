package com.sflpro.rate.admin.ui.panels.language;

import com.sflpro.rate.admin.ui.dto.TranslationDTO;
import com.sflpro.rate.models.datatypes.LanguageKeyType;
import com.sflpro.rate.models.dto.Language;
import com.sflpro.rate.models.dto.LanguageKey;
import com.sflpro.rate.models.dto.LanguageValue;
import com.sflpro.rate.services.translation.TranslationService;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.AbstractTextComponent;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/10/14
 * Time: 4:04 PM
 */
public class DynamicLanguagePanel extends Panel {

    @SpringBean
    private TranslationService translationService;

    private List<TranslationDTO> translationDTOList;

    private String entityName;
    private Long entityId;
    private String fieldName;
    private LanguageKeyType languageKeyType;

    private FieldListViewContainer fieldListViewContainer;

    public DynamicLanguagePanel(String id, String entityName, Long entityId, String fieldName, LanguageKeyType languageKeyType) {
        super(id);

        this.entityName = entityName;
        this.entityId = entityId;
        this.fieldName = fieldName;
        this.languageKeyType = languageKeyType;

        initDynamicLanguagePanel();
    }


    /**
     * Initializing Dynamic Language Panel
     */
    private void initDynamicLanguagePanel() {
        translationDTOList = new ArrayList<>();

        if (entityId != null) {
            final LanguageKey key = translationService.getEntityTranslatableValuesByField(entityName, entityId, fieldName.toLowerCase(), languageKeyType);
            final Set<LanguageValue> languageValues = key.getLanguageValues();

            for (LanguageValue languageValue : languageValues) {
                TranslationDTO dto = new TranslationDTO();
                dto.setLanguageId(languageValue.getLanguage().getId());
                dto.setLanguageCode(languageValue.getLanguage().getCode());
                dto.setFieldValue(languageValue.getValue());

                translationDTOList.add(dto);
            }

            for (Language language : translationService.getAllLanguages()) {

                boolean contains = false;

                for (TranslationDTO translationDTO : translationDTOList) {
                    if (translationDTO.getLanguageId() == language.getId()) {
                        contains = true;
                        break;
                    }
                }

                if (!contains) {
                    TranslationDTO dto = new TranslationDTO();
                    dto.setLanguageId(language.getId());
                    dto.setLanguageCode(language.getCode());

                    translationDTOList.add(dto);
                }
            }

        } else {
            for (Language language : translationService.getAllLanguages()) {
                TranslationDTO dto = new TranslationDTO();
                dto.setLanguageId(language.getId());
                dto.setLanguageCode(language.getCode());

                translationDTOList.add(dto);
            }
        }

        fieldListViewContainer = new FieldListViewContainer("fieldListViewContainer");
        add(fieldListViewContainer);
    }


    public FieldListViewContainer getFieldListViewContainer() {
        return fieldListViewContainer;
    }


    /**
     * Field List View Container
     */
    public class FieldListViewContainer extends WebMarkupContainer {

        private FieldListView fieldListView;

        public FieldListViewContainer(String id) {
            super(id);
            initListViewContainer();
        }


        /**
         * Initializing List View Container
         */
        public void initListViewContainer() {
            removeAll();

            fieldListView = new FieldListView("fieldListView", translationDTOList);
            add(fieldListView);
        }


        /**
         * Field List View
         */
        public class FieldListView extends ListView<TranslationDTO> {

            public FieldListView(String id, List<? extends TranslationDTO> list) {
                super(id, list);
            }

            @Override
            protected void populateItem(final ListItem<TranslationDTO> item) {

                final TranslationDTO l = item.getModelObject();
                final String fieldLbl = fieldName + " (" + l.getLanguageCode() + ")";

                String fieldModel = "";
                if (l.getFieldValue() != null) {
                    fieldModel = l.getFieldValue();
                }

                item.add(new Label("fieldLbl", fieldLbl));

                final AbstractTextComponent textComponent = getAbstractTextComponent(l, fieldModel, l.getLanguageCode());

                textComponent.setRequired(true).setLabel(new Model<>(fieldLbl));

                item.add(textComponent);
            }
        }

        public FieldListView getFieldListView() {
            return fieldListView;
        }
    }

    /**
     * Get text component for translation values input
     *
     * @param l          translation data transfer object
     * @param fieldModel field value for editing mode
     * @return text component
     */
    protected AbstractTextComponent getAbstractTextComponent(final TranslationDTO l, final String fieldModel, final String languageCode) {
        return new TextField<String>("field", new Model<String>(fieldModel) {
            @Override
            public String getObject() {
                return l.getFieldValue();
            }

            @Override
            public void setObject(String object) {
                l.setFieldValue(object);
            }
        }) {
            @Override
            protected void onComponentTag(ComponentTag tag) {
                super.onComponentTag(tag);

                if (languageCode.equals("EN")) {
                    tag.put("id", fieldName);
                }
            }
        };
    }

}
