package com.sflpro.rate.admin.ui.pages.organization.event;

/**
 * User: Vazgen Danielyan
 * Company: Wappsnet LLC
 * Date: 7/21/14
 * Time: 5:18 PM
 */
public enum BranchPageType {
    CREATE, EDIT
}
