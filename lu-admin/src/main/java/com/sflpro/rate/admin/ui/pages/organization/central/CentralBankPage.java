package com.sflpro.rate.admin.ui.pages.organization.central;

import com.sflpro.rate.admin.ui.pages.AdminBasePage;
import com.sflpro.rate.admin.ui.panels.AdminMenuPanel;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/26/14
 * Time: 4:09 PM
 */
public class CentralBankPage extends AdminBasePage {
    public CentralBankPage() {
        super("Add Central Bank", AdminMenuPanel.CENTRAL_BANK_LINK_ID);
    }
}
