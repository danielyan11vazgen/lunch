package com.sflpro.rate.admin.ui.pages.organization.event;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/14/14
 * Time: 8:41 PM
 */
public enum BankFilterFieldType implements FilterFieldType {
    NAME, BRANCH, STATUS
}
