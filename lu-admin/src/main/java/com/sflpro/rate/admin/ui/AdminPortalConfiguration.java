package com.sflpro.rate.admin.ui;

import org.apache.wicket.RuntimeConfigurationType;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/20/14
 * Time: 4:06 PM
 */
public interface AdminPortalConfiguration {
    RuntimeConfigurationType getRuntime();
}
