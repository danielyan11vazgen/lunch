package com.sflpro.rate.admin.ui.pages.organization.investment;

import com.sflpro.rate.admin.ui.pages.organization.AbstractOrganizationPage;
import com.sflpro.rate.admin.ui.panels.AdminMenuPanel;
import com.sflpro.rate.models.datatypes.OrganizationType;
import org.apache.wicket.request.mapper.parameter.PageParameters;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/26/14
 * Time: 4:09 PM
 */
public class InvestmentPage extends AbstractOrganizationPage {

    public InvestmentPage() {
        super("Add Investment Organizations", AdminMenuPanel.INVESTMENT_LINK_ID, "Add Investment Organizations", OrganizationType.INVESTMENT);
    }

    @Override
    public void onViewDetailsPage(PageParameters parameters) {
        setResponsePage(InvestmentDetailsPage.class, parameters);
    }
}
