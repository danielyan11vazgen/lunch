package com.sflpro.rate.admin.ui.panels.modal.organization.event;

import org.apache.wicket.ajax.AjaxRequestTarget;

import java.io.Serializable;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/9/14
 * Time: 11:40 AM
 */
public interface ModalWindowChangeEvent extends Serializable {

    void change(AjaxRequestTarget target);

}
