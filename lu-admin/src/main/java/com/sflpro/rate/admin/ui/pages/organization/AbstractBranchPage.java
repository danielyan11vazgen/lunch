package com.sflpro.rate.admin.ui.pages.organization;

import com.sflpro.rate.admin.ui.AdminPortalApplication;
import com.sflpro.rate.admin.ui.dto.TranslationDTO;
import com.sflpro.rate.admin.ui.dto.WorkingHourCode;
import com.sflpro.rate.admin.ui.pages.AdminBasePage;
import com.sflpro.rate.admin.ui.pages.organization.bank.BankDetailsPage;
import com.sflpro.rate.admin.ui.pages.organization.event.BranchPageType;
import com.sflpro.rate.admin.ui.panels.language.DynamicLanguagePanel;
import com.sflpro.rate.models.datatypes.LanguageKeyType;
import com.sflpro.rate.models.datatypes.OrganizationStatus;
import com.sflpro.rate.models.datatypes.OrganizationType;
import com.sflpro.rate.models.datatypes.ResourceType;
import com.sflpro.rate.models.dto.*;
import com.sflpro.rate.services.branch.BranchService;
import com.sflpro.rate.services.city.CityService;
import com.sflpro.rate.services.district.DistrictService;
import com.sflpro.rate.services.exception.BranchAlreadyExistException;
import com.sflpro.rate.services.exception.WorkingHourExistException;
import com.sflpro.rate.services.organization.OrganizationService;
import com.sflpro.rate.services.region.RegionService;
import com.sflpro.rate.services.resource.ResourceManager;
import com.sflpro.rate.services.translation.TranslationService;
import com.sflpro.rate.services.working_hour.WorkingHourService;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.joda.time.LocalTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/15/14
 * Time: 8:42 PM
 */
public abstract class AbstractBranchPage extends AdminBasePage {

    @SpringBean
    private RegionService regionService;

    @SpringBean
    private CityService cityService;

    @SpringBean
    private DistrictService districtService;

    @SpringBean
    private ResourceManager resourceManager;

    @SpringBean
    private TranslationService translationService;

    @SpringBean
    private OrganizationService organizationService;

    @SpringBean
    private BranchService branchService;

    @SpringBean
    private WorkingHourService workingHourService;


    public final Logger logger = LoggerFactory.getLogger(getClass());

    private Long organizationId;
    private OrganizationType organizationType;
    private Long branchId;
    private BranchPageType pageType;
    private String menuActiveItem;

    private BranchForm branchForm;

    public AbstractBranchPage(PageParameters pageParameters) {
        super("Add Branch", pageParameters.get("menuActiveItem").toString());

        menuActiveItem = pageParameters.get("menuActiveItem").toString();
        organizationId = pageParameters.get("organizationId").toLong();
        organizationType = pageParameters.get("organizationType").toOptional(OrganizationType.class);

        if (pageParameters.get("branchId").toString() != null) {
            branchId = pageParameters.get("branchId").toLong();
        }

        pageType = pageParameters.get("pageType").toOptional(BranchPageType.class);

        initAddBranchPage();
    }


    /**
     * Initializing Add Branch Page
     */
    private void initAddBranchPage() {

        add(new AjaxLink<String>("backToBankDetailsPage") {

            @Override
            public void onClick(AjaxRequestTarget target) {
                PageParameters pageParameters = new PageParameters();
                pageParameters.add("organizationId", organizationId);
                pageParameters.add("organizationType", organizationType);
                pageParameters.add("menuActiveItem", menuActiveItem);

                setResponsePage(BankDetailsPage.class, pageParameters);
            }
        });

        branchForm = new BranchForm("branchForm");
        add(branchForm);
    }


    /**
     * Branch Form
     */
    private class BranchForm extends Form {

        private WebMarkupContainer logoPreview;
        private FileUploadField logoUploadField;

        private Boolean isHeadOffice;
        private CheckBox headOfficeCheckbox;

        private DynamicLanguagePanel branchNameContainer;

        private Region region;
        private List<Region> regionList;
        private DropDownChoice<Region> regionSelect;

        private City city;
        private List<City> cityList;
        private DropDownChoice<City> citySelect;

        private District district;
        private List<District> districtList;
        private DropDownChoice<District> districtSelect;

        private DynamicLanguagePanel branchAddressContainer;


        private String zipCode;
        private TextField<String> zipCodeTextField;

        private String phone1;
        private TextField<String> phone1TextField;

        private String phone2;
        private TextField<String> phone2TextField;

        private String email;
        private TextField<String> emailTextField;

        private Boolean isFullDay;
        private AjaxCheckBox isFullDayCheckbox;

        private ArrayList<WorkingHour> workingHourList;
        private WorkingHoursContainer workingHoursContainer;

        private OrganizationStatus status;
        private List<OrganizationStatus> statusList;
        private DropDownChoice<OrganizationStatus> statusSelect;

        private double longitude;
        private TextField<Double> longitudeTextField;

        private double latitude;
        private TextField<Double> latitudeTextField;

        private FeedbackPanel feedbackPanel;


        public BranchForm(String id) {
            super(id);
            setOutputMarkupId(true);

            switch (pageType) {
                case CREATE: {
                    initDefaultValuesOfModels(null);
                    initBranchForm(null);
                    break;
                }

                case EDIT: {
                    Branch branch = branchService.findBranchById(branchId);

                    initDefaultValuesOfModels(branch);
                    initBranchForm(branch);
                    break;
                }
            }
        }


        /**
         * Initializing default values
         */
        private void initDefaultValuesOfModels(Branch branch) {

            regionList = regionService.findByDeletedIsNull();
            cityList = cityService.findCitiesByDeletedIsNull();
            districtList = districtService.findDistrictByDeletedIsNull();


            statusList = new ArrayList<>(EnumSet.allOf(OrganizationStatus.class));
            statusList.remove(OrganizationStatus.PENDING);
            statusList.remove(OrganizationStatus.ALL);

            if (branch != null) {
                isHeadOffice = branch.getIsHead();
                region = branch.getRegion();
                city = branch.getCity();
                district = branch.getDistrict();
                zipCode = branch.getZipCode();
                phone1 = branch.getPhoneNumber1();
                phone2 = branch.getPhoneNumber2();
                email = branch.getEmail();
                isFullDay = branch.getIsFullDay();
                workingHourList = new ArrayList<>(branch.getWorkingHours());
                status = branch.getStatus();
                longitude = branch.getLongitude();
                latitude = branch.getLatitude();
            } else {
                isHeadOffice = false;
                zipCode = "";
                phone1 = "";
                phone2 = "";
                email = "";
                isFullDay = false;
                workingHourList = new ArrayList<>();
                status = OrganizationStatus.ACTIVE;
                longitude = 0;
                latitude = 0;
            }

            if (workingHourList.size() == 0) {
                for (WorkingHourCode days : WorkingHourCode.values()) {
                    WorkingHour c = new WorkingHour();
                    c.setCode(days);
                    workingHourList.add(c);
                }
            }

            Collections.sort(workingHourList, new Comparator<WorkingHour>() {
                @Override
                public int compare(WorkingHour o1, WorkingHour o2) {
                    return o1.getCode().ordinal() - o2.getCode().ordinal();
                }
            });
        }


        /**
         * Initializing Branch Form
         */
        private void initBranchForm(final Branch branch) {
            removeAll();

            logoPreview = new WebMarkupContainer("logoPreview") {
                @Override
                protected void onComponentTag(ComponentTag tag) {
                    super.onComponentTag(tag);

                    if (branch != null && branch.getLogoUrl() != null) {
                        tag.put("src", "/resource/" + branch.getLogoUrl());
                    } else {
                        tag.put("src", "/static/images/logo-not-available.gif");
                    }
                }
            };
            add(logoPreview);

            logoUploadField = new FileUploadField("fileUploadField");
            add(logoUploadField);

            headOfficeCheckbox = new CheckBox("headOffice", new PropertyModel<Boolean>(this, "isHeadOffice"));
            add(headOfficeCheckbox);

            branchNameContainer = new DynamicLanguagePanel("branchNameContainer", Branch.class.getSimpleName().toLowerCase(), branch != null ? branch.getId() : null, "Name", LanguageKeyType.USER_GENERATED);
            add(branchNameContainer);

            regionSelect = new DropDownChoice<>("region", new PropertyModel<Region>(this, "region"), regionList, new IChoiceRenderer<Region>() {

                @Override
                public Object getDisplayValue(Region region1) {
                    return region1.getName();
                }

                @Override
                public String getIdValue(Region region1, int i) {
                    return region1.getName();
                }
            });
            add(regionSelect);

            citySelect = new DropDownChoice<>("city", new PropertyModel<City>(this, "city"), cityList, new IChoiceRenderer<City>() {

                @Override
                public Object getDisplayValue(City city1) {
                    return city1.getName();
                }

                @Override
                public String getIdValue(City city1, int i) {
                    return city1.getName();
                }
            });
            add(citySelect);

            districtSelect = new DropDownChoice<>("district", new PropertyModel<District>(this, "district"), districtList, new IChoiceRenderer<District>() {

                @Override
                public Object getDisplayValue(District district1) {
                    return district1.getName();
                }

                @Override
                public String getIdValue(District district1, int i) {
                    return district1.getName();
                }
            });
            add(districtSelect);

            branchAddressContainer = new DynamicLanguagePanel("branchAddressContainer", Branch.class.getSimpleName().toLowerCase(), branch != null ? branch.getId() : null, "Address", LanguageKeyType.USER_GENERATED);
            add(branchAddressContainer);

            zipCodeTextField = new TextField<>("zipCode", new PropertyModel<String>(this, "zipCode"));
            add(zipCodeTextField);

            phone1TextField = new TextField<>("phone1", new PropertyModel<String>(this, "phone1"));
            add(phone1TextField);

            phone2TextField = new TextField<>("phone2", new PropertyModel<String>(this, "phone2"));
            add(phone2TextField);

            emailTextField = new TextField<>("email", new PropertyModel<String>(this, "email"));
            add(emailTextField);

            workingHoursContainer = new WorkingHoursContainer("workingHoursPanel", workingHourList);
            add(workingHoursContainer);

            isFullDayCheckbox = new AjaxCheckBox("isFullDay", new PropertyModel<Boolean>(this, "isFullDay")) {

                @Override
                protected void onUpdate(AjaxRequestTarget target) {

                    if (isFullDay) {
                        target.appendJavaScript("$('#" + workingHoursContainer.getMarkupId() + "').css('display', 'none');");
                    } else {
                        target.appendJavaScript("$('#" + workingHoursContainer.getMarkupId() + "').css('display', 'block');");
                    }
                }
            };
            add(isFullDayCheckbox);

            statusSelect = new DropDownChoice<>("status", new PropertyModel<OrganizationStatus>(this, "status"), statusList, new IChoiceRenderer<OrganizationStatus>() {

                @Override
                public Object getDisplayValue(OrganizationStatus object) {
                    return object.name();
                }

                @Override
                public String getIdValue(OrganizationStatus object, int index) {
                    return object.name();
                }
            });
            add(statusSelect);

            longitudeTextField = new TextField<>("longitude", new PropertyModel<Double>(this, "longitude"));
            add(longitudeTextField);

            latitudeTextField = new TextField<>("latitude", new PropertyModel<Double>(this, "latitude"));
            add(latitudeTextField);

            feedbackPanel = new FeedbackPanel("feedbackPanel");
            feedbackPanel.setOutputMarkupId(true);
            add(feedbackPanel);

            add(new AjaxLink<String>("cancelSaveBranchAjaxLink") {

                @Override
                public void onClick(AjaxRequestTarget target) {
                    PageParameters pageParameters = new PageParameters();
                    pageParameters.add("organizationId", organizationId);
                    pageParameters.add("organizationType", organizationType);
                    pageParameters.add("menuActiveItem", menuActiveItem);

                    setResponsePage(BankDetailsPage.class, pageParameters);
                }
            });

            add(new SaveBranchAjaxLink("saveBranchAjaxLink", this));

            StringBuilder b = new StringBuilder();
            if (pageType.equals(BranchPageType.EDIT)) {
                b.append("google.maps.event.addDomListener(window, 'load', function() {");
                b.append("  var latlng = new google.maps.LatLng(").append(longitude).append(", ").append(latitude).append(");");
                b.append("  addMarkerFromDB(latlng);");
                if (isFullDay) {
                    b.append("$('#").append(workingHoursContainer.getMarkupId()).append("').css('display', 'none');");
                }
                b.append("});");
            }

            add(new Label("autoAjaxCaller", b.toString()).setEscapeModelStrings(false));


        }


        /**
         * Working Hours Container
         */
        private class WorkingHoursContainer extends WebMarkupContainer {

            private WorkingHoursListView workingHoursListView;

            public WorkingHoursContainer(String id, List<WorkingHour> workingHourList1) {
                super(id);
                setOutputMarkupId(true);
                add(workingHoursListView = new WorkingHoursListView("workingHoursListView", workingHourList1));
            }

            /**
             * Working Hours List View
             */
            private class WorkingHoursListView extends ListView<WorkingHour> {

                public WorkingHoursListView(String id, List<? extends WorkingHour> list) {
                    super(id, list);
                }

                @Override
                protected void populateItem(final ListItem<WorkingHour> item) {

                    final WorkingHour hour = item.getModelObject();

                    item.add(new CheckBox("day", new Model<Boolean>(hour.isWork() != null ? hour.isWork() : hour.getCode().ordinal() < WorkingHourCode.SATURDAY.ordinal()) {
                        @Override
                        public Boolean getObject() {
                            return hour.isWork();
                        }

                        @Override
                        public void setObject(Boolean object) {
                            hour.setWork(object);
                        }
                    }));

                    item.add(new Label("dayName", hour.getCode().name()));

                    item.add(new TextField<>("startTime", new Model<String>(hour.getStartTime() != null ? hour.getStartTime().toString() : "09:00") {

                        @Override
                        public String getObject() {
                            return hour.getStartTime().toString("HH:mm");
                        }

                        @Override
                        public void setObject(String object) {
                            hour.setStartTime(LocalTime.parse(object));
                        }
                    }));

                    item.add(new TextField<>("endTime", new Model<String>(hour.getEndTime() != null ? hour.getEndTime().toString() : "18:00") {

                        @Override
                        public String getObject() {
                            return hour.getEndTime().toString("HH:mm");
                        }

                        @Override
                        public void setObject(String object) {
                            hour.setEndTime(LocalTime.parse(object));
                        }
                    }));

                }
            }

        }

    }


    /**
     * Save Branch Ajax Link
     */
    private class SaveBranchAjaxLink extends AjaxSubmitLink {

        public SaveBranchAjaxLink(String id, Form<?> form) {
            super(id, form);
        }

        @Override
        protected void onSubmit(AjaxRequestTarget target, Form<?> form) {

            BranchForm submitForm = (BranchForm) form;

            /**
             * Get system default language
             */
            Language systemDefaultLanguage = translationService.getSystemDefaultLanguage();


            /**
             * Save Logo
             */
            FileUpload fileUpload = submitForm.logoUploadField.getFileUpload();
            Resource resource = null;

            if (fileUpload != null) {
                try {
                    File file = resourceManager.createTemporaryFile(fileUpload.getInputStream());
                    resource = resourceManager.finalizeWorkingWithResource(file, ResourceType.LOGO, fileUpload.getClientFileName());
                } catch (IOException e) {
                    logger.error("Unexpected error occurred during saving branch logo! ", e);
                    error(e.getMessage());
                }
            }

            List<TranslationDTO> branchNamesTranslationDTOList = submitForm.branchNameContainer.getFieldListViewContainer().getFieldListView().getModelObject();
            List<TranslationDTO> branchAddressesTranslationDTOList = submitForm.branchAddressContainer.getFieldListViewContainer().getFieldListView().getModelObject();

            String branchName = null;
            for (TranslationDTO translationDTO : branchNamesTranslationDTOList) {
                if (translationDTO.getLanguageCode().equals(systemDefaultLanguage.getCode())) {
                    branchName = translationDTO.getFieldValue();
                    break;
                }
            }

            String branchAddress = null;
            for (TranslationDTO translationDTO : branchAddressesTranslationDTOList) {
                if (translationDTO.getLanguageCode().equals(systemDefaultLanguage.getCode())) {
                    branchAddress = translationDTO.getFieldValue();
                    break;
                }
            }

            /**
             * Save Branch
             */
            Branch branch = new Branch();
            branch.setId(branchId != null ? branchId : 0);
            branch.setLogoUrl(resource != null ? resource.getPath() : null);
            branch.setLogo(resource);
            branch.setIsHead(submitForm.isHeadOffice);
            branch.setRegion(submitForm.region);
            branch.setCity(submitForm.city);
            branch.setDistrict(submitForm.district);
            branch.setZipCode(submitForm.zipCode);
            branch.setPhoneNumber1(submitForm.phone1);
            branch.setPhoneNumber2(submitForm.phone2);
            branch.setEmail(submitForm.email);
            branch.setIsFullDay(submitForm.isFullDay);
            branch.setStatus(submitForm.status);
            branch.setLongitude(submitForm.longitude);
            branch.setLatitude(submitForm.latitude);
            branch.setName(branchName);
            branch.setAddress(branchAddress);
            branch.setOrganization(organizationService.findById(organizationId));

            try {

                switch (pageType) {
                    case CREATE: {
                        branch = branchService.createBranch(branch);
                        break;
                    }

                    case EDIT: {
                        branch = branchService.createOrUpdateBranch(branch);
                        break;
                    }
                }

                if (branch.getIsFullDay()) {
                    workingHourService.deleteWorkingHoursByBranch(branch.getId());
                } else {

                    /**
                     * Save Working Hours
                     */
                    List<WorkingHour> workingHourList = submitForm.workingHoursContainer.workingHoursListView.getModelObject();
                    for (WorkingHour workingHour : workingHourList) {

                        switch (pageType) {
                            case CREATE: {
                                workingHourService.createWorkingHour(workingHour, branch.getId());
                                break;
                            }

                            case EDIT: {
                                workingHourService.createOrUpdateWorkingHour(workingHour, branch.getId());
                                break;
                            }
                        }
                    }
                }
                /**
                 * Save Translations
                 */
                translationService.createOrUpdateTranslation(branchNamesTranslationDTOList, branch.getId(), Branch.class.getSimpleName().toLowerCase(), "name");
                translationService.createOrUpdateTranslation(branchAddressesTranslationDTOList, branch.getId(), Branch.class.getSimpleName().toLowerCase(), "address");


                /**
                 * Redirect to BankDetailsPage
                 */
                PageParameters pageParameters = new PageParameters();
                pageParameters.add("organizationId", organizationId);
                pageParameters.add("organizationType", organizationType);
                pageParameters.add("menuActiveItem", menuActiveItem);

                setResponsePage(BankDetailsPage.class, pageParameters);
            } catch (BranchAlreadyExistException | WorkingHourExistException e) {
                error(e.getMessage());
                target.add(branchForm.feedbackPanel);
            }

        }

        @Override
        protected void onError(AjaxRequestTarget target, Form<?> form) {
            target.add(branchForm.feedbackPanel);
        }

    }


    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);

        response.render(JavaScriptHeaderItem.forReference(AdminPortalApplication.GOOGLE_MAP_API));
        response.render(JavaScriptHeaderItem.forReference(AdminPortalApplication.BRANCH_JS));
    }


    /**
     * Call when clicking on Back Link
     *
     * @param parameters PageParameters
     */
    public abstract void onBackLinkClick(PageParameters parameters);


    /**
     * Call when clicking on Cancel Link
     *
     * @param parameters PageParameters
     */
    public abstract void onCancelLinkClick(PageParameters parameters);


    /**
     * Call when clicking on Save Link
     *
     * @param parameters PageParameters
     */
    public abstract void onSaveLinkClick(PageParameters parameters);
}
