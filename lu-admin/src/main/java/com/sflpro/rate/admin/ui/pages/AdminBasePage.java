package com.sflpro.rate.admin.ui.pages;

import com.sflpro.rate.admin.ui.AdminPortalApplication;
import com.sflpro.rate.admin.ui.panels.AdminMenuPanel;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/20/14
 * Time: 3:58 PM
 */
public abstract class AdminBasePage extends WebPage {

    public AdminBasePage(String pageTitle) {
        setPageTitle(pageTitle);
    }

    public AdminBasePage(String pageTitle, String menuActiveItem) {
        setPageTitle(pageTitle);

        add(new AdminMenuPanel("adminMenuPanel", menuActiveItem));
    }


    @Override
    public void renderHead(IHeaderResponse response) {
        response.render(JavaScriptHeaderItem.forReference(AdminPortalApplication.JQUERY_JS_URL));
        response.render(JavaScriptHeaderItem.forReference(AdminPortalApplication.JQUERY_TREE_GRID_JS_URL));
        response.render(JavaScriptHeaderItem.forReference(AdminPortalApplication.JQUERY_TREE_GRID_FOR_BOOTSTRAP3_JS_URL));
        response.render(JavaScriptHeaderItem.forReference(AdminPortalApplication.BOOTSTRAP_JS_URL));
        response.render(JavaScriptHeaderItem.forReference(AdminPortalApplication.BOOTSTRAP_JS_DATEPICKER_URL));
        response.render(JavaScriptHeaderItem.forReference(AdminPortalApplication.JASNY_BOOTSTRAP_JS_URL));
        response.render(JavaScriptHeaderItem.forReference(AdminPortalApplication.ADMIN_JS));

        response.render(CssHeaderItem.forReference(AdminPortalApplication.JQUERY_TREE_GRID_CSS_URL));
        response.render(CssHeaderItem.forReference(AdminPortalApplication.BOOTSTRAP_CSS_URL));
        response.render(CssHeaderItem.forReference(AdminPortalApplication.BOOTSTRAP_CSS_THEME_URL));
        response.render(CssHeaderItem.forReference(AdminPortalApplication.BOOTSTRAP_CSS_DATEPICKER3_URL));
        response.render(CssHeaderItem.forReference(AdminPortalApplication.JASNY_BOOTSTRAP_CSS_URL));
        response.render(CssHeaderItem.forReference(AdminPortalApplication.ADMIN_CSS));
    }

    protected final void setPageTitle(String title) {
        add(new Label("pageTitle", "Rate Admin: " + title));
    }

}
