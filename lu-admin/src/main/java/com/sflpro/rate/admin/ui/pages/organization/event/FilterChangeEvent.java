package com.sflpro.rate.admin.ui.pages.organization.event;

import org.apache.wicket.ajax.AjaxRequestTarget;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/14/14
 * Time: 8:40 PM
 */
public interface FilterChangeEvent {

    /**
     * Call when filter change
     *
     * @param target Ajax Request target
     * @param value  Field value
     * @param type   Field type
     */
    void onChangeFilter(AjaxRequestTarget target, Object value, FilterFieldType type);
}
