package com.sflpro.rate.admin.ui.pages.organization.bank;

import com.sflpro.rate.admin.ui.pages.organization.AbstractOrganizationPage;
import com.sflpro.rate.admin.ui.panels.AdminMenuPanel;
import com.sflpro.rate.models.datatypes.OrganizationType;
import org.apache.wicket.request.mapper.parameter.PageParameters;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/24/14
 * Time: 9:41 AM
 */
public class BankPage extends AbstractOrganizationPage {

    public BankPage() {
        super("Add Bank", AdminMenuPanel.BANKS_LINK_ID, "Add Bank", OrganizationType.BANK);
    }


    @Override
    public void onViewDetailsPage(PageParameters parameters) {
        setResponsePage(BankDetailsPage.class, parameters);
    }
}
