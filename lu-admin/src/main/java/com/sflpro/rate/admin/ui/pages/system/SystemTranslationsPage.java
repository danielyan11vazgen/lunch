package com.sflpro.rate.admin.ui.pages.system;

import com.sflpro.rate.admin.ui.dto.SystemTranslationDTO;
import com.sflpro.rate.admin.ui.pages.AdminBasePage;
import com.sflpro.rate.admin.ui.panels.AdminMenuPanel;
import com.sflpro.rate.models.dto.Language;
import com.sflpro.rate.services.translation.TranslationService;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.*;

/**
 * User: Mher Sargsyan
 * Company: SFL LLC
 * Date: 7/15/2014
 * Time: 10:03 AM
 */
public class SystemTranslationsPage extends AdminBasePage {

    @SpringBean
    private TranslationService translationService;

    private DataViewContainer dataViewContainer;

    public SystemTranslationsPage() {
        super("System translations", AdminMenuPanel.SYSTEM_TRANSLATIONS_LINK_ID);

        initSystemTranslationsPage();
    }

    /**
     * Initialize page content
     */
    private void initSystemTranslationsPage() {
        dataViewContainer = new DataViewContainer("dataViewContainer");
        add(dataViewContainer);

        add(new FiltersContainer("filterContainer", dataViewContainer));
    }

    /**
     * Data View Container
     */
    private class DataViewContainer extends WebMarkupContainer implements FilterChangeEvent {

        private TranslationDataView translationDataView;

        private String numberFilterValue;

        private Map<String, String> translatableFilterValues = new HashMap<>();


        public DataViewContainer(String id) {
            super(id);
            setOutputMarkupId(true);

            initDataViewContainer();
        }


        /**
         * Initialize DataViewContainer
         */
        private void initDataViewContainer() {
            removeAll();

            translationDataView = new TranslationDataView("translationDataView", new TranslationsDataProvider());
            add(translationDataView);
        }

        /**
         * @param numberFilterValue        filter value for translation unique number
         * @param translatableFilterValues filter values for translation strings
         */
        private void updateSearchResult(String numberFilterValue, Map<String, String> translatableFilterValues) {
            removeAll();

            translationDataView = new TranslationDataView("translationDataView", new TranslationsDataProvider(numberFilterValue, translatableFilterValues));
            add(translationDataView);
        }

        @Override
        public void onChangeFilter(AjaxRequestTarget target, Object value, String fieldType) {

            switch (fieldType) {
                case "translation_number": {
                    numberFilterValue = value != null ? (String) value : "";
                    break;
                }
                default: {
                    final String translatableValue = value != null ? (String) value : "";
                    translatableFilterValues.put(fieldType, translatableValue);
                }
            }

            updateSearchResult(numberFilterValue, translatableFilterValues);
            target.add(this);
        }


        /**
         * Translations Data View
         */
        private class TranslationDataView extends DataView<SystemTranslationDTO> {

            protected TranslationDataView(String id, IDataProvider<SystemTranslationDTO> dataProvider) {
                super(id, dataProvider);
//                setItemsPerPage(ITEM_PER_PAGE);
                setOutputMarkupId(true);
            }

            @Override
            protected void populateItem(Item<SystemTranslationDTO> item) {
                final SystemTranslationDTO systemTranslationDTO = item.getModelObject();

                item.add(new Label("contentId", systemTranslationDTO.getContentId()));

                final Map<String, String> translatableValuesMap = systemTranslationDTO.getTranslationValues();

                final List<Language> languageList = translationService.getAllLanguages();

                item.add(new ListView<Language>("translatableColumn", languageList) {

                    @Override
                    protected void populateItem(ListItem<Language> item) {
                        String key = item.getModelObject().getCode();
                        item.add(new Label("translatableValue", translatableValuesMap.get(key)).setEscapeModelStrings(false));
                    }
                });

                item.add(new AjaxLink<String>("editTranslationPageLink") {

                    @Override
                    public void onClick(AjaxRequestTarget target) {

                        PageParameters pageParameters = new PageParameters();
                        pageParameters.add("translationId", systemTranslationDTO.getContentId());

                        setResponsePage(EditSystemTranslationsPage.class, pageParameters);
                    }
                });

            }
        }


        /**
         * Translations Data Provider
         */
        private class TranslationsDataProvider extends SortableDataProvider<SystemTranslationDTO, SystemTranslationDTO> {

            private String numberFilterValue;

            private Map<String, String> translatableFilterValues;

            private TranslationsDataProvider() {
            }

            private TranslationsDataProvider(String numberFilterValue, Map<String, String> translatableFilterValues) {
                this.numberFilterValue = numberFilterValue;
                this.translatableFilterValues = translatableFilterValues;
            }

            @Override
            public Iterator<SystemTranslationDTO> iterator(long first, long count) {
                return translationService.getAllSystemTranslations(this.numberFilterValue, translatableFilterValues).iterator();
            }

            @Override
            public long size() {
                return translationService.getAllSystemTranslations(this.numberFilterValue, translatableFilterValues).size();
            }

            @Override
            public IModel<SystemTranslationDTO> model(SystemTranslationDTO systemTranslationDTO) {
                return new Model<>(systemTranslationDTO);
            }

        }

    }

    /**
     * Filters Container
     */
    private class FiltersContainer extends WebMarkupContainer {

        private TextField<String> numberFilter;

        public FiltersContainer(String id, FilterChangeEvent event) {
            super(id);

            initFilterContainer(event);
        }


        /**
         * Initialize Filter Container
         */
        private void initFilterContainer(final FilterChangeEvent event) {

            numberFilter = new TextField<>("numberFilter", new Model<String>());
            add(numberFilter);

            numberFilter.add(new AjaxFormComponentUpdatingBehavior("change") {
                @Override
                protected void onUpdate(AjaxRequestTarget target) {
                    final String numberFilterValue = numberFilter.getModelObject();
                        event.onChangeFilter(target, numberFilterValue, "translation_number");
                }
            });

            final List<Language> languageList = translationService.getAllLanguages();

            add(new ListView<Language>("translatableColumnHeaderList", languageList) {

                @Override
                protected void populateItem(final ListItem<Language> item) {
                    final Language language = item.getModelObject();
                    item.add(new Label("translatableColumnHeaderLabel", language.getName()));

                    final TextField textField = new TextField<>("translatableColumnHeaderInput", new Model<String>());

                    textField.add(new AjaxFormComponentUpdatingBehavior("change") {
                        @Override
                        protected void onUpdate(AjaxRequestTarget target) {
                            event.onChangeFilter(target, textField.getModelObject(), language.getCode());
                        }
                    });

                    item.add(textField);
                }
            });


        }
    }

    /**
     * Filters change events handler interface
     */
    private interface FilterChangeEvent {
        void onChangeFilter(AjaxRequestTarget target, Object value, String fieldType);
    }
}
