package com.sflpro.rate.admin.ui.pages.organization.investment;

import com.sflpro.rate.admin.ui.pages.organization.AbstractBranchPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/24/14
 * Time: 3:51 PM
 */
public class InvestmentBranchPage extends AbstractBranchPage {

    public InvestmentBranchPage(PageParameters pageParameters) {
        super(pageParameters);
    }

    @Override
    public void onBackLinkClick(PageParameters parameters) {

    }

    @Override
    public void onCancelLinkClick(PageParameters parameters) {

    }

    @Override
    public void onSaveLinkClick(PageParameters parameters) {

    }
}
