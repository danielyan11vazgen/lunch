package com.sflpro.rate.admin.ui.pages.organization;

import com.sflpro.rate.admin.ui.pages.AdminBasePage;
import com.sflpro.rate.admin.ui.pages.organization.event.BranchFilterFieldType;
import com.sflpro.rate.admin.ui.pages.organization.event.BranchPageType;
import com.sflpro.rate.admin.ui.pages.organization.event.FilterChangeEvent;
import com.sflpro.rate.admin.ui.pages.organization.event.FilterFieldType;
import com.sflpro.rate.admin.ui.panels.modal.organization.AbstractModalWindow;
import com.sflpro.rate.admin.ui.panels.modal.organization.event.ModalWindowChangeEvent;
import com.sflpro.rate.admin.ui.panels.modal.organization.type.ModalWindowType;
import com.sflpro.rate.models.datatypes.OrganizationStatus;
import com.sflpro.rate.models.datatypes.OrganizationType;
import com.sflpro.rate.models.dto.Branch;
import com.sflpro.rate.models.dto.Organization;
import com.sflpro.rate.services.branch.BranchService;
import com.sflpro.rate.services.organization.OrganizationService;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.ajax.markup.html.navigation.paging.AjaxPagingNavigator;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.navigation.paging.IPageable;
import org.apache.wicket.markup.html.navigation.paging.IPagingLabelProvider;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigation;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/14/14
 * Time: 8:50 PM
 */
public abstract class AbstractOrganizationDetailsPage extends AdminBasePage {

    @SpringBean
    private OrganizationService organizationService;

    @SpringBean
    private BranchService branchService;

    private BankDetailsContainer bankDetailsContainer;

    private AbstractModalWindow bankModalWindow;

    private Long organizationId;
    private OrganizationType organizationType;
    private String menuActiveItem;

    private static final int ITEM_PER_PAGE = 10;


    public AbstractOrganizationDetailsPage(String pageTitle, PageParameters pageParameters) {
        super(pageTitle, pageParameters.get("menuActiveItem").toString());

        menuActiveItem = pageParameters.get("menuActiveItem").toString();
        organizationId = pageParameters.get("organizationId").toLong();
        organizationType = pageParameters.get("organizationType").toOptional(OrganizationType.class);

        initOrganizationDetailsPage();
    }


    /**
     * Initializing organization details page
     */
    private void initOrganizationDetailsPage() {
        add(new BackAjaxLink("backLink"));
        add(new EditDetailsAjaxLink("editDetailsAjaxLink"));
        add(new SetRatesAjaxLink("setRatesAjaxLink"));
        add(new ParsersAjaxLink("parsersAjaxLink"));

        bankDetailsContainer = new BankDetailsContainer("bankDetailsContainer");
        add(bankDetailsContainer);

        bankModalWindow = new AbstractModalWindow("bankModalWindow", "Edit Bank", organizationId, organizationType, new BankModalWindowChangeEventHandler(), ModalWindowType.UPDATE);
        add(bankModalWindow);

        DataViewContainer dataViewContainer = new DataViewContainer("dataViewContainer");
        add(dataViewContainer);

        add(new FiltersContainer("filterContainer", dataViewContainer));

        add(new AjaxLink<String>("addBranchAjaxLink") {

            @Override
            public void onClick(AjaxRequestTarget target) {
                PageParameters parameters = new PageParameters();
                parameters.add("organizationId", organizationId);
                parameters.add("pageType", BranchPageType.CREATE);
                parameters.add("organizationType", organizationType);
                parameters.add("menuActiveItem", menuActiveItem);

                onAddBranchLinkClick(parameters);
            }
        });
    }


    /**
     * Add Organization Ajax Link
     */
    private class BackAjaxLink extends AjaxLink<String> {

        public BackAjaxLink(String id) {
            super(id);
        }

        @Override
        public void onClick(AjaxRequestTarget target) {
            onBackLinkClick(target);
        }
    }


    /**
     * Edit Details Ajax Link
     */
    private class EditDetailsAjaxLink extends AjaxLink<String> {

        public EditDetailsAjaxLink(String id) {
            super(id);
        }

        @Override
        public void onClick(AjaxRequestTarget target) {
            bankModalWindow.updateForm(target, organizationId);
            target.appendJavaScript("showOrganizationModalWindow();");
        }
    }


    /**
     * Set Rates Ajax Link
     */
    private class SetRatesAjaxLink extends AjaxLink<String> {

        public SetRatesAjaxLink(String id) {
            super(id);
        }

        @Override
        public void onClick(AjaxRequestTarget target) {
            onSetRatesLinkClick(target);
        }
    }


    /**
     * Add Parsers Ajax Link
     */
    private class ParsersAjaxLink extends AjaxLink<String> {

        public ParsersAjaxLink(String id) {
            super(id);
        }

        @Override
        public void onClick(AjaxRequestTarget target) {
            onParsersLinkClick(target);
        }
    }


    /**
     * Bank Details Container
     */
    private class BankDetailsContainer extends WebMarkupContainer {

        private Organization organization;


        public BankDetailsContainer(String id) {
            super(id);
            setOutputMarkupId(true);

            initBankDetailsContainer();
        }


        /**
         * Initializing Bank Details Container
         */
        private void initBankDetailsContainer() {
            removeAll();

            this.organization = organizationService.findById(organizationId);

            String url = organization.getUrl() != null ? organization.getUrl() : "";

            final String license = organization.getLicense() != null ? organization.getLicense() : "";

            add(new Label("bankName", organization.getName()));
            add(new ExternalLink("webSiteUrl", url, url) {
                @Override
                protected void onComponentTag(ComponentTag tag) {
                    super.onComponentTag(tag);
                    tag.put("target", "_blank");
                }
            });
            add(new Label("license", license));
            add(new Label("status", organization.getStatus().name()));

            add(new WebMarkupContainer("logo") {

                @Override
                protected void onComponentTag(ComponentTag tag) {
                    super.onComponentTag(tag);

                    if (organization.getLogoUrl() != null) {
                        tag.put("src", "/resource/" + organization.getLogoUrl());
                    } else {
                        tag.put("src", "/static/images/logo-not-available.gif");
                    }
                }
            });
        }

    }


    /**
     * Filters Container
     */
    private class FiltersContainer extends WebMarkupContainer {

        private TextField<String> nameFilter;
        private TextField<String> regionFilter;
        private TextField<String> cityFilter;
        private TextField<String> districtFilter;
        private TextField<String> addressFilter;
        private DropDownChoice<OrganizationStatus> statusFilter;

        public FiltersContainer(String id, FilterChangeEvent event) {
            super(id);

            initFilterContainer(event);
        }


        /**
         * Initialize Filter Container
         */
        private void initFilterContainer(final FilterChangeEvent event) {

            nameFilter = new TextField<>("nameFilter", new Model<String>());
            add(nameFilter);

            regionFilter = new TextField<>("regionFilter", new Model<String>());
            add(regionFilter);

            cityFilter = new TextField<>("cityFilter", new Model<String>());
            add(cityFilter);

            districtFilter = new TextField<>("districtFilter", new Model<String>());
            add(districtFilter);

            addressFilter = new TextField<>("addressFilter", new Model<String>());
            add(addressFilter);

            List<OrganizationStatus> organizationStatusList = new ArrayList<>(EnumSet.allOf(OrganizationStatus.class));
            organizationStatusList.remove(OrganizationStatus.PENDING);

            statusFilter = new DropDownChoice<>("statusFilter", new Model<>(OrganizationStatus.ALL), organizationStatusList);
            add(statusFilter);

            nameFilter.add(new AjaxFormComponentUpdatingBehavior("change") {
                @Override
                protected void onUpdate(AjaxRequestTarget target) {
                    event.onChangeFilter(target, nameFilter.getModelObject(), BranchFilterFieldType.NAME);
                }
            });

            regionFilter.add(new AjaxFormComponentUpdatingBehavior("change") {
                @Override
                protected void onUpdate(AjaxRequestTarget target) {
                    event.onChangeFilter(target, regionFilter.getModelObject(), BranchFilterFieldType.REGION);
                }
            });

            cityFilter.add(new AjaxFormComponentUpdatingBehavior("change") {
                @Override
                protected void onUpdate(AjaxRequestTarget target) {
                    event.onChangeFilter(target, cityFilter.getModelObject(), BranchFilterFieldType.CITY);
                }
            });

            districtFilter.add(new AjaxFormComponentUpdatingBehavior("change") {
                @Override
                protected void onUpdate(AjaxRequestTarget target) {
                    event.onChangeFilter(target, districtFilter.getModelObject(), BranchFilterFieldType.DISTRICT);
                }
            });

            addressFilter.add(new AjaxFormComponentUpdatingBehavior("change") {
                @Override
                protected void onUpdate(AjaxRequestTarget target) {
                    event.onChangeFilter(target, addressFilter.getModelObject(), BranchFilterFieldType.ADDRESS);
                }
            });

            statusFilter.add(new AjaxFormComponentUpdatingBehavior("change") {
                @Override
                protected void onUpdate(AjaxRequestTarget target) {
                    event.onChangeFilter(target, statusFilter.getModelObject(), BranchFilterFieldType.STATUS);
                }
            });
        }
    }


    /**
     * Data View Container
     */
    private class DataViewContainer extends WebMarkupContainer implements FilterChangeEvent {

        private BranchesDataView branchesDataView;

        private String nameFilterValue = "";

        private String regionFilterValue = "";

        private String cityFilterValue = "";

        private String districtFilterValue = "";

        private String addressFilterValue = "";

        private OrganizationStatus statusFilterValue = OrganizationStatus.ALL;

        public DataViewContainer(String id) {
            super(id);
            setOutputMarkupId(true);

            initDataViewContainer("", "", "", "", "", OrganizationStatus.ALL);
        }


        private void initDataViewContainer(String nameFilterValue, String regionFilterValue, String cityFilterValue, String districtFilterValue, String addressFilterValue, OrganizationStatus statusFilterValue) {
            removeAll();

            branchesDataView = new BranchesDataView("branchesDataView", new BranchesDataProvider(nameFilterValue, regionFilterValue, cityFilterValue, districtFilterValue, addressFilterValue, statusFilterValue));
            add(branchesDataView);

            add(new CustomAjaxPagingNavigator("pagingNavigator", branchesDataView));
        }


        @Override
        public void onChangeFilter(AjaxRequestTarget target, Object value, FilterFieldType type) {
            final BranchFilterFieldType filterFieldType = (BranchFilterFieldType) type;
            switch (filterFieldType) {
                case NAME: {
                    nameFilterValue = value != null ? (String) value : "";
                    break;
                }
                case REGION: {
                    regionFilterValue = value != null ? (String) value : "";
                    break;
                }
                case CITY: {
                    cityFilterValue = value != null ? (String) value : "";
                    break;
                }
                case DISTRICT: {
                    districtFilterValue = value != null ? (String) value : "";
                    break;
                }
                case ADDRESS: {
                    addressFilterValue = value != null ? (String) value : "";
                    break;
                }
                case STATUS: {
                    statusFilterValue = value != null ? (OrganizationStatus) value : OrganizationStatus.ALL;
                    break;
                }
            }


            initDataViewContainer(nameFilterValue, regionFilterValue, cityFilterValue, districtFilterValue, addressFilterValue, statusFilterValue);
            target.add(this);
        }


        /**
         * Branches Data View
         */
        private class BranchesDataView extends DataView<Branch> {

            protected BranchesDataView(String id, IDataProvider<Branch> dataProvider) {
                super(id, dataProvider);
                setItemsPerPage(ITEM_PER_PAGE);
                setOutputMarkupId(true);
            }

            @Override
            protected void populateItem(Item<Branch> item) {
                final Branch branch = item.getModelObject();

                item.add(new Label("serialNumber", ((BranchesDataProvider) getDataProvider()).getRowNum() + item.getIndex()));

                item.add(new Label("name", branch.getName()));
                item.add(new Label("region", branch.getRegion().getName()));
                item.add(new Label("city", branch.getCity() != null ? branch.getCity().getName() : ""));
                item.add(new Label("district", branch.getDistrict() != null ? branch.getDistrict().getName() : ""));
                item.add(new Label("address", branch.getAddress()));

                item.add(new Label("status", branch.getStatus().name()));
                item.add(new AjaxLink<String>("editBranchDetails") {
                    @Override
                    public void onClick(AjaxRequestTarget target) {

                        PageParameters pageParameters = new PageParameters();
                        pageParameters.add("organizationId", organizationId);
                        pageParameters.add("branchId", branch.getId());
                        pageParameters.add("organizationType", organizationType);
                        pageParameters.add("pageType", BranchPageType.EDIT);
                        pageParameters.add("menuActiveItem", menuActiveItem);

                        onEditDetailsLinkClick(pageParameters);

                    }
                });
            }
        }


        /**
         * Bank Data Provider
         */
        private class BranchesDataProvider extends SortableDataProvider<Branch, Branch> {

            private String nameFilterValue = "";
            private String regionFilterValue = "";
            private String cityFilterValue = "";
            private String districtFilterValue = "";
            private String addressFilterValue = "";
            private OrganizationStatus statusFilterValue = OrganizationStatus.ALL;

            private long rowNum;

            private BranchesDataProvider(String nameFilterValue, String regionFilterValue, String cityFilterValue, String districtFilterValue, String addressFilterValue, OrganizationStatus statusFilterValue) {
                this.nameFilterValue = nameFilterValue;
                this.regionFilterValue = regionFilterValue;
                this.cityFilterValue = cityFilterValue;
                this.districtFilterValue = districtFilterValue;
                this.addressFilterValue = addressFilterValue;
                this.statusFilterValue = statusFilterValue;
            }

            @Override
            public Iterator<Branch> iterator(long first, long count) {
                rowNum = first + 1;
                return branchService.getAllOrganizationBranchesByFilter(
                        organizationId,
                        (int) first,
                        (int) (count),
                        nameFilterValue,
                        regionFilterValue,
                        cityFilterValue,
                        districtFilterValue,
                        addressFilterValue,
                        statusFilterValue);
            }

            @Override
            public long size() {
                return branchService.count(
                        organizationId,
                        nameFilterValue != null ? nameFilterValue : "",
                        regionFilterValue,
                        cityFilterValue,
                        districtFilterValue,
                        addressFilterValue,
                        statusFilterValue
                );
            }

            @Override
            public IModel<Branch> model(Branch object) {
                return new Model<>(object);
            }

            public long getRowNum() {
                return rowNum;
            }
        }
    }


    /**
     * Custom Ajax Paging Navigator
     */
    private class CustomAjaxPagingNavigator extends AjaxPagingNavigator {

        public CustomAjaxPagingNavigator(String id, IPageable pageable) {
            super(id, pageable);
            setOutputMarkupId(true);
        }

        @Override
        protected AbstractLink newPagingNavigationIncrementLink(String id, IPageable pageable, int increment) {
            return super.newPagingNavigationIncrementLink(id, pageable, increment);
        }

        @Override
        protected AbstractLink newPagingNavigationLink(String id, IPageable pageable, int pageNumber) {
            return super.newPagingNavigationLink(id, pageable, pageNumber);
        }

        @Override
        protected PagingNavigation newNavigation(String id, IPageable pageable, IPagingLabelProvider labelProvider) {
            return super.newNavigation(id, pageable, labelProvider);
        }

        @Override
        public boolean isVisible() {
            return ((DataView) getPageable()).getItemCount() > ITEM_PER_PAGE;
        }

    }


    /**
     * Bank modal window change detector
     */
    private class BankModalWindowChangeEventHandler implements ModalWindowChangeEvent {

        @Override
        public void change(AjaxRequestTarget target) {
            bankDetailsContainer.initBankDetailsContainer();
            target.add(bankDetailsContainer);
        }
    }


    /**
     * Call when clicking on Back Link
     *
     * @param target AjaxRequestTarget
     */
    public abstract void onBackLinkClick(AjaxRequestTarget target);


    /**
     * Call when clicking on Add Branch
     *
     * @param parameters page parameters
     */
    public abstract void onAddBranchLinkClick(PageParameters parameters);


    /**
     * Call when clicking on Edit Details Link
     *
     * @param parameters PageParameters
     */
    public abstract void onEditDetailsLinkClick(PageParameters parameters);


    /**
     * Call when clicking on Set Rates
     *
     * @param target AjaxRequestTarget
     */
    public abstract void onSetRatesLinkClick(AjaxRequestTarget target);


    /**
     * Call when clicking on Parsers Link
     *
     * @param target AjaxRequestTarget
     */
    public abstract void onParsersLinkClick(AjaxRequestTarget target);
}
