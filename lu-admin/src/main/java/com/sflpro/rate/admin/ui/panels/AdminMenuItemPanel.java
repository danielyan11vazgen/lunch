package com.sflpro.rate.admin.ui.panels;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/20/14
 * Time: 4:17 PM
 */
public class AdminMenuItemPanel extends Panel {

    private final WebMarkupContainer menuItemContainer;

    public AdminMenuItemPanel(String id, IModel<String> linkText, final Class<? extends WebPage> toPage) {
        super(id);

        menuItemContainer = new WebMarkupContainer("menuItemContainer");
        Link<Void> menuItem = new Link<Void>("menuItem") {
            @Override
            public void onClick() {
                if (toPage != null) {
                    setResponsePage(toPage);
                }
            }

        };

        add(menuItemContainer);
        menuItemContainer.add(menuItem);
        menuItem.add(new Label("linkText", linkText));
    }

    public void setActive(boolean active) {
        if (active) {
            menuItemContainer.add(new AttributeModifier("class", "active"));
        } else {
            menuItemContainer.add(new AttributeModifier("class", ""));
        }
    }

}
