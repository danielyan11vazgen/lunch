package com.sflpro.rate.admin.ui.pages;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/20/14
 * Time: 4:21 PM
 */
public class AdminDashboardPage extends AdminBasePage {

    public AdminDashboardPage() {
        super("Home", "");
    }
}
