package com.sflpro.rate.admin.ui.pages.system;

import com.sflpro.rate.admin.ui.dto.TranslationDTO;
import com.sflpro.rate.admin.ui.pages.AdminBasePage;
import com.sflpro.rate.admin.ui.panels.AdminMenuPanel;
import com.sflpro.rate.admin.ui.panels.language.DynamicLanguagePanel;
import com.sflpro.rate.admin.ui.panels.system.SystemTranslationLanguagePanel;
import com.sflpro.rate.models.datatypes.LanguageKeyType;
import com.sflpro.rate.models.datatypes.OrganizationType;
import com.sflpro.rate.models.datatypes.ResourceType;
import com.sflpro.rate.models.dto.Organization;
import com.sflpro.rate.models.dto.Resource;
import com.sflpro.rate.models.dto.SystemTranslation;
import com.sflpro.rate.services.translation.TranslationService;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * User: Mher Sargsyan
 * Company: SFL LLC
 * Date: 7/15/2014
 * Time: 4:46 PM
 */
public class EditSystemTranslationsPage extends AdminBasePage {

    @SpringBean
    private TranslationService translationService;

    private long systemTranslationId;

    public EditSystemTranslationsPage(PageParameters pageParameters) {
        super("System translations", AdminMenuPanel.SYSTEM_TRANSLATIONS_LINK_ID);

        systemTranslationId = pageParameters.get("translationId").toLong();

        initEditSystemTranslationsPage();
    }

    /**
     * Initialize page content
     */
    private void initEditSystemTranslationsPage() {

        final WebMarkupContainer container = new WebMarkupContainer("container");
        add(container);

        final Form editForm = new EditSystemTranslationsForm("editForm");

        container.add(new SaveTranslationAjaxLink("saveAjaxLink", editForm));
        container.add(new BackAjaxLink("backLink"));
        container.add(new BackAjaxLink("cancelLink"));

        container.add(editForm);
    }

    /**
     * Back link
     */
    private class BackAjaxLink extends AjaxLink<String> {

        public BackAjaxLink(String id) {
            super(id);
        }

        @Override
        public void onClick(AjaxRequestTarget target) {

            setResponsePage(SystemTranslationsPage.class);
        }
    }

    /**
     * Edit translation form component
     */
    private class EditSystemTranslationsForm extends Form {

        private DynamicLanguagePanel dynamicLanguagePanel;

        private FeedbackPanel feedbackPanel;

        public EditSystemTranslationsForm(String id) {
            super(id);
            setMultiPart(true);
            initEditSystemTranslationsForm();
        }

        /**
         * Initialize edit system translations form
         */
        private void initEditSystemTranslationsForm() {

            final SystemTranslation systemTranslation = translationService.getSystemTranslationById(systemTranslationId);

            final String entityName = systemTranslation.getClass().getSimpleName().toLowerCase();
            final Long entityId = (systemTranslation == null) ? null : systemTranslation.getId();

            dynamicLanguagePanel = new SystemTranslationLanguagePanel("dynamicLanguagePanel", entityName, entityId, "name", LanguageKeyType.SYSTEM);

            feedbackPanel = new FeedbackPanel("feedbackPanel");
            feedbackPanel.setOutputMarkupId(true);

            add(feedbackPanel);
            add(dynamicLanguagePanel);
        }
    }

    /**
     * Save Modal Window Ajax Link
     */
    private class SaveTranslationAjaxLink extends AjaxSubmitLink {

        public SaveTranslationAjaxLink(String id, Form<?> form) {
            super(id, form);
            setOutputMarkupId(true);
        }

        @Override
        protected void onSubmit(AjaxRequestTarget target, Form<?> form) {

            EditSystemTranslationsForm submitForm = (EditSystemTranslationsForm) form;

            handleTranslationSave(submitForm);
            setResponsePage(SystemTranslationsPage.class);
        }


        @Override
        protected void onError(AjaxRequestTarget target, Form<?> form) {
            EditSystemTranslationsForm submitForm = (EditSystemTranslationsForm) form;
            target.add(submitForm.feedbackPanel);
        }

        /**
         * Save translation values
         */
        private void handleTranslationSave(EditSystemTranslationsForm form) {
            /**
             * Get system translations
             */
            List<TranslationDTO> translationDTOList = form.dynamicLanguagePanel.getFieldListViewContainer().getFieldListView().getModelObject();

            /**
             * Save Translations
             */
            translationService.createOrUpdateSystemTranslation(translationDTOList, systemTranslationId, SystemTranslation.class.getSimpleName().toLowerCase(), "name");
        }
    }
}