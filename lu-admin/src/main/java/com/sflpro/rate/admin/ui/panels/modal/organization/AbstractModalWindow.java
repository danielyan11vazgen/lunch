package com.sflpro.rate.admin.ui.panels.modal.organization;

import com.sflpro.rate.admin.ui.dto.TranslationDTO;
import com.sflpro.rate.admin.ui.panels.language.DynamicLanguagePanel;
import com.sflpro.rate.admin.ui.panels.modal.organization.event.ModalWindowChangeEvent;
import com.sflpro.rate.admin.ui.panels.modal.organization.type.ModalWindowType;
import com.sflpro.rate.models.datatypes.LanguageKeyType;
import com.sflpro.rate.models.datatypes.OrganizationStatus;
import com.sflpro.rate.models.datatypes.OrganizationType;
import com.sflpro.rate.models.datatypes.ResourceType;
import com.sflpro.rate.models.dto.Organization;
import com.sflpro.rate.models.dto.Resource;
import com.sflpro.rate.services.exception.OrganizationExistException;
import com.sflpro.rate.services.organization.OrganizationService;
import com.sflpro.rate.services.resource.ResourceManager;
import com.sflpro.rate.services.translation.TranslationService;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/14/14
 * Time: 5:43 PM
 */
public class AbstractModalWindow extends Panel {

    @SpringBean
    private ResourceManager resourceManager;

    @SpringBean
    private OrganizationService organizationService;

    @SpringBean
    private TranslationService translationService;

    public final Logger logger = LoggerFactory.getLogger(getClass());

    private BankModalWindowForm bankModalWindowForm;
    private ModalWindowChangeEvent event;

    private ModalWindowType type;
    private OrganizationType organizationType;
    private Organization o;

    /**
     * Constructor
     *
     * @param id         modal window markup id
     * @param headerText modal window header text
     */
    public AbstractModalWindow(String id, String headerText, Long organizationId, OrganizationType organizationType,  ModalWindowChangeEvent event, ModalWindowType type) {
        super(id);

        this.event = event;
        this.type = type;
        this.organizationType = organizationType;

        if (organizationId != null) {
            o = organizationService.findById(organizationId);
        }


        initModalWindow(headerText);
    }


    /**
     * Initializing modal window
     *
     * @param headerText modal window header text
     */
    private void initModalWindow(String headerText) {
        add(new Label("modalWindowHeader", headerText));

        bankModalWindowForm = new BankModalWindowForm("bankModalWindowForm");
        add(bankModalWindowForm);

        add(new SaveModalWindowAjaxLink("saveModalWindowAjaxLink", bankModalWindowForm));
    }


    /**
     * Update bank modal form via ajax
     *
     * @param target AjaxRequestTarget
     */
    public void updateForm(AjaxRequestTarget target, Long organizationId) {

        if (organizationId != null) {
            o = organizationService.findById(organizationId);
        }

        bankModalWindowForm.initAddBankModalWindowForm();
        target.add(bankModalWindowForm);
    }


    /**
     * Bank Modal Window Form
     */
    private class BankModalWindowForm extends Form {

        private Model<String> bankWebsiteUrlModel;
        private Model<String> bankLicenseModel;
        private Model<OrganizationStatus> bankStatusModel;

        private TextField<String> bankWebsiteUrl;
        private TextField<String> bankLicense;
        private DropDownChoice<OrganizationStatus> bankStatus;

        private WebMarkupContainer bankLogoPreview;
        private FileUploadField bankLogoUploadField;

        private DynamicLanguagePanel dynamicLanguagePanel;

        private FeedbackPanel feedbackPanel;

        public BankModalWindowForm(String id) {
            super(id);

            setOutputMarkupId(true);
            initAddBankModalWindowForm();
        }


        /**
         * Initializing add bank modal window form
         */
        private void initAddBankModalWindowForm() {
            removeAll();

            add(new Label("bankWebsiteUrlLbl", new ResourceModel("bankPage.bankWebsiteUrlLbl")));
            add(new Label("bankLicenseLbl", new ResourceModel("bankPage.bankLicenseLbl")));
            add(new Label("bankStatusLbl", new ResourceModel("bankPage.bankStatusLbl")));

            add(new Label("bankLogoLbl", new ResourceModel("bankPage.bankLogoLbl")));

            initFormComponentModels();

            final String entityName = (o == null) ? null : o.getClass().getSimpleName().toLowerCase();
            final Long entityId = (o == null) ? null : o.getId();

            dynamicLanguagePanel = new DynamicLanguagePanel("dynamicLanguagePanel", entityName, entityId, "Name", LanguageKeyType.USER_GENERATED);
            add(dynamicLanguagePanel);

            List<OrganizationStatus> organizationStatusList = new ArrayList<>(EnumSet.allOf(OrganizationStatus.class));
            organizationStatusList.remove(OrganizationStatus.PENDING);
            organizationStatusList.remove(OrganizationStatus.ALL);

            bankWebsiteUrl = new TextField<>("bankWebsiteUrl", bankWebsiteUrlModel);
            bankLicense = new TextField<>("bankLicense", bankLicenseModel);
            bankStatus = new DropDownChoice<>("bankStatus", bankStatusModel, organizationStatusList);

            bankLogoPreview = new WebMarkupContainer("bankLogoPreview") {
                @Override
                protected void onComponentTag(ComponentTag tag) {
                    super.onComponentTag(tag);

                    if (o != null && o.getLogoUrl() != null) {
                        tag.put("src", "/resource/" + o.getLogoUrl());
                    } else {
                        tag.put("src", "/static/images/logo-not-available.gif");
                    }
                }
            };
            bankLogoUploadField = new FileUploadField("bankLogoUploadField");

            feedbackPanel = new FeedbackPanel("feedbackPanel");
            feedbackPanel.setOutputMarkupId(true);

            add(bankWebsiteUrl);
            add(bankLicense);
            add(bankStatus);

            add(bankLogoPreview);
            add(bankLogoUploadField);

            add(feedbackPanel);
        }


        /**
         * Initializing form components models
         */
        private void initFormComponentModels() {
            if (o != null) {
                bankWebsiteUrlModel = new Model<>(o.getUrl() != null ? o.getUrl() : "");
                bankLicenseModel = new Model<>(o.getLicense() != null ? o.getLicense() : "");
                bankStatusModel = new Model<>(o.getStatus() != null ? o.getStatus() : OrganizationStatus.INACTIVE);
            } else {
                bankWebsiteUrlModel = Model.of();
                bankLicenseModel = Model.of();
                bankStatusModel = new Model<>(OrganizationStatus.INACTIVE);
            }
        }
    }


    /**
     * Save Modal Window Ajax Link
     */
    private class SaveModalWindowAjaxLink extends AjaxSubmitLink {

        public SaveModalWindowAjaxLink(String id, Form<?> form) {
            super(id, form);
        }

        @Override
        protected void onSubmit(AjaxRequestTarget target, Form<?> form) {

            BankModalWindowForm submitForm = (BankModalWindowForm) form;

            /**
             * Save Logo
             */
            FileUpload fileUpload = submitForm.bankLogoUploadField.getFileUpload();
            Resource resource = null;

            if (fileUpload != null) {
                try {
                    File file = resourceManager.createTemporaryFile(fileUpload.getInputStream());
                    resource = resourceManager.finalizeWorkingWithResource(file, ResourceType.LOGO, fileUpload.getClientFileName());
                } catch (IOException e) {
                    logger.error("Unexpected error occurred during save organization logo! ", e);
                    error(e.getMessage());
                }
            }


            /**
             * Get organization name for saving organization
             */
            List<TranslationDTO> translationDTOList = submitForm.dynamicLanguagePanel.getFieldListViewContainer().getFieldListView().getModelObject();
            String organizationName = null;
            for (TranslationDTO translationDTO : translationDTOList) {

                //todo: get system default language code from db
                if (translationDTO.getLanguageCode().equals("EN")) {
                    organizationName = translationDTO.getFieldValue();
                    break;
                }
            }


            /**
             * Save organization
             */

            String url = submitForm.bankWebsiteUrl.getModelObject();

            if (url != null) {
                if (!url.contains("http://")) {
                    url = "http://" + url;
                }
            }

            Organization organization = new Organization();

            organization.setName(organizationName);
            organization.setUrl(url);
            organization.setLicense(submitForm.bankLicense.getModelObject());
            organization.setStatus(submitForm.bankStatus.getModelObject());
            organization.setType(organizationType);

            if (resource != null) {
                organization.setLogo(resource);
                organization.setLogoUrl(resource.getPath());
            }

            Organization result = null;

            if (o != null) {
                organization.setId(o.getId());
            }

            try {

                switch (type) {
                    case CREATE: {
                        result = organizationService.create(organization);
                        break;
                    }
                    case UPDATE: {
                        result = organizationService.createOrUpdate(organization);
                        break;
                    }
                }


                /**
                 * Save Translations
                 */
                translationService.createOrUpdateTranslation(translationDTOList, result.getId(), Organization.class.getSimpleName().toLowerCase(), "name");

                target.appendJavaScript("closeOrganizationModalWindow();");

                event.change(target);
            } catch (OrganizationExistException e) {
                error(e.getMessage());
                target.add(submitForm.feedbackPanel);
            }
        }


        @Override
        protected void onError(AjaxRequestTarget target, Form<?> form) {
            BankModalWindowForm submitForm = (BankModalWindowForm) form;
            target.add(submitForm.feedbackPanel);
        }
    }

}
