package com.sflpro.rate.admin.ui.panels;

import com.sflpro.rate.admin.ui.pages.organization.bank.BankPage;
import com.sflpro.rate.admin.ui.pages.organization.central.CentralBankPage;
import com.sflpro.rate.admin.ui.pages.organization.credit.CreditPage;
import com.sflpro.rate.admin.ui.pages.organization.exchange.ExchangePage;
import com.sflpro.rate.admin.ui.pages.organization.investment.InvestmentPage;
import com.sflpro.rate.admin.ui.pages.system.SystemTranslationsPage;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.Model;
import org.apache.wicket.util.visit.IVisit;
import org.apache.wicket.util.visit.IVisitor;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/20/14
 * Time: 4:10 PM
 */
public class AdminMenuPanel extends Panel {

    public static final String BANKS_LINK_ID = "adminMenuBanksLink";
    public static final String EXCHANGE_LINK_ID = "adminMenuExchangeLink";
    public static final String CREDIT_LINK_ID = "adminMenuCreditLink";
    public static final String INVESTMENT_LINK_ID = "adminMenuInvestmentLink";
    public static final String CENTRAL_BANK_LINK_ID = "adminMenuCentralBankLink";

    public static final String SYSTEM_TRANSLATIONS_LINK_ID = "adminMenuSystemTranslationsLink";


    public AdminMenuPanel(String id, String menuActiveItemId) {
        super(id);

        final WebMarkupContainer organizationSection = new RepeatingView("organizationMenu");

        organizationSection.add(new AdminMenuItemPanel(BANKS_LINK_ID, Model.of("Banks"), BankPage.class));
        organizationSection.add(new AdminMenuItemPanel(EXCHANGE_LINK_ID, Model.of("Exchange points"), ExchangePage.class));
        organizationSection.add(new AdminMenuItemPanel(CREDIT_LINK_ID, Model.of("Credit organizations"), CreditPage.class));
        organizationSection.add(new AdminMenuItemPanel(INVESTMENT_LINK_ID, Model.of("Investment organizations"), InvestmentPage.class));
        organizationSection.add(new AdminMenuItemPanel(CENTRAL_BANK_LINK_ID, Model.of("Central bank"), CentralBankPage.class));

        add(organizationSection);

        final WebMarkupContainer siteManagementSection = new RepeatingView("siteManagementMenu");

        siteManagementSection.add(new AdminMenuItemPanel(SYSTEM_TRANSLATIONS_LINK_ID, Model.of("System translations"), SystemTranslationsPage.class));

        add(siteManagementSection);

        highlightActiveMenuItem(menuActiveItemId);
    }

    private void highlightActiveMenuItem(final String menuActiveItemId) {
        if (menuActiveItemId != null) {
            visitChildren(AdminMenuItemPanel.class, new IVisitor<AdminMenuItemPanel, Object>() {
                @Override
                public void component(AdminMenuItemPanel object, IVisit<Object> visit) {
                    if (object.getId().contentEquals(menuActiveItemId)) {
                        object.setActive(true);
                    }
                }
            });

        }
    }
}
