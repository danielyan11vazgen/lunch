package com.sflpro.rate.admin.ui.pages.organization.credit;

import com.sflpro.rate.admin.ui.pages.organization.AbstractOrganizationPage;
import com.sflpro.rate.admin.ui.panels.AdminMenuPanel;
import com.sflpro.rate.models.datatypes.OrganizationType;
import org.apache.wicket.request.mapper.parameter.PageParameters;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/26/14
 * Time: 4:08 PM
 */
public class CreditPage extends AbstractOrganizationPage {

    public CreditPage() {
        super("Add Credit Organization", AdminMenuPanel.CREDIT_LINK_ID, "Add Credit Organization", OrganizationType.CREDIT);
    }

    @Override
    public void onViewDetailsPage(PageParameters parameters) {
        setResponsePage(CreditDetailsPage.class, parameters);
    }
}
