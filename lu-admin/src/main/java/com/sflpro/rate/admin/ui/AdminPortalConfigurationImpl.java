package com.sflpro.rate.admin.ui;

import org.apache.wicket.RuntimeConfigurationType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/20/14
 * Time: 4:07 PM
 */
@Service
public class AdminPortalConfigurationImpl implements AdminPortalConfiguration {

    @Value("#{appProperties['rate.runtime']}")
    private String runtime;

    @Override
    public RuntimeConfigurationType getRuntime() {
        return RuntimeConfigurationType.valueOf(runtime.toUpperCase());
    }
}
