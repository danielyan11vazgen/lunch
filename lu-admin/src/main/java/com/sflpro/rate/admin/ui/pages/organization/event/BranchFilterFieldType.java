package com.sflpro.rate.admin.ui.pages.organization.event;

/**
 * User: Mher Sargsyan
 * Company: SFL LLC
 * Date: 7/21/2014
 * Time: 1:48 PM
 */
public enum BranchFilterFieldType implements FilterFieldType {
    NAME, REGION, CITY, DISTRICT, ADDRESS, STATUS
}
