package com.sflpro.rate.admin.ui.panels.modal.organization.type;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/18/14
 * Time: 10:55 AM
 */
public enum ModalWindowType {
    CREATE,
    UPDATE
}
