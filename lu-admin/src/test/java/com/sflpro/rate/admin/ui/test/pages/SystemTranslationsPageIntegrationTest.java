package com.sflpro.rate.admin.ui.test.pages;

import com.sflpro.rate.admin.ui.dto.SystemTranslationDTO;
import com.sflpro.rate.admin.ui.pages.system.EditSystemTranslationsPage;
import com.sflpro.rate.admin.ui.pages.system.SystemTranslationsPage;
import com.sflpro.rate.admin.ui.test.AdminIntegrationTest;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.protocol.http.mock.MockHttpServletRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Iterator;
import java.util.Map;

import static org.junit.Assert.assertTrue;

/**
 * User: Mher Sargsyan
 * Company: SFL LLC
 * Date: 7/15/2014
 * Time: 6:25 PM
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-admin-test.xml"})
@Transactional
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class SystemTranslationsPageIntegrationTest extends AdminIntegrationTest {

    @Test
    public void testOpenPage() {
        wicketTester.startPage(SystemTranslationsPage.class);

        wicketTester.assertRenderedPage(SystemTranslationsPage.class);
    }


    @Test
    public void testTranslatableValuesFilters() throws Exception {

        wicketTester.startPage(SystemTranslationsPage.class);

        ((MockHttpServletRequest) wicketTester.getRequestCycle().getRequest().getContainerRequest())
                .setParameter("filterContainer:translatableColumnHeaderList:0:translatableColumnHeaderInput", "arm");
        wicketTester.executeAjaxEvent("filterContainer:translatableColumnHeaderList:0:translatableColumnHeaderInput", "change");

        DataView dataView = (DataView) wicketTester.getComponentFromLastRenderedPage("dataViewContainer:translationDataView");

        IDataProvider dataProvider = dataView.getDataProvider();
        Iterator iterator = dataProvider.iterator(0, 1);

        while (iterator.hasNext()) {

            SystemTranslationDTO dto = (SystemTranslationDTO) iterator.next();

            Map<String, String> map = dto.getTranslationValues();

            assertTrue(map.containsValue("arm"));

        }

        wicketTester.assertRenderedPage(SystemTranslationsPage.class);

    }

    @Test
    public void testTranslatableValueNumberFilters() throws Exception {

        wicketTester.startPage(SystemTranslationsPage.class);

        ((MockHttpServletRequest) wicketTester.getRequestCycle().getRequest().getContainerRequest())
                .setParameter("filterContainer:numberFilter", "1");
        wicketTester.executeAjaxEvent("filterContainer:numberFilter", "change");

        DataView dataView = (DataView) wicketTester.getComponentFromLastRenderedPage("dataViewContainer:translationDataView");

        IDataProvider dataProvider = dataView.getDataProvider();
        Iterator iterator = dataProvider.iterator(0, 1);

        while (iterator.hasNext()) {

            SystemTranslationDTO dto = (SystemTranslationDTO) iterator.next();

            Map<String, String> map = dto.getTranslationValues();

            assertTrue(map.containsValue("a"));

        }

        wicketTester.assertRenderedPage(SystemTranslationsPage.class);

    }

    @Test
    public void testOpenEditPage() throws Exception {
        wicketTester.startPage(SystemTranslationsPage.class);

        wicketTester.clickLink("dataViewContainer:translationDataView:1:editTranslationPageLink", true);

        wicketTester.assertRenderedPage(EditSystemTranslationsPage.class);

    }
}
