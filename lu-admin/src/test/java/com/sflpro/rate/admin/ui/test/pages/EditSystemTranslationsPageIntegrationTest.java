package com.sflpro.rate.admin.ui.test.pages;

import com.sflpro.rate.admin.ui.pages.system.EditSystemTranslationsPage;
import com.sflpro.rate.admin.ui.pages.system.SystemTranslationsPage;
import com.sflpro.rate.admin.ui.test.AdminIntegrationTest;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.util.tester.FormTester;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * User: Mher Sargsyan
 * Company: SFL LLC
 * Date: 7/16/2014
 * Time: 10:37 AM
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-admin-test.xml"})
@Transactional
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class EditSystemTranslationsPageIntegrationTest extends AdminIntegrationTest {

    @Test
    public void testOpenPage() {
        final PageParameters pageParameters = new PageParameters();
        pageParameters.add("translationId", 1L);

        wicketTester.startPage(EditSystemTranslationsPage.class, pageParameters);

        wicketTester.assertRenderedPage(EditSystemTranslationsPage.class);
    }

    @Test
    public void testClickBackLink() {
        final PageParameters pageParameters = new PageParameters();
        pageParameters.add("translationId", 1L);

        wicketTester.startPage(EditSystemTranslationsPage.class, pageParameters);

        wicketTester.clickLink("container:backLink", true);

        wicketTester.assertRenderedPage(SystemTranslationsPage.class);
    }

    @Test
    public void testClickCancelLink() {
        final PageParameters pageParameters = new PageParameters();
        pageParameters.add("translationId", 1L);

        wicketTester.startPage(EditSystemTranslationsPage.class, pageParameters);

        wicketTester.clickLink("container:cancelLink", true);

        wicketTester.assertRenderedPage(SystemTranslationsPage.class);
    }

    @Test
    public void testFormSubmit() {
        final PageParameters pageParameters = new PageParameters();
        pageParameters.add("translationId", 1L);

        wicketTester.startPage(EditSystemTranslationsPage.class, pageParameters);

        wicketTester.clickLink("container:saveAjaxLink");

        wicketTester.assertRenderedPage(SystemTranslationsPage.class);
    }

    @Test
    public void testFormSubmitWithValidationErrors() {
        final PageParameters pageParameters = new PageParameters();
        pageParameters.add("translationId", 1L);

        wicketTester.startPage(EditSystemTranslationsPage.class, pageParameters);

        FormTester formTester = wicketTester.newFormTester("container:editForm");

        formTester.setValue("dynamicLanguagePanel:fieldListViewContainer:fieldListView:0:field", "");
        formTester.setValue("dynamicLanguagePanel:fieldListViewContainer:fieldListView:1:field", "");
        formTester.setValue("dynamicLanguagePanel:fieldListViewContainer:fieldListView:2:field", "");

        wicketTester.clickLink("container:saveAjaxLink");

        wicketTester.assertErrorMessages("'name (HY)' is required.", "'name (EN)' is required.", "'name (RU)' is required.");

        wicketTester.assertRenderedPage(EditSystemTranslationsPage.class);
    }

}
