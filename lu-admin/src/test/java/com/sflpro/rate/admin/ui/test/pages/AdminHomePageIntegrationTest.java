package com.sflpro.rate.admin.ui.test.pages;

import com.sflpro.rate.admin.ui.pages.AdminDashboardPage;
import com.sflpro.rate.admin.ui.test.AdminIntegrationTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/9/14
 * Time: 6:46 PM
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-admin-test.xml"})
@Transactional
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class AdminHomePageIntegrationTest extends AdminIntegrationTest {

    @Test
    public void testHomePage() {
        wicketTester.startPage(AdminDashboardPage.class);

        wicketTester.assertRenderedPage(AdminDashboardPage.class);
    }

}
