package com.sflpro.rate.admin.ui.test.pages;

import com.sflpro.rate.admin.ui.pages.organization.AbstractBranchPage;
import com.sflpro.rate.admin.ui.pages.organization.event.BranchPageType;
import com.sflpro.rate.admin.ui.test.AdminIntegrationTest;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertNotNull;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/21/14
 * Time: 11:46 AM
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-admin-test.xml"})
@Transactional
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class AbstractBranchPageIntegrationTest extends AdminIntegrationTest {

    @Test
    public void testAddBranchPage() throws Exception {
       /* PageParameters pageParameters = new PageParameters();
        pageParameters.add("organizationId", "3");
        pageParameters.add("pageType", BranchPageType.CREATE);

        wicketTester.startPage(AbstractBranchPage.class, pageParameters);

        wicketTester.assertRenderedPage(AbstractBranchPage.class);*/
    }

    @Test
    public void testAddBranchPageBackToBankDetailsPage() throws Exception {
        /*PageParameters pageParameters = new PageParameters();
        pageParameters.add("organizationId", "3");
        pageParameters.add("pageType", BranchPageType.CREATE);

        wicketTester.startPage(BranchPage.class, pageParameters);

        wicketTester.clickLink("backToBankDetailsPage");

        wicketTester.assertRenderedPage(BankDetailsPage.class);*/
    }

    @Test
    public void testAddBranchPageCancelSaveBranchAjaxLink() throws Exception {
        /*PageParameters pageParameters = new PageParameters();
        pageParameters.add("organizationId", "3");
        pageParameters.add("pageType", BranchPageType.CREATE);

        wicketTester.startPage(BranchPage.class, pageParameters);

        wicketTester.clickLink("branchForm:cancelSaveBranchAjaxLink");

        wicketTester.assertRenderedPage(BankDetailsPage.class);*/
    }

    @Test
    public void testAddBranchPageCreateBranch() throws Exception {
        /*PageParameters pageParameters = new PageParameters();
        pageParameters.add("organizationId", "3");
        pageParameters.add("pageType", BranchPageType.CREATE);

        wicketTester.startPage(BranchPage.class, pageParameters);

        URL url = Thread.currentThread().getContextClassLoader().getResource("logo/acba.png");
        assertNotNull(url);
        File file = new File(url.toURI());

        FormTester formTester = wicketTester.newFormTester("branchForm");
        formTester.setFile("fileUploadField", file, "jpeg");


        formTester.setValue("headOffice", true);

        formTester.setValue("branchNameContainer:fieldListViewContainer:fieldListView:0:field", "a");
        formTester.setValue("branchNameContainer:fieldListViewContainer:fieldListView:1:field", "a");
        formTester.setValue("branchNameContainer:fieldListViewContainer:fieldListView:2:field", "a");

        formTester.select("region", 0);
        formTester.select("city", 0);
        formTester.select("district", 0);


        formTester.setValue("branchAddressContainer:fieldListViewContainer:fieldListView:0:field", "a");
        formTester.setValue("branchAddressContainer:fieldListViewContainer:fieldListView:1:field", "a");
        formTester.setValue("branchAddressContainer:fieldListViewContainer:fieldListView:2:field", "a");

        formTester.setValue("zipCode", "123");
        formTester.setValue("phone1", "phone1");
        formTester.setValue("phone2", "phone2");
        formTester.setValue("email", "email");


        formTester.setValue("isFullDay", true);


        formTester.setValue("workingHoursPanel:workingHoursListView:0:day", true);

        formTester.setValue("workingHoursPanel:workingHoursListView:0:startTime","09:00");
        formTester.setValue("workingHoursPanel:workingHoursListView:0:endTime","18:00");

        formTester.select("status", 0);
        formTester.setValue("longitude", "1234");
        formTester.setValue("latitude", "1234");

        wicketTester.clickLink("branchForm:saveBranchAjaxLink");

        wicketTester.assertRenderedPage(BankDetailsPage.class);*/
    }


}
