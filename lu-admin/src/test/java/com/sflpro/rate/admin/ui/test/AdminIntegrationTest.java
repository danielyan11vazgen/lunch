package com.sflpro.rate.admin.ui.test;

import com.sflpro.rate.admin.ui.AdminPortalApplication;
import com.sflpro.rate.test.integration.AbstractIntegrationTest;
import org.apache.wicket.util.tester.WicketTester;
import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/9/14
 * Time: 6:43 PM
 */
@ContextConfiguration(locations = {"classpath:applicationContext-admin-test.xml"})
public abstract class AdminIntegrationTest extends AbstractIntegrationTest {

    protected WicketTester wicketTester;

    @Autowired
    protected AdminPortalApplication application;

    public AdminIntegrationTest() {

    }

    @Before
    public void init() throws URISyntaxException, SQLException, DatabaseUnitException, IOException {
        super.init();
        wicketTester = new WicketTester(application);
    }

}
