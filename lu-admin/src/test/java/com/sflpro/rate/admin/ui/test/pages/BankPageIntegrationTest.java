package com.sflpro.rate.admin.ui.test.pages;

import com.sflpro.rate.admin.ui.pages.organization.bank.BankDetailsPage;
import com.sflpro.rate.admin.ui.pages.organization.bank.BankPage;
import com.sflpro.rate.admin.ui.test.AdminIntegrationTest;
import com.sflpro.rate.models.datatypes.OrganizationStatus;
import com.sflpro.rate.models.dto.Organization;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.protocol.http.mock.MockHttpServletRequest;
import org.apache.wicket.util.file.File;
import org.apache.wicket.util.tester.FormTester;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.net.URL;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/9/14
 * Time: 7:14 PM
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-admin-test.xml"})
@Transactional
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class BankPageIntegrationTest extends AdminIntegrationTest {

    @Test
    public void testBankPageAddNewBankWhenNameFieldsAreEmpty() throws Exception {
        wicketTester.startPage(BankPage.class);

        wicketTester.clickLink("addOrganizationAjaxLink");

        URL url = Thread.currentThread().getContextClassLoader().getResource("logo/acba.png");
        assertNotNull(url);
        File file = new File(url.toURI());

        FormTester formTester = wicketTester.newFormTester("bankModalWindow:bankModalWindowForm");

        formTester.setValue("bankWebsiteUrl", "url");
        formTester.setValue("bankLicense", "license");
        formTester.select("bankStatus", 0);
        formTester.setFile("bankLogoUploadField", file, "jpeg");

        wicketTester.clickLink("bankModalWindow:saveModalWindowAjaxLink");

        wicketTester.assertErrorMessages("'Name (HY)' is required.", "'Name (EN)' is required.", "'Name (RU)' is required.");
        wicketTester.assertRenderedPage(BankPage.class);
    }

    @Test
    public void testBankPageAddNewBank() throws Exception {
        wicketTester.startPage(BankPage.class);

        wicketTester.clickLink("addOrganizationAjaxLink");

        URL url = Thread.currentThread().getContextClassLoader().getResource("logo/acba.png");
        assertNotNull(url);
        File file = new File(url.toURI());

        FormTester formTester = wicketTester.newFormTester("bankModalWindow:bankModalWindowForm");

        formTester.setValue("dynamicLanguagePanel:fieldListViewContainer:fieldListView:0:field", "name hy");
        formTester.setValue("dynamicLanguagePanel:fieldListViewContainer:fieldListView:1:field", "name en");
        formTester.setValue("dynamicLanguagePanel:fieldListViewContainer:fieldListView:2:field", "name ru");

        formTester.setValue("bankWebsiteUrl", "url");
        formTester.setValue("bankLicense", "license");
        formTester.select("bankStatus", 0);
        formTester.setFile("bankLogoUploadField", file, "jpeg");

        wicketTester.clickLink("bankModalWindow:saveModalWindowAjaxLink");

        DataView dataView = (DataView) wicketTester.getLastRenderedPage().get("dataViewContainer:bankDataView");

        assertEquals(4, dataView.getDataProvider().size());

        Iterator iterator = dataView.getDataProvider().iterator(0, dataView.getDataProvider().size());

        Organization o = (Organization) iterator.next();
        assertEquals("name en", o.getName());
        assertEquals("http://url", o.getUrl());
        assertEquals("license", o.getLicense());
        assertEquals(OrganizationStatus.ACTIVE, o.getStatus());


        wicketTester.assertRenderedPage(BankPage.class);
    }

    @Test
    public void testNameFilter() throws Exception {
        wicketTester.startPage(BankPage.class);

        ((MockHttpServletRequest) wicketTester.getRequestCycle().getRequest().getContainerRequest())
                .setParameter("filterContainer:nameFilter", "a");
        wicketTester.executeAjaxEvent("filterContainer:nameFilter", "change");

        DataView dataView = (DataView) wicketTester.getComponentFromLastRenderedPage("dataViewContainer:bankDataView");

        assertEquals(1, dataView.getDataProvider().size());

        wicketTester.assertRenderedPage(BankPage.class);
    }

    @Test
    public void testBranchFilter() throws Exception {
        wicketTester.startPage(BankPage.class);

        ((MockHttpServletRequest) wicketTester.getRequestCycle().getRequest().getContainerRequest())
                .setParameter("filterContainer:branchFilter", "5");
        wicketTester.executeAjaxEvent("filterContainer:branchFilter", "change");

        DataView dataView = (DataView) wicketTester.getComponentFromLastRenderedPage("dataViewContainer:bankDataView");

        assertEquals(0, dataView.getDataProvider().size());

        wicketTester.assertRenderedPage(BankPage.class);
    }

    @Test
    public void testStatusFilter() throws Exception {
        wicketTester.startPage(BankPage.class);

        ((MockHttpServletRequest) wicketTester.getRequestCycle().getRequest().getContainerRequest())
                .setParameter("filterContainer:statusFilter", OrganizationStatus.ACTIVE.name());
        wicketTester.executeAjaxEvent("filterContainer:statusFilter", "change");

        DataView dataView = (DataView) wicketTester.getComponentFromLastRenderedPage("dataViewContainer:bankDataView");

        assertEquals(3, dataView.getDataProvider().size());

        wicketTester.assertRenderedPage(BankPage.class);
    }

    @Test
    public void testToDetailsPage() throws Exception {
        wicketTester.startPage(BankPage.class);

        wicketTester.clickLink("dataViewContainer:bankDataView:2:viewBankDetails", true);

        wicketTester.assertRenderedPage(BankDetailsPage.class);
    }

}
