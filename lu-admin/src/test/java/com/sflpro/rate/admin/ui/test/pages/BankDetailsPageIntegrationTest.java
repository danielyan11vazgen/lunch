package com.sflpro.rate.admin.ui.test.pages;

import com.sflpro.rate.admin.ui.pages.organization.bank.BankDetailsPage;
import com.sflpro.rate.admin.ui.pages.organization.bank.BankPage;
import com.sflpro.rate.admin.ui.test.AdminIntegrationTest;
import com.sflpro.rate.models.datatypes.OrganizationStatus;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.util.tester.FormTester;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/16/14
 * Time: 2:28 PM
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-admin-test.xml"})
@Transactional
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class BankDetailsPageIntegrationTest extends AdminIntegrationTest {

    @Test
    public void testBankDetailsPageWhenLogoAvailable() throws Exception {

        PageParameters pageParameters = new PageParameters();
        pageParameters.add("organizationId", "3");

        wicketTester.startPage(BankDetailsPage.class, pageParameters);

        wicketTester.assertRenderedPage(BankDetailsPage.class);
    }

    @Test
    public void testBankDetailsPageWhenLogoNotAvailable() throws Exception {

        PageParameters pageParameters = new PageParameters();
        pageParameters.add("organizationId", "2");

        wicketTester.startPage(BankDetailsPage.class, pageParameters);

        wicketTester.assertRenderedPage(BankDetailsPage.class);
    }

    @Test
    public void testBackToBankPageAjaxLink() throws Exception {
        PageParameters pageParameters = new PageParameters();
        pageParameters.add("organizationId", "2");

        wicketTester.startPage(BankDetailsPage.class, pageParameters);

        wicketTester.clickLink("backLink", true);

        wicketTester.assertRenderedPage(BankPage.class);
    }

    @Test
    public void testEditDetailsAjaxLink() throws Exception {
        PageParameters pageParameters = new PageParameters();
        pageParameters.add("organizationId", "2");

        wicketTester.startPage(BankDetailsPage.class, pageParameters);

        wicketTester.clickLink("editDetailsAjaxLink", true);

        TextField textField1 = (TextField) wicketTester.getComponentFromLastRenderedPage("bankModalWindow:bankModalWindowForm:dynamicLanguagePanel:fieldListViewContainer:fieldListView:0:field");
        TextField textField2 = (TextField) wicketTester.getComponentFromLastRenderedPage("bankModalWindow:bankModalWindowForm:dynamicLanguagePanel:fieldListViewContainer:fieldListView:1:field");
        TextField textField3 = (TextField) wicketTester.getComponentFromLastRenderedPage("bankModalWindow:bankModalWindowForm:dynamicLanguagePanel:fieldListViewContainer:fieldListView:2:field");

        TextField textField4 = (TextField) wicketTester.getComponentFromLastRenderedPage("bankModalWindow:bankModalWindowForm:bankWebsiteUrl");
        TextField textField5 = (TextField) wicketTester.getComponentFromLastRenderedPage("bankModalWindow:bankModalWindowForm:bankLicense");
        DropDownChoice dropDownChoice6 = (DropDownChoice) wicketTester.getComponentFromLastRenderedPage("bankModalWindow:bankModalWindowForm:bankStatus");

        assertEquals("b", textField1.getModelObject());
        assertEquals("b", textField2.getModelObject());
        assertEquals("b", textField3.getModelObject());

        assertEquals("http://b", textField4.getModelObject());
        assertEquals("b", textField5.getModelObject());
        assertEquals(OrganizationStatus.ACTIVE, dropDownChoice6.getModelObject());


        FormTester form = wicketTester.newFormTester("bankModalWindow:bankModalWindowForm");

        form.setValue("dynamicLanguagePanel:fieldListViewContainer:fieldListView:0:field", "d");
        form.setValue("dynamicLanguagePanel:fieldListViewContainer:fieldListView:1:field", "d");
        form.setValue("dynamicLanguagePanel:fieldListViewContainer:fieldListView:2:field", "d");

        form.setValue("bankWebsiteUrl", "d");
        form.setValue("bankLicense", "d");
        form.select("bankStatus", 1);

        wicketTester.clickLink("bankModalWindow:saveModalWindowAjaxLink", true);

        wicketTester.clickLink("editDetailsAjaxLink", true);

        TextField textField11 = (TextField) wicketTester.getComponentFromLastRenderedPage("bankModalWindow:bankModalWindowForm:dynamicLanguagePanel:fieldListViewContainer:fieldListView:0:field");
        TextField textField21 = (TextField) wicketTester.getComponentFromLastRenderedPage("bankModalWindow:bankModalWindowForm:dynamicLanguagePanel:fieldListViewContainer:fieldListView:1:field");
        TextField textField31 = (TextField) wicketTester.getComponentFromLastRenderedPage("bankModalWindow:bankModalWindowForm:dynamicLanguagePanel:fieldListViewContainer:fieldListView:2:field");

        TextField textField41 = (TextField) wicketTester.getComponentFromLastRenderedPage("bankModalWindow:bankModalWindowForm:bankWebsiteUrl");
        TextField textField51 = (TextField) wicketTester.getComponentFromLastRenderedPage("bankModalWindow:bankModalWindowForm:bankLicense");
        DropDownChoice dropDownChoice61 = (DropDownChoice) wicketTester.getComponentFromLastRenderedPage("bankModalWindow:bankModalWindowForm:bankStatus");

        assertEquals("d", textField11.getModelObject());
        assertEquals("d", textField21.getModelObject());
        assertEquals("d", textField31.getModelObject());

        assertEquals("http://d", textField41.getModelObject());
        assertEquals("d", textField51.getModelObject());
        assertEquals(OrganizationStatus.INACTIVE, dropDownChoice61.getModelObject());


        wicketTester.assertRenderedPage(BankDetailsPage.class);
    }

    @Test
    public void testEditDetailsAjaxLinkWhenFieldsAreEmpty() throws Exception {
        PageParameters pageParameters = new PageParameters();
        pageParameters.add("organizationId", "2");

        wicketTester.startPage(BankDetailsPage.class, pageParameters);

        wicketTester.clickLink("editDetailsAjaxLink", true);

        TextField textField1 = (TextField) wicketTester.getComponentFromLastRenderedPage("bankModalWindow:bankModalWindowForm:dynamicLanguagePanel:fieldListViewContainer:fieldListView:0:field");
        TextField textField2 = (TextField) wicketTester.getComponentFromLastRenderedPage("bankModalWindow:bankModalWindowForm:dynamicLanguagePanel:fieldListViewContainer:fieldListView:1:field");
        TextField textField3 = (TextField) wicketTester.getComponentFromLastRenderedPage("bankModalWindow:bankModalWindowForm:dynamicLanguagePanel:fieldListViewContainer:fieldListView:2:field");

        TextField textField4 = (TextField) wicketTester.getComponentFromLastRenderedPage("bankModalWindow:bankModalWindowForm:bankWebsiteUrl");
        TextField textField5 = (TextField) wicketTester.getComponentFromLastRenderedPage("bankModalWindow:bankModalWindowForm:bankLicense");
        DropDownChoice dropDownChoice6 = (DropDownChoice) wicketTester.getComponentFromLastRenderedPage("bankModalWindow:bankModalWindowForm:bankStatus");

        assertEquals("b", textField1.getModelObject());
        assertEquals("b", textField2.getModelObject());
        assertEquals("b", textField3.getModelObject());

        assertEquals("http://b", textField4.getModelObject());
        assertEquals("b", textField5.getModelObject());
        assertEquals(OrganizationStatus.ACTIVE, dropDownChoice6.getModelObject());


        FormTester form = wicketTester.newFormTester("bankModalWindow:bankModalWindowForm");

        form.setValue("dynamicLanguagePanel:fieldListViewContainer:fieldListView:0:field", "");
        form.setValue("dynamicLanguagePanel:fieldListViewContainer:fieldListView:1:field", "");
        form.setValue("dynamicLanguagePanel:fieldListViewContainer:fieldListView:2:field", "");

        form.setValue("bankWebsiteUrl", "d");
        form.setValue("bankLicense", "d");
        form.select("bankStatus", 1);

        wicketTester.clickLink("bankModalWindow:saveModalWindowAjaxLink", true);

        wicketTester.assertErrorMessages("'Name (HY)' is required.", "'Name (EN)' is required.", "'Name (RU)' is required.");

        wicketTester.assertRenderedPage(BankDetailsPage.class);
    }

    @Test
    public void testSetRatesAjaxLink() throws Exception {
        PageParameters pageParameters = new PageParameters();
        pageParameters.add("organizationId", "2");

        wicketTester.startPage(BankDetailsPage.class, pageParameters);

        wicketTester.clickLink("setRatesAjaxLink", true);

        wicketTester.assertRenderedPage(BankDetailsPage.class);
    }

    @Test
    public void testParsersAjaxLink() throws Exception {
        PageParameters pageParameters = new PageParameters();
        pageParameters.add("organizationId", "2");

        wicketTester.startPage(BankDetailsPage.class, pageParameters);

        wicketTester.clickLink("parsersAjaxLink", true);

        wicketTester.assertRenderedPage(BankDetailsPage.class);
    }

    @Test
    public void testAddBranchAjaxLink() throws Exception {
        /*PageParameters pageParameters = new PageParameters();
        pageParameters.add("organizationId", "2");

        wicketTester.startPage(BankDetailsPage.class, pageParameters);

        wicketTester.clickLink("addBranchAjaxLink", true);

        wicketTester.assertRenderedPage(BranchPage.class);*/
    }
}
