package com.sflpro.rate.repository.system_translation.impl;

import com.sflpro.rate.repository.system_translation.SystemTranslationMethod;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * User: Mher Sargsyan
 * Company: SFL LLC
 * Date: 7/17/2014
 * Time: 5:45 PM
 */
@Repository
public class SystemTranslationRepositoryImpl implements SystemTranslationMethod {

    @PersistenceContext
    private EntityManager em;

    /**
     * {@inheritDoc}
     */
    @Override
    public List getSystemTranslationsForLanguage(String language) {

        final String sql =
                "SELECT " +
                        "   lv.value AS translation_value, \n" +
                        "   st.content_id AS content_id \n" +
                        "FROM \n" +
                        "   language_value lv \n" +
                        "   INNER JOIN language_key lk ON (lv.language_key_id = lk.id) \n" +
                        "   INNER JOIN \"language\" l ON (lv.language_id = l.id ) \n" +
                        "   INNER JOIN system_translation st ON (lk.entity_id = st.id) \n" +
                        "WHERE \n" +
                        "   l.code = :language AND \n" +
                        "   st.deleted IS NULL \n" +
                        "ORDER BY st.content_id";


        javax.persistence.Query jpaQuery = em.createNativeQuery(sql);

        jpaQuery.setParameter("language", language.toUpperCase());

        return jpaQuery.getResultList();
    }
}
