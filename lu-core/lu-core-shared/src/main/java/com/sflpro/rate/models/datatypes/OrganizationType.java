package com.sflpro.rate.models.datatypes;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/18/14
 * Time: 4:18 PM
 */

public enum OrganizationType {
    BANK, EXCHANGE, CB, CREDIT, INVESTMENT
}
