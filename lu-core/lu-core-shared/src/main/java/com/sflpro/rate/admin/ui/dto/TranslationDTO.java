package com.sflpro.rate.admin.ui.dto;

import java.io.Serializable;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/10/14
 * Time: 5:51 PM
 */
public class TranslationDTO implements Serializable {

    private long languageId;

    private String languageCode;

    private String fieldValue;

    public long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(long languageId) {
        this.languageId = languageId;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

}
