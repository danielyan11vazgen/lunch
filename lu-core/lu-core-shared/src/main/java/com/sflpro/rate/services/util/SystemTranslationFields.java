package com.sflpro.rate.services.util;

/**
 * User: Mher Sargsyan
 * Company: SFL LLC
 * Date: 7/17/2014
 * Time: 7:04 PM
 */
public enum SystemTranslationFields {
    TRANSLATION_VALUE(0),
    CONTENT_ID(1);

    private final int fieldNumber;

    SystemTranslationFields(int fieldNumber) {
        this.fieldNumber = fieldNumber;
    }

    public int getFieldNumber() {
        return this.fieldNumber;
    }
}
