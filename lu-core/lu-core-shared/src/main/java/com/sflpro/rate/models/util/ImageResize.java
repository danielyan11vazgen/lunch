package com.sflpro.rate.models.util;

import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/3/14
 * Time: 11:01 AM
 */
public class ImageResize {

    public static final int IMG_WIDTH = 160;
    public static final int IMG_HEIGHT = 115;

    public static final int THUMB_WIDTH = 120;
    public static final int THUMB_HEIGHT = 86;

    public static final int ADMIN_THUMB_WIDTH = 51;
    public static final int ADMIN_THUMB_HEIGHT = 41;

    public static final String IMAGE_PNG = "png";
    public static final String IMAGE_JPG = "jpg";

    public static final String RESIZED_IMAGE_POSTFIX = "_%sx%d";

    private static final String IMAGE_PATTERN = "((\\.(?i)(/bmp|jpg|gif|png|PNG|JPG|GIF|BMP))$)";

    private ImageResize() {
    }


    /**
     * Create resized image with custom size
     *
     * @param originalImageFile original file path
     * @param resizedImagePath resized file path
     */
    public static void createImageResized(File originalImageFile, String resizedImagePath) throws IOException {
        BufferedImage originalImage = ImageIO.read(originalImageFile);

        BufferedImage resizeImage;
        if (originalImage.getWidth() >= originalImage.getHeight()) {
            resizeImage = ImageResize.resizeImage(originalImage, IMG_WIDTH, IMG_HEIGHT, Scalr.Mode.FIT_TO_WIDTH);
        } else {
            resizeImage = ImageResize.resizeImage(originalImage, IMG_WIDTH, IMG_HEIGHT, Scalr.Mode.FIT_TO_HEIGHT);
        }

        ImageIO.write(resizeImage, detectImageFormat(originalImageFile), new File(resizedImagePath));
    }

    /**
     * Create thumbnail from original image for item lists
     *
     * @param originalImageFile original file path
     * @param resizedImagePath resized file path
     */
    public static void createThumbnail(File originalImageFile, String resizedImagePath) throws IOException {
        BufferedImage originalImage = ImageIO.read(originalImageFile);

        BufferedImage resizeImage;
        if (originalImage.getWidth() >= originalImage.getHeight()) {
            resizeImage = ImageResize.resizeImage(originalImage, THUMB_WIDTH, THUMB_HEIGHT, Scalr.Mode.FIT_TO_WIDTH);
        } else {
            resizeImage = ImageResize.resizeImage(originalImage, THUMB_WIDTH, THUMB_HEIGHT, Scalr.Mode.FIT_TO_HEIGHT);
        }

        ImageIO.write(resizeImage, detectImageFormat(originalImageFile), new File(resizedImagePath));
    }

    /**
     * Create thumbnail from original image for item lists
     *
     * @param originalImageFile original file path
     * @param resizedImagePath resized file path
     */
    public static void createAdminThumbnail(File originalImageFile, String resizedImagePath) throws IOException {
        BufferedImage originalImage = ImageIO.read(originalImageFile);

        BufferedImage resizeImage;
        if (originalImage.getWidth() >= originalImage.getHeight()) {
            resizeImage = ImageResize.resizeImage(originalImage, ADMIN_THUMB_WIDTH, ADMIN_THUMB_HEIGHT, Scalr.Mode.FIT_TO_WIDTH);
        } else {
            resizeImage = ImageResize.resizeImage(originalImage, ADMIN_THUMB_WIDTH, ADMIN_THUMB_HEIGHT, Scalr.Mode.FIT_TO_HEIGHT);
        }

        ImageIO.write(resizeImage, detectImageFormat(originalImageFile), new File(resizedImagePath));
    }

    /**
     * Resize image with scalr
     *
     * @param originalImage original image
     * @param width         image width
     * @param height        image height
     * @return resizedImage
     */
    private static BufferedImage resizeImage(BufferedImage originalImage, int width, int height, Scalr.Mode fitMode) {
        return Scalr.resize(originalImage, Scalr.Method.ULTRA_QUALITY, fitMode, width, height, Scalr.OP_ANTIALIAS);
    }

    /**
     * Resize image (may cause quality loss)
     *
     * @param originalImage original image
     * @param type          The ColorSpace for the image is the default sRGB space.
     * @param width         image width
     * @param height        image height
     * @return resizedImage
     */
    private static BufferedImage resizeImageWithHint(BufferedImage originalImage, int type, int width, int height) {

        BufferedImage resizedImage = new BufferedImage(width, height, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, width, height, null);
        g.dispose();
        g.setComposite(AlphaComposite.Src);

        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        return resizedImage;
    }

    /**
     * Detect original image format (e.g. jpg, png, gif,..)
     *
     * @param file Original file
     * @return imageFormat
     */
    private static String detectImageFormat(File file) throws IOException {
        String imageFormat = IMAGE_PNG;
        ImageInputStream iis = ImageIO.createImageInputStream(file);

        Iterator<ImageReader> readerIterator = ImageIO.getImageReaders(iis);

        if (!readerIterator.hasNext()) {
            return imageFormat;
        }

        ImageReader reader = readerIterator.next();
        imageFormat = reader.getFormatName();
        iis.close();
        return imageFormat;
    }

    /**
     * Get resized image name (depends on size)
     *
     * @param originalFilePath file path
     * @return resizedImageName
     */
    public static String getResizedImagePath(String originalFilePath, int width, int height) {
        final Pattern imagePattern = Pattern.compile(IMAGE_PATTERN);

        String resizedImagePath = originalFilePath;
        Matcher matcher = imagePattern.matcher(resizedImagePath);
        if (matcher.find()) {
            resizedImagePath = originalFilePath.replaceAll(IMAGE_PATTERN, String.format(RESIZED_IMAGE_POSTFIX, width, height) + matcher.group(1));
        }

        return resizedImagePath;
    }


}
