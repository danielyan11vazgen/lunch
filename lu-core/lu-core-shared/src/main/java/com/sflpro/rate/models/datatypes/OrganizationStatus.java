package com.sflpro.rate.models.datatypes;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/19/14
 * Time: 8:26 PM
 */
public enum OrganizationStatus {
    ALL,
    ACTIVE,
    INACTIVE,
    PENDING
}
