package com.sflpro.rate.models.datatypes;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/19/14
 * Time: 5:04 PM
 */
public enum LanguageKeyType {
    USER_GENERATED,
    SYSTEM
}
