package com.sflpro.rate.admin.ui.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * User: Mher Sargsyan
 * Company: SFL LLC
 * Date: 7/15/2014
 * Time: 11:27 AM
 */
public class SystemTranslationDTO implements Serializable {

    private Long contentId;

    private Map<String, String> translationValues;

    public Long getContentId() {
        return contentId;
    }

    public void setContentId(Long contentId) {
        this.contentId = contentId;
    }

    public Map<String, String> getTranslationValues() {
        return translationValues;
    }

    /**
     * Add translation value
     *
     * @param languageCode unique language code
     * @param value translated string value
     */
    public void addTranslationValue(String languageCode, String value) {
        if(this.translationValues == null) {
            this.translationValues = new HashMap<>();
        }
        this.translationValues.put(languageCode, value);
    }
}
