package com.sflpro.rate.repository.language;

import com.sflpro.rate.models.dto.Language;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/27/14
 * Time: 6:53 PM
 */
@Repository
public interface LanguageRepository extends JpaRepository<Language, Long> {


    Language findByIdAndDeletedIsNull(long id);

    List<Language> findByDeletedIsNull(Sort orders);

    Language findByIsSystemDefaultAndDeletedIsNull(boolean isSystemDefault);
}
