package com.sflpro.rate.repository.language_value;

import com.sflpro.rate.models.dto.LanguageValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/10/14
 * Time: 7:53 PM
 */
@Repository
public interface LanguageValueRepository extends JpaRepository<LanguageValue, Long> {

    /**
     * Find language value by language key id and language id
     *
     * @param languageKeyId language key id
     * @return found language value
     */
    LanguageValue findByLanguageKey_IdAndLanguage_Id(long languageKeyId, long languageId);

}
