package com.sflpro.rate.repository.language_key;

import com.sflpro.rate.models.datatypes.LanguageKeyType;
import com.sflpro.rate.models.dto.LanguageKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/10/14
 * Time: 6:14 PM
 */
@Repository
public interface LanguageKeyRepository extends JpaRepository<LanguageKey, Long> {

    /**
     * Find all language keys with type "system" with filter
     *
     * @param languageKeyType type of language key
     * @param entityId id number of system translation
     * @return list of language keys
     */
    List<LanguageKey> findByDeletedIsNullAndLanguageKeyTypeAndEntityId(LanguageKeyType languageKeyType, Long entityId);

    /**
     * Find all language keys with type "system"
     *
     * @param languageKeyType type of language key
     * @return list of language keys
     */
    List<LanguageKey> findByDeletedIsNullAndLanguageKeyType(LanguageKeyType languageKeyType);

    LanguageKey findByEntityNameAndEntityIdAndEntityFieldAndLanguageKeyTypeAndDeletedIsNull(String entityName, Long entityId, String entityField, LanguageKeyType languageKeyType);

}
