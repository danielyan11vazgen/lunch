package com.sflpro.rate.repository.organization;

import com.sflpro.rate.models.datatypes.OrganizationType;
import com.sflpro.rate.models.dto.Organization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/27/14
 * Time: 6:40 PM
 */
@Repository
public interface OrganizationRepository extends JpaRepository<Organization, Long> {

    /**
     * Find Organization by id
     *
     * @param id organization id
     * @return found Organization
     */
    Organization findById(long id);

    /**
     * Find Organization by its name when deleted <u>is</u> null
     *
     * @param name organization name
     * @return found Organization
     */
    Organization findByNameAndDeletedIsNull(String name);


    /**
     * Find Organization by its name when deleted is <u>not</u> null
     *
     * @param name organization name
     * @return found Organization
     */
    Organization findByNameAndDeletedIsNotNull(String name);


    /**
     * Find Count of organizations
     *
     * @param name organization name
     * @return organizations count
     */
    @Query("SELECT \n" +
            "   count (o) \n" +
            "FROM \n" +
            "   Organization o \n" +
            "WHERE \n" +
            "   o.type = :organizationType and \n" +
            "   (UPPER(o.name) LIKE UPPER(CONCAT('%', :name, '%'))) AND \n" +
            "   ((:branches <> -1 and o.branches.size = :branches) OR (:branches = -1 )) AND \n" +
            "   ((:status = '') OR (UPPER(o.status) = UPPER(:status)))")
    long findCount(@Param("name") String name, @Param("branches") Integer branches, @Param("status") String status, @Param("organizationType") OrganizationType organizationType);

}
