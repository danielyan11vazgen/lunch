package com.sflpro.rate.repository.district;

import com.sflpro.rate.models.dto.District;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/20/14
 * Time: 4:25 AM
 */
@Repository
public interface DistrictRepository extends JpaRepository<District, Long> {

    /**
     * Get all not deleted cities
     *
     * @return list of cities
     */
    List<District> findByDeletedIsNull(Sort orders);


}
