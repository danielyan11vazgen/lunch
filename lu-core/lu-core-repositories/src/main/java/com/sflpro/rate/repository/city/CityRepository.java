package com.sflpro.rate.repository.city;

import com.sflpro.rate.models.dto.City;
import com.sflpro.rate.models.dto.Region;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/20/14
 * Time: 3:14 AM
 */
@Repository
public interface CityRepository extends JpaRepository<City, Long> {


    /**
     * Get all not deleted cities
     *
     * @return list of cities
     */
    List<City> findByDeletedIsNull(Sort orders);


    /**
     * Find all not deleted cities by region
     *
     * @param region region
     * @return list of cities
     */
    List<City> findByRegionAndDeletedIsNull(Region region);

}
