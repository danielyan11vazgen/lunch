package com.sflpro.rate.repository.system_translation;

import com.sflpro.rate.models.dto.SystemTranslation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * User: Mher Sargsyan
 * Company: SFL LLC
 * Date: 7/16/2014
 * Time: 1:20 PM
 */
public interface SystemTranslationRepository extends JpaRepository<SystemTranslation, Long>, SystemTranslationMethod {

    /**
     * Return list of all available systems translations
     *
     * @return list of system translations
     */
    List<SystemTranslation> findByDeletedIsNull();

    /**
     * Return list of all available systems translations matching criteria
     *
     * @param contentId id number of translatable content
     * @return List<SystemTranslation>
     */
    List<SystemTranslation> findByDeletedIsNullAndContentId(long contentId);


    /**
     * Find system translation y id and not deleted
     *
     * @param id system translation unique id
     * @return system translation entity
     */
    SystemTranslation findByIdAndDeletedIsNull(long id);

}
