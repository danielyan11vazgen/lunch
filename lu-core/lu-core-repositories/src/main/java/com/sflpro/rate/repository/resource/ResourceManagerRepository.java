package com.sflpro.rate.repository.resource;

import com.sflpro.rate.models.dto.Resource;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/3/14
 * Time: 10:55 AM
 */
public interface ResourceManagerRepository extends JpaRepository<Resource, Long> {

}
