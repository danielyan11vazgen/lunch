package com.sflpro.rate.repository.branch;

import com.sflpro.rate.models.dto.Branch;
import com.sflpro.rate.models.dto.Organization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/20/14
 * Time: 3:39 PM
 */
@Repository
public interface BranchRepository extends JpaRepository<Branch, Long> {


    /**
     * Find Branch by id
     *
     * @param branchId branch id
     * @return found branch
     */
    Branch findByIdAndDeletedIsNull(long branchId);


    /**
     * Find Head Office
     *
     * @param organizationId organization id
     * @param isHead         is head
     * @return null if no head office found otherwise HeadOffice
     */
    Branch findByOrganization_IdAndIsHeadAndDeletedIsNull(long organizationId, boolean isHead);

    /**
     * Find Branch by name and organization and deleted is null
     *
     * @param name         branch name
     * @param organization organization
     * @return found branch
     */
    Branch findByNameAndOrganizationAndDeletedIsNotNull(String name, Organization organization);


    /**
     * Get count of all branches for organization matching filter
     *
     * @param organizationId organization id
     * @param name           name filter
     * @param region         region name filter
     * @param city           city name filter
     * @param district       district name filter
     * @param address        branch address filter
     * @param status         branch status filter
     * @return number of branches
     */
    @Query("select \n"+
            "   count(b) \n"+
            "from \n"+
            "   Branch b \n"+
            "   LEFT JOIN b.region r \n"+
            "   LEFT JOIN b.city c \n"+
            "   LEFT JOIN b.district d \n"+
            "where \n"+
            "   b.organization.id = :organizationId and \n"+
            "   upper(b.name) like upper(concat( '%', :name, '%')) and \n"+
            "   ((b.address is null and :address ='') or upper(b.address) like upper(concat( '%', :address, '%'))) and \n"+
            "   upper(r.name) like upper(concat( '%', :region, '%')) and \n"+
            "   ((c.name is null and :city = '') or upper(c.name) like upper(concat( '%', :city, '%'))) and \n"+
            "   ((d.name is null and :district ='') or upper(d.name) like upper(concat( '%', :district, '%'))) and \n"+
            "   ((:status = '') OR (UPPER(b.status) = UPPER(:status)))")
    long findCount(@Param("organizationId") long organizationId, @Param("name") String name, @Param("region") String region, @Param("city") String city, @Param("district") String district, @Param("address") String address, @Param("status") String status);

}
