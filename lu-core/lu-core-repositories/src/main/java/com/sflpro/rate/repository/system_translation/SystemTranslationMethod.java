package com.sflpro.rate.repository.system_translation;

import java.util.List;

/**
 * User: Mher Sargsyan
 * Company: SFL LLC
 * Date: 7/17/2014
 * Time: 5:39 PM
 */
public interface SystemTranslationMethod {

    /**
     * Get system translations for language
     *
     * @param language language parameter
     * @return map of system translations
     */
    List getSystemTranslationsForLanguage(String language);

}
