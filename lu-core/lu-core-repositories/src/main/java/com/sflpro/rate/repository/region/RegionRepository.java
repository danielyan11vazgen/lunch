package com.sflpro.rate.repository.region;

import com.sflpro.rate.models.dto.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/20/14
 * Time: 1:07 AM
 */
@Repository
public interface RegionRepository extends JpaRepository<Region, Long>{

    /**
     * Find all not deleted regions
     * @return list of regions
     */
    List<Region> findByDeletedIsNull();

}
