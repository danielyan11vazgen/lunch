package com.sflpro.rate.repository.working_hour;

import com.sflpro.rate.admin.ui.dto.WorkingHourCode;
import com.sflpro.rate.models.dto.Branch;
import com.sflpro.rate.models.dto.WorkingHour;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/20/14
 * Time: 3:41 PM
 */
@Repository
public interface WorkingHourRepository extends JpaRepository<WorkingHour, Long> {


    /**
     * Find working hour by code and branch
     *
     * @param code   working hour code
     * @param branch branch
     * @return found working hour
     */
    WorkingHour findByCodeAndBranchAndDeletedIsNull(WorkingHourCode code, Branch branch);


    /**
     * Find Working Hour by code and branch id
     *
     * @param code     working hour code
     * @param branchId branch id
     * @return found working hour
     */
    WorkingHour findByCodeAndBranch_IdAndDeletedIsNull(WorkingHourCode code, Long branchId);


    /**
     * Find working hour by branch id
     *
     * @param branchId branch id
     * @return found working hours
     */
    List<WorkingHour> findByBranch_IdAndDeletedIsNull(Long branchId);
}
