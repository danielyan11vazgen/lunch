package com.sflpro.rate.models.dto;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/19/14
 * Time: 4:53 PM
 */
@Entity
@Table(name = "informer")
public class Informer {

    @Id
    @GeneratedValue(generator = "informer_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "informer_seq", sequenceName = "informer_seq")
    @Column(name = "id")
    private long id;

    @Column(name = "domain")
    private String domain;

    @Column(name = "activation_code")
    private String activationCode;

    @Column(name = "width")
    private double width;

    @Column(name = "height")
    private double height;

    @Column(name = "informer")
    private String informer;

    @Column(name = "bg_color")
    private String bgColor;

    @Column(name = "cb_tab")
    private String cbTab;

    @Column(name = "is_posted")
    private boolean isPosted;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "created", nullable = false, updatable = false)
    private DateTime created;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "deleted")
    private DateTime deleted;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
}
