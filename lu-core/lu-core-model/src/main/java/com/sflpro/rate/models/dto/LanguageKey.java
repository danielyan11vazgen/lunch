package com.sflpro.rate.models.dto;

import com.sflpro.rate.models.datatypes.LanguageKeyType;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/29/14
 * Time: 7:41 PM
 */
@Entity
@Table(name = "language_key",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"entity_name", "entity_id", "entity_field"}),

        },
        indexes = {
                @Index(columnList ="entity_name, entity_id")
        })
public class LanguageKey implements Serializable {

    @Id
    @GeneratedValue(generator = "language_key_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "language_key_seq", sequenceName = "language_key_seq")
    @Column(name = "id")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "language_key_type")
    private LanguageKeyType languageKeyType;

    @Column(name = "entity_id")
    private Long entityId;

    @Column(name = "entity_field")
    private String entityField;

    @Column(name = "entity_name")
    private String entityName;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "languageKey")
    private Set<LanguageValue> languageValues;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "created", nullable = false, updatable = false)
    private DateTime created;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "deleted")
    private DateTime deleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LanguageKeyType getLanguageKeyType() {
        return languageKeyType;
    }

    public void setLanguageKeyType(LanguageKeyType languageKeyType) {
        this.languageKeyType = languageKeyType;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityItemId) {
        this.entityId = entityItemId;
    }

    public String getEntityField() {
        return entityField;
    }

    public void setEntityField(String entityField) {
        this.entityField = entityField;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public Set<LanguageValue> getLanguageValues() {
        return languageValues;
    }

    public void setLanguageValues(Set<LanguageValue> languageValues) {
        this.languageValues = languageValues;
    }

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public DateTime getDeleted() {
        return deleted;
    }

    public void setDeleted(DateTime deleted) {
        this.deleted = deleted;
    }
}
