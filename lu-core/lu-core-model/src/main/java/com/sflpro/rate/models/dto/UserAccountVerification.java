package com.sflpro.rate.models.dto;

import javax.persistence.*;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/19/14
 * Time: 12:44 PM
 */
@Entity
@Table(name = "user_account_verification")
public class UserAccountVerification {

    @Id
    @GeneratedValue(generator = "user_verification_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "user_verification_seq", sequenceName = "user_verification_seq")
    @Column(name = "id")
    private long id;

    @Column(name = "activation_number")
    private String activationNumber;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getActivationNumber() {
        return activationNumber;
    }

    public void setActivationNumber(String activationNumber) {
        this.activationNumber = activationNumber;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
