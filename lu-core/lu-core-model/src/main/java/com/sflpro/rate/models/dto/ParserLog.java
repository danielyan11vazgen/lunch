package com.sflpro.rate.models.dto;

import com.sflpro.rate.models.datatypes.ParserStatus;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/19/14
 * Time: 12:13 PM
 */
@Entity
@Table(name = "parser_log")
public class ParserLog {

    @Id
    @GeneratedValue(generator = "parser_log_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "parser_log_seq", sequenceName = "parser_log_seq")
    @Column(name = "id")
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private ParserStatus status;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "start_time", nullable = false, updatable = false)
    private DateTime startTime;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "end_time")
    private DateTime endTime;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "created", nullable = false, updatable = false)
    private DateTime created;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "deleted")
    private DateTime deleted;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Parser.class)
    @JoinColumn(name = "parser_id", nullable = false)
    private Parser parser;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ParserStatus getStatus() {
        return status;
    }

    public void setStatus(ParserStatus status) {
        this.status = status;
    }

    public DateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(DateTime startTime) {
        this.startTime = startTime;
    }

    public DateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(DateTime endTime) {
        this.endTime = endTime;
    }

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public DateTime getDeleted() {
        return deleted;
    }

    public void setDeleted(DateTime deleted) {
        this.deleted = deleted;
    }

    public Parser getParser() {
        return parser;
    }

    public void setParser(Parser parser) {
        this.parser = parser;
    }
}
