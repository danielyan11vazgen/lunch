package com.sflpro.rate.models.dto;

import com.sflpro.rate.models.datatypes.ParserStatus;
import com.sflpro.rate.models.datatypes.ParserType;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.util.Set;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/19/14
 * Time: 12:07 PM
 */
@Entity
@Table(name = "parser")
public class Parser {

    @Id
    @Column(name = "organization_id")
    private long organizationId;


    @Column(name = "is_automatically_parsed")
    private boolean isAutomaticallyParsed;

    @Column(name = "email")
    private String email;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "last_run_date", nullable = false, updatable = false)
    private DateTime lastRunDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "last_run_status")
    private ParserStatus lastRunStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "parser_type")
    private ParserType parserType;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "created", nullable = false, updatable = false)
    private DateTime created;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "deleted")
    private DateTime deleted;


    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn(name = "organization_id")
    private Organization organization;

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, mappedBy = "parser")
    private Set<ParserLog> parserLogs;

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, mappedBy = "parser")
    private Set<Rate> rates;

    public long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(long organizationId) {
        this.organizationId = organizationId;
    }

    public boolean isAutomaticallyParsed() {
        return isAutomaticallyParsed;
    }

    public void setAutomaticallyParsed(boolean isAutomaticallyParsed) {
        this.isAutomaticallyParsed = isAutomaticallyParsed;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public DateTime getLastRunDate() {
        return lastRunDate;
    }

    public void setLastRunDate(DateTime lastRunDate) {
        this.lastRunDate = lastRunDate;
    }

    public ParserStatus getLastRunStatus() {
        return lastRunStatus;
    }

    public void setLastRunStatus(ParserStatus lastRunStatus) {
        this.lastRunStatus = lastRunStatus;
    }

    public ParserType getParserType() {
        return parserType;
    }

    public void setParserType(ParserType parserType) {
        this.parserType = parserType;
    }

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public DateTime getDeleted() {
        return deleted;
    }

    public void setDeleted(DateTime deleted) {
        this.deleted = deleted;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Set<ParserLog> getParserLogs() {
        return parserLogs;
    }

    public void setParserLogs(Set<ParserLog> parserLogs) {
        this.parserLogs = parserLogs;
    }

    public Set<Rate> getRates() {
        return rates;
    }

    public void setRates(Set<Rate> rates) {
        this.rates = rates;
    }
}
