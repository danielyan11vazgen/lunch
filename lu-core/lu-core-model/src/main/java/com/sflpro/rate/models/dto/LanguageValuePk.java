package com.sflpro.rate.models.dto;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/29/14
 * Time: 9:54 PM
 */
@Embeddable
public class LanguageValuePk implements Serializable {

    public Long languageKey;

    public Long language;

}
