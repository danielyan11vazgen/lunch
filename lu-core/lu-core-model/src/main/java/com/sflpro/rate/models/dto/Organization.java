package com.sflpro.rate.models.dto;

import com.sflpro.rate.models.datatypes.OrganizationStatus;
import com.sflpro.rate.models.datatypes.OrganizationType;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/18/14
 * Time: 4:16 PM
 */
@Entity
@Table(name = "organization",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"name"}),
                @UniqueConstraint(columnNames = {"license"})
        }
)
public class Organization implements Serializable {

    @Id
    @GeneratedValue(generator = "organization_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "organization_seq", sequenceName = "organization_seq")
    @Column(name = "id")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private OrganizationType type;

    @Column(name = "name")
    private String name;

    @Column(name = "license")
    private String license;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "cb_decision")
    private DateTime centralBankDecision;

    @Column(name = "url")
    private String url;

    @Column(name = "logo_url")
    private String logoUrl;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private OrganizationStatus status;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "issued_on")
    private DateTime issuedOn;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "created", nullable = false, updatable = false)
    private DateTime created;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "updated")
    private DateTime updated;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "deleted")
    private DateTime deleted;


    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER, mappedBy = "organization")
    private Set<Branch> branches;

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, mappedBy = "organization")
    private Set<RateHistory> rateHistories;

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, mappedBy = "organization")
    private Set<Rate> rate;

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, mappedBy = "organization")
    private Set<OrganizationUser> organizationUsers;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "organization")
    private Parser parser;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Currency.class)
    @JoinTable(name = "organization_currency",
            joinColumns = {
                    @JoinColumn(name = "organization_id", nullable = false, updatable = false)
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "currency_id", nullable = false, updatable = false)
            },
            uniqueConstraints = {@UniqueConstraint(columnNames = {"organization_id", "currency_id"})})
    private Set<Currency> currencies;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Resource.class)
    @JoinColumn(name = "resource_id")
    private Resource logo;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OrganizationType getType() {
        return type;
    }

    public void setType(OrganizationType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public DateTime getCentralBankDecision() {
        return centralBankDecision;
    }

    public void setCentralBankDecision(DateTime centralBankDecision) {
        this.centralBankDecision = centralBankDecision;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public OrganizationStatus getStatus() {
        return status;
    }

    public void setStatus(OrganizationStatus status) {
        this.status = status;
    }

    public DateTime getIssuedOn() {
        return issuedOn;
    }

    public void setIssuedOn(DateTime issuedOn) {
        this.issuedOn = issuedOn;
    }

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public DateTime getUpdated() {
        return updated;
    }

    public void setUpdated(DateTime updated) {
        this.updated = updated;
    }

    public DateTime getDeleted() {
        return deleted;
    }

    public void setDeleted(DateTime deleted) {
        this.deleted = deleted;
    }

    public Set<Branch> getBranches() {
        return branches;
    }

    public void setBranches(Set<Branch> branches) {
        this.branches = branches;
    }

    public Set<RateHistory> getRateHistories() {
        return rateHistories;
    }

    public void setRateHistories(Set<RateHistory> rateHistories) {
        this.rateHistories = rateHistories;
    }

    public Set<Rate> getRate() {
        return rate;
    }

    public void setRate(Set<Rate> rate) {
        this.rate = rate;
    }

    public Set<OrganizationUser> getOrganizationUsers() {
        return organizationUsers;
    }

    public void setOrganizationUsers(Set<OrganizationUser> organizationUsers) {
        this.organizationUsers = organizationUsers;
    }

    public Parser getParser() {
        return parser;
    }

    public void setParser(Parser parser) {
        this.parser = parser;
    }

    public Set<Currency> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(Set<Currency> currencies) {
        this.currencies = currencies;
    }

    public Resource getLogo() {
        return logo;
    }

    public void setLogo(Resource logo) {
        this.logo = logo;
    }

}
