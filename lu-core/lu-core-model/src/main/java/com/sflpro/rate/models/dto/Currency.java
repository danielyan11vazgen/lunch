package com.sflpro.rate.models.dto;

import com.sflpro.rate.models.datatypes.CurrencySortOrder;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.util.Set;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/19/14
 * Time: 10:53 AM
 */
@Entity
@Table(name = "currency")
public class Currency {

    @Id
    @GeneratedValue(generator = "currency_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "currency_seq", sequenceName = "currency_seq")
    @Column(name = "id")
    private long id;

    @Column(name = "iso")
    private String iso;

    @Column(name = "symbol")
    private String symbol;

    @Column(name = "code")
    private String code;

    @Column(name = "icon_url")
    private String iconUrl;

    @Column(name = "unit")
    private String unit;

    @Column(name = "is_home")
    private boolean isHome;

    @Column(name = "is_default_cell")
    private boolean isDefaultCell;

    @Column(name = "is_default_by")
    private boolean isDefaultBy;

    @Column(name = "is_base")
    private boolean isBase;

    @Enumerated(EnumType.STRING)
    @Column(name = "sort_order")
    private CurrencySortOrder sortOrder;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "created", nullable = false, updatable = false)
    private DateTime created;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "deleted")
    private DateTime deleted;

    @ManyToMany(fetch = FetchType.LAZY, targetEntity = Organization.class, mappedBy = "currencies")
    private Set<Organization> organizations;

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, mappedBy = "currency")
    private Set<RateHistory> rateHistories;

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, mappedBy = "currency")
    private Set<Rate> rates;

}
