package com.sflpro.rate.models.dto;

import com.sflpro.rate.models.datatypes.RateType;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/19/14
 * Time: 10:50 AM
 */
@Entity
@Table(name = "rate_history")
public class RateHistory {

    @Id
    @GeneratedValue(generator = "rate_history_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "rate_history_seq", sequenceName = "rate_history_seq")
    @Column(name = "id")
    private long id;


    @Enumerated(EnumType.STRING)
    @Column(name = "rate_type")
    private RateType rateType;


    @Column(name = "open_sell_balance")
    private BigDecimal openSellBalance;

    @Column(name = "close_sell_balance")
    private BigDecimal closeSellBalance;

    @Column(name = "average_sell_balance")
    private BigDecimal averageSellBalance;

    @Column(name = "open_buy_balance")
    private BigDecimal openBuyBalance;

    @Column(name = "close_buy_balance")
    private BigDecimal closeBuyBalance;

    @Column(name = "average_buy_balance")
    private BigDecimal averageBuyBalance;

    @Column(name = "count")
    private long count;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "created", nullable = false, updatable = false)
    private DateTime created;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "deleted")
    private DateTime deleted;


    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Organization.class)
    @JoinColumn(name = "organization_id", nullable = false)
    private Organization organization;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Currency.class)
    @JoinColumn(name = "currency_id", nullable = false)
    private Currency currency;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public RateType getRateType() {
        return rateType;
    }

    public void setRateType(RateType rateType) {
        this.rateType = rateType;
    }

    public BigDecimal getOpenSellBalance() {
        return openSellBalance;
    }

    public void setOpenSellBalance(BigDecimal openSellBalance) {
        this.openSellBalance = openSellBalance;
    }

    public BigDecimal getCloseSellBalance() {
        return closeSellBalance;
    }

    public void setCloseSellBalance(BigDecimal closeSellBalance) {
        this.closeSellBalance = closeSellBalance;
    }

    public BigDecimal getAverageSellBalance() {
        return averageSellBalance;
    }

    public void setAverageSellBalance(BigDecimal averageSellBalance) {
        this.averageSellBalance = averageSellBalance;
    }

    public BigDecimal getOpenBuyBalance() {
        return openBuyBalance;
    }

    public void setOpenBuyBalance(BigDecimal openBuyBalance) {
        this.openBuyBalance = openBuyBalance;
    }

    public BigDecimal getCloseBuyBalance() {
        return closeBuyBalance;
    }

    public void setCloseBuyBalance(BigDecimal closeBuyBalance) {
        this.closeBuyBalance = closeBuyBalance;
    }

    public BigDecimal getAverageBuyBalance() {
        return averageBuyBalance;
    }

    public void setAverageBuyBalance(BigDecimal averageBuyBalance) {
        this.averageBuyBalance = averageBuyBalance;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public DateTime getDeleted() {
        return deleted;
    }

    public void setDeleted(DateTime deleted) {
        this.deleted = deleted;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
