package com.sflpro.rate.models.dto;

import javax.persistence.*;
import java.io.Serializable;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/29/14
 * Time: 7:41 PM
 */
@Entity
@Table(name = "language_value")
@IdClass(LanguageValuePk.class)
public class LanguageValue implements Serializable {

    @Id
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = LanguageKey.class)
    @JoinColumn(name = "language_key_id", referencedColumnName = "id", nullable = false)
    private LanguageKey languageKey;

    @Id
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = Language.class)
    @JoinColumn(name = "language_id", referencedColumnName = "id", nullable = false)
    private Language language;

    @Column(name = "value")
    private String value;

    public LanguageKey getLanguageKey() {
        return languageKey;
    }

    public void setLanguageKey(LanguageKey languageKey) {
        this.languageKey = languageKey;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
