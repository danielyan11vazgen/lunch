package com.sflpro.rate.models.dto;

import com.sflpro.rate.admin.ui.dto.WorkingHourCode;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.LocalTime;

import javax.persistence.*;
import java.io.Serializable;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/19/14
 * Time: 10:45 AM
 */
@Entity
@Table(name = "working_hour")
public class WorkingHour implements Serializable {

    @Id
    @GeneratedValue(generator = "working_hour_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "working_hour_seq", sequenceName = "working_hour_seq")
    @Column(name = "id")
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "code")
    private WorkingHourCode code;

    @Column(name = "is_work")
    private Boolean isWork;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTime")
    @Column(name = "start_time")
    private LocalTime startTime;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTime")
    @Column(name = "end_time")
    private LocalTime endTime;


    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "created", nullable = false, updatable = false)
    private DateTime created;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "deleted")
    private DateTime deleted;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Branch.class)
    @JoinColumn(name = "branch_id", nullable = false)
    private Branch branch;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public WorkingHourCode getCode() {
        return code;
    }

    public void setCode(WorkingHourCode code) {
        this.code = code;
    }

    public Boolean isWork() {
        return isWork;
    }

    public void setWork(Boolean isWork) {
        this.isWork = isWork;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public DateTime getDeleted() {
        return deleted;
    }

    public void setDeleted(DateTime deleted) {
        this.deleted = deleted;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }
}
