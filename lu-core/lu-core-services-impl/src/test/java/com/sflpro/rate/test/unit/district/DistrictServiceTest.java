package com.sflpro.rate.test.unit.district;

import com.sflpro.rate.models.dto.District;
import com.sflpro.rate.repository.district.DistrictRepository;
import com.sflpro.rate.services.district.DistrictService;
import com.sflpro.rate.services.district.impl.DistrictServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/21/14
 * Time: 11:27 AM
 */
@RunWith(MockitoJUnitRunner.class)
public class DistrictServiceTest {

    @Mock
    private DistrictRepository districtRepository;

    @InjectMocks
    private DistrictService districtService = new DistrictServiceImpl();


    @Test
    public void testFindDistrictByDeletedIsNull() throws Exception {

        District d = new District();

        List<District> districtsList = new ArrayList<>();
        districtsList.add(d);

        when(districtRepository.findByDeletedIsNull(new Sort(Sort.Direction.ASC, "name"))).thenReturn(districtsList);

        List<District> result= districtService.findDistrictByDeletedIsNull();

        verify(districtRepository, times(1)).findByDeletedIsNull(new Sort(Sort.Direction.ASC, "name"));

        assertEquals(districtsList.size(), result.size());

    }
}
