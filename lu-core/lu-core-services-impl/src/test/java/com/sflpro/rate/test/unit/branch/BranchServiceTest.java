package com.sflpro.rate.test.unit.branch;

import com.sflpro.rate.models.datatypes.OrganizationStatus;
import com.sflpro.rate.models.dto.*;
import com.sflpro.rate.repository.branch.BranchRepository;
import com.sflpro.rate.services.branch.BranchService;
import com.sflpro.rate.services.branch.impl.BranchServiceImpl;
import com.sflpro.rate.services.exception.BranchAlreadyExistException;
import com.sflpro.rate.services.organization.OrganizationService;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/21/14
 * Time: 10:15 AM
 */
@RunWith(MockitoJUnitRunner.class)
public class BranchServiceTest {

    @Mock
    private BranchRepository branchRepository;

    @Mock
    private OrganizationService organizationService;

    @Mock
    private TypedQuery typedQuery;

    @Mock
    private EntityManager em;


    @InjectMocks
    private BranchService branchService = new BranchServiceImpl();


    @Test
    public void testFindBranchById() throws Exception {
        long id = 1;

        Branch b = new Branch();
        b.setId(id);

        when(branchRepository.findByIdAndDeletedIsNull(id)).thenReturn(b);

        Branch result = branchService.findBranchById(id);

        verify(branchRepository, times(1)).findByIdAndDeletedIsNull(id);

        assertEquals(b.getId(), result.getId());
    }

    @Test
    public void testFindHeadOffice() throws Exception {

        long organizationId = 1;

        Branch b = new Branch();
        b.setId(1);

        when(branchRepository.findByOrganization_IdAndIsHeadAndDeletedIsNull(organizationId, true)).thenReturn(b);

        Branch result = branchService.findHeadOffice(organizationId);

        verify(branchRepository, times(1)).findByOrganization_IdAndIsHeadAndDeletedIsNull(organizationId, true);

        assertEquals(b.getId(), result.getId());

    }

    @Test
    public void testCreateBranch() throws Exception {

        Organization o = new Organization();

        Branch b = new Branch();
        b.setId(1);
        b.setName("name");
        b.setOrganization(o);
        b.setCreated(new DateTime());
        b.setIsHead(false);

        when(branchRepository.findByNameAndOrganizationAndDeletedIsNotNull(b.getName(), b.getOrganization())).thenReturn(null);
        when(branchRepository.save(b)).thenReturn(b);

        Branch result = branchService.createBranch(b);

        verify(branchRepository, times(1)).findByNameAndOrganizationAndDeletedIsNotNull(b.getName(), b.getOrganization());
        verify(branchRepository, times(1)).save(b);

        assertEquals(b.getId(), result.getId());
    }

    @Test(expected = BranchAlreadyExistException.class)
    public void testCreatedBranchAlreadyExist() throws Exception {

        Organization o = new Organization();

        Branch b = new Branch();
        b.setId(1);
        b.setName("name");
        b.setOrganization(o);
        b.setCreated(new DateTime());

        when(branchRepository.findByNameAndOrganizationAndDeletedIsNotNull(b.getName(), b.getOrganization())).thenReturn(b);

        branchService.createBranch(b);

        verify(branchRepository, times(1)).findByNameAndOrganizationAndDeletedIsNotNull(b.getName(), b.getOrganization());

    }

    @Test
    public void testRemoveHeadOfficeIfCreateOtherOne() throws Exception {

        Organization o = new Organization();
        o.setId((long) 1);

        Branch b = new Branch();
        b.setId(1);
        b.setIsHead(true);
        b.setOrganization(o);

        Branch result = new Branch();
        result.setId(1);
        result.setIsHead(false);
        result.setOrganization(o);

        when(branchRepository.findByOrganization_IdAndIsHeadAndDeletedIsNull(o.getId(), true)).thenReturn(b);
        when(em.merge(any(Branch.class))).thenReturn(result);

        branchService.removeHeadOfficeWhenCreateOtherOne(b);

        verify(branchRepository, times(1)).findByOrganization_IdAndIsHeadAndDeletedIsNull(o.getId(), true);
        verify(em, times(1)).merge(any(Branch.class));

    }

    @Test
    public void testCreateOrUpdateBranch_Update() throws Exception {
        Organization o1 = new Organization();
        o1.setId((long) 1);

        Branch updatedBranch = new Branch();
        updatedBranch.setId(1);
        updatedBranch.setIsHead(true);
        updatedBranch.setOrganization(o1);


        Organization o = new Organization();
        o.setId((long) 1);

        Branch b = new Branch();
        b.setId(1);
        b.setName("name");
        b.setLogoUrl("url");
        b.setLogo(new Resource());
        b.setIsHead(true);
        b.setRegion(new Region());
        b.setCity(new City());
        b.setDistrict(new District());
        b.setZipCode("zip");
        b.setPhoneNumber1("phone 1");
        b.setPhoneNumber2("phone 1");
        b.setEmail("email");
        b.setIsFullDay(false);
        b.setStatus(OrganizationStatus.ACTIVE);
        b.setLongitude(new Double("12245"));
        b.setLatitude(new Double("12245"));
        b.setOrganization(o);

        when(branchRepository.findByIdAndDeletedIsNull(b.getId())).thenReturn(updatedBranch);

        when(branchRepository.findByOrganization_IdAndIsHeadAndDeletedIsNull(o.getId(), true)).thenReturn(updatedBranch);
        when(em.merge(updatedBranch)).thenReturn(any(Branch.class));

        when(em.merge(b)).thenReturn(b);

        branchService.createOrUpdateBranch(b);

        verify(branchRepository, times(1)).findByIdAndDeletedIsNull(b.getId());
        verify(branchRepository, times(1)).findByOrganization_IdAndIsHeadAndDeletedIsNull(o.getId(), true);
        verify(em, times(2)).merge(any(Branch.class));

    }

    @Test
    public void testCreateOrUpdateBranch_Create() throws Exception {

        Organization organization = new Organization();
        organization.setId((long) 1);

        Branch branch = new Branch();
        branch.setId(1);
        branch.setName("name");
        branch.setLogoUrl("url");
        branch.setLogo(new Resource());
        branch.setIsHead(true);
        branch.setRegion(new Region());
        branch.setCity(new City());
        branch.setDistrict(new District());
        branch.setZipCode("zip");
        branch.setPhoneNumber1("phone 1");
        branch.setPhoneNumber2("phone 1");
        branch.setEmail("email");
        branch.setIsFullDay(false);
        branch.setStatus(OrganizationStatus.ACTIVE);
        branch.setLongitude(new Double("12245"));
        branch.setLatitude(new Double("12245"));
        branch.setOrganization(organization);


        when(branchRepository.findByIdAndDeletedIsNull(branch.getId())).thenReturn(null);
        when(organizationService.findById(branch.getOrganization().getId())).thenReturn(organization);

        when(branchRepository.findByNameAndOrganizationAndDeletedIsNotNull(branch.getName(), branch.getOrganization())).thenReturn(null);
        when(branchRepository.save(any(Branch.class))).thenReturn(branch);

        when(branchRepository.findByOrganization_IdAndIsHeadAndDeletedIsNull(organization.getId(), true)).thenReturn(null);

        when(em.merge(branch)).thenReturn(branch);

        branchService.createOrUpdateBranch(branch);


        verify(branchRepository, times(1)).findByIdAndDeletedIsNull(branch.getId());
        verify(organizationService, times(1)).findById(branch.getOrganization().getId());

        verify(branchRepository, times(1)).findByNameAndOrganizationAndDeletedIsNotNull(branch.getName(), branch.getOrganization());
        verify(branchRepository, times(1)).save(any(Branch.class));

        verify(branchRepository, times(2)).findByOrganization_IdAndIsHeadAndDeletedIsNull(organization.getId(), true);

        verify(em, times(1)).merge(branch);
    }

    @Test
    public void testGetAllOrganizationBranchesByFilter() throws Exception {

        long organizationId = 1;
        String name = "name";
        String address = "address";
        String region = "region";
        String city = "city";
        String district = "district";
        OrganizationStatus status = OrganizationStatus.ACTIVE;
        long start = 0;
        long count = 3;

        String q =
                "select \n" +
                        "   b \n" +
                        "from \n" +
                        "   Branch b \n" +
                        "   LEFT JOIN b.region r \n" +
                        "   LEFT JOIN b.city c \n" +
                        "   LEFT JOIN b.district d \n" +
                        "where \n" +
                        "   b.organization.id = :organizationId and \n" +
                        "   upper(b.name) like upper(concat( '%', :name, '%')) and \n" +
                        "   ((b.address is null and :address ='') or upper(b.address) like upper(concat( '%', :address, '%'))) and \n" +
                        "   upper(r.name) like upper(concat( '%', :region, '%')) and \n" +
                        "   ((c.name is null and :city = '') or upper(c.name) like upper(concat( '%', :city, '%'))) and \n" +
                        "   ((d.name is null and :district ='') or upper(d.name) like upper(concat( '%', :district, '%'))) and \n" +
                        "   ((:status = '') OR (UPPER(b.status) = UPPER(:status))) \n" +
                        "ORDER BY b.created DESC";


        typedQuery.setParameter("organizationId", organizationId);
        typedQuery.setParameter("name", name);
        typedQuery.setParameter("address", address);
        typedQuery.setParameter("region", region);
        typedQuery.setParameter("city", city);
        typedQuery.setParameter("district", district);
        typedQuery.setParameter("status", status.name());

        typedQuery.setFirstResult((int) start);
        typedQuery.setMaxResults((int) count);


        when(em.createQuery(q, Branch.class)).thenReturn(typedQuery);

        branchService.getAllOrganizationBranchesByFilter(organizationId, start, count, name, region, city, district, address, status);

        verify(em, times(1)).createQuery(q, Branch.class);
    }

    @Test
    public void testCount() throws Exception {
        long organizationId = 1;
        String name = "name";
        String address = "address";
        String region = "region";
        String city = "city";
        String district = "district";
        OrganizationStatus status = OrganizationStatus.ACTIVE;

        Long count = (long) 10;

        when(branchRepository.findCount(organizationId, name, region, city, district, address, status.name())).thenReturn(count);

        branchService.count(organizationId, name, region, city, district, address, status);

        verify(branchRepository, times(1)).findCount(organizationId, name, region, city, district, address, status.name());

    }
}
