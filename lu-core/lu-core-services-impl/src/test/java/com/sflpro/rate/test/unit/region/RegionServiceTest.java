package com.sflpro.rate.test.unit.region;

import com.sflpro.rate.models.dto.Region;
import com.sflpro.rate.repository.region.RegionRepository;
import com.sflpro.rate.services.region.RegionService;
import com.sflpro.rate.services.region.impl.RegionServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/20/14
 * Time: 2:34 AM
 */
@RunWith(MockitoJUnitRunner.class)
public class RegionServiceTest {

    @Mock
    private RegionRepository regionRepository;


    @InjectMocks
    private RegionService regionService = new RegionServiceImpl();

    @Test
    public void testGetAllRegions() throws Exception {

        List<Region> regionList = new ArrayList<>();
        regionList.add(new Region());

        when(regionRepository.findByDeletedIsNull()).thenReturn(regionList);

        List<Region> result = regionService.findByDeletedIsNull();

        verify(regionRepository, times(1)).findByDeletedIsNull();


        assertEquals(regionList.size(), result.size());

    }
}
