package com.sflpro.rate.test.unit.working_hour;

import com.sflpro.rate.admin.ui.dto.WorkingHourCode;
import com.sflpro.rate.models.dto.Branch;
import com.sflpro.rate.models.dto.WorkingHour;
import com.sflpro.rate.repository.working_hour.WorkingHourRepository;
import com.sflpro.rate.services.branch.BranchService;
import com.sflpro.rate.services.exception.WorkingHourExistException;
import com.sflpro.rate.services.working_hour.WorkingHourService;
import com.sflpro.rate.services.working_hour.impl.WorkingHourServiceImpl;
import org.joda.time.LocalTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/21/14
 * Time: 11:36 AM
 */
@RunWith(MockitoJUnitRunner.class)
public class WorkingHourServiceTest {

    @Mock
    private WorkingHourRepository workingHourRepository;

    @Mock
    private BranchService branchService;

    @Mock
    private EntityManager em;

    @InjectMocks
    private WorkingHourService workingHourService = new WorkingHourServiceImpl();


    @Test
    public void testCreateWorkingHour() throws Exception {

        long branchId = 1;

        Branch b = new Branch();
        b.setId(branchId);

        WorkingHour workingHour = new WorkingHour();
        workingHour.setBranch(b);
        workingHour.setCode(WorkingHourCode.MONDAY);

        when(branchService.findBranchById(b.getId())).thenReturn(b);
        when(workingHourRepository.findByCodeAndBranchAndDeletedIsNull(workingHour.getCode(), b)).thenReturn(null);
        when(workingHourRepository.save(workingHour)).thenReturn(workingHour);

        workingHourService.createWorkingHour(workingHour, branchId);

        verify(branchService, times(1)).findBranchById(b.getId());
        verify(workingHourRepository, times(1)).findByCodeAndBranchAndDeletedIsNull(workingHour.getCode(), b);
        verify(workingHourRepository, times(1)).save(workingHour);

    }

    @Test(expected = WorkingHourExistException.class)
    public void testCreateWorkingHourAlreadyExist() throws Exception {

        long branchId = 1;

        Branch b = new Branch();
        b.setId(branchId);

        WorkingHour workingHour = new WorkingHour();
        workingHour.setBranch(b);
        workingHour.setCode(WorkingHourCode.MONDAY);

        when(branchService.findBranchById(b.getId())).thenReturn(b);
        when(workingHourRepository.findByCodeAndBranchAndDeletedIsNull(workingHour.getCode(), b)).thenReturn(workingHour);

        workingHourService.createWorkingHour(workingHour, branchId);

        verify(branchService, times(1)).findBranchById(b.getId());
        verify(workingHourRepository, times(1)).findByCodeAndBranchAndDeletedIsNull(workingHour.getCode(), b);
    }

    @Test
    public void testCreateOrUpdateWorkingHour_Create() throws Exception {

        long branchId = 1;

        Branch b = new Branch();
        b.setId(branchId);

        WorkingHour workingHour = new WorkingHour();
        workingHour.setCode(WorkingHourCode.MONDAY);
        workingHour.setStartTime(new LocalTime());
        workingHour.setEndTime(new LocalTime());
        workingHour.setWork(true);

        when(workingHourRepository.findByCodeAndBranch_IdAndDeletedIsNull(workingHour.getCode(), branchId)).thenReturn(null);

        when(branchService.findBranchById(b.getId())).thenReturn(b);
        when(workingHourRepository.findByCodeAndBranchAndDeletedIsNull(workingHour.getCode(), b)).thenReturn(null);
        when(workingHourRepository.save(workingHour)).thenReturn(workingHour);

        when(em.merge(any(WorkingHour.class))).thenReturn(workingHour);


        workingHourService.createOrUpdateWorkingHour(workingHour, branchId);


        verify(workingHourRepository, times(1)).findByCodeAndBranch_IdAndDeletedIsNull(workingHour.getCode(), branchId);

        verify(branchService, times(1)).findBranchById(b.getId());
        verify(workingHourRepository, times(1)).findByCodeAndBranchAndDeletedIsNull(workingHour.getCode(), b);
        verify(workingHourRepository, times(1)).save(workingHour);

        verify(em, times(1)).merge(any(WorkingHour.class));
    }

    @Test
    public void testCreateOrUpdateWorkingHour_Update() throws Exception {

        long branchId = 1;

        WorkingHour workingHour = new WorkingHour();
        workingHour.setCode(WorkingHourCode.MONDAY);
        workingHour.setStartTime(new LocalTime());
        workingHour.setEndTime(new LocalTime());
        workingHour.setWork(true);

        when(workingHourRepository.findByCodeAndBranch_IdAndDeletedIsNull(workingHour.getCode(), branchId)).thenReturn(new WorkingHour());
        when(em.merge(any(WorkingHour.class))).thenReturn(workingHour);

        workingHourService.createOrUpdateWorkingHour(workingHour, branchId);

        verify(workingHourRepository, times(1)).findByCodeAndBranch_IdAndDeletedIsNull(workingHour.getCode(), branchId);
        verify(em, times(1)).merge(any(WorkingHour.class));
    }

    @Test
    public void testFindWorkingHoursByBranch() throws Exception {

        long branchId = 1;

        WorkingHour workingHour = new WorkingHour();

        List<WorkingHour> workingHourList = new ArrayList<>();
        workingHourList.add(workingHour);

        when(workingHourRepository.findByBranch_IdAndDeletedIsNull(branchId)).thenReturn(workingHourList);

        List<WorkingHour> result = workingHourService.findWorkingHoursByBranch(branchId);

        verify(workingHourRepository, times(1)).findByBranch_IdAndDeletedIsNull(branchId);

        assertEquals(workingHourList.size(), result.size());
    }

    @Test
    public void testDeleteWorkingHoursByBranch() throws Exception {

        long branchId = 1;

        WorkingHour workingHour = new WorkingHour();

        List<WorkingHour> workingHourList = new ArrayList<>();
        workingHourList.add(workingHour);

        when(workingHourRepository.findByBranch_IdAndDeletedIsNull(branchId)).thenReturn(workingHourList);
        when(em.merge(any(WorkingHour.class))).thenReturn(any(WorkingHour.class));

        workingHourService.deleteWorkingHoursByBranch(branchId);

        verify(workingHourRepository, times(1)).findByBranch_IdAndDeletedIsNull(branchId);
        verify(em, times(workingHourList.size())).merge(any(WorkingHour.class));
    }
}
