package com.sflpro.rate.test.unit.resource;

import com.sflpro.rate.models.datatypes.ResourceType;
import com.sflpro.rate.models.dto.Resource;
import com.sflpro.rate.repository.resource.ResourceManagerRepository;
import com.sflpro.rate.services.resource.impl.ResourceManagerImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/3/14
 * Time: 3:24 PM
 */
@RunWith(MockitoJUnitRunner.class)
public class ResourceManagerTest {

    @Mock
    private ResourceManagerRepository resourceManagerRepository;

    @InjectMocks
    private ResourceManagerImpl resourceManager = new ResourceManagerImpl();


    @Test
    public void testSaveDraftResource() throws Exception {

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("resource/acba.png");
        ResourceType resourceType = ResourceType.LOGO;

        Assert.assertNotNull("Resource file not found.", inputStream);

        Resource resource = resourceManager.saveDraftResource(inputStream, resourceType);

        Assert.assertNotNull("Resource not created.", resource);

    }

    @Test
    public void testCreateTemporaryFile() throws IOException {

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("resource/acba.png");

        Assert.assertNotNull(inputStream);

        File file = resourceManager.createTemporaryFile(inputStream);

        Assert.assertNotNull(file);

    }

    @Test
    public void testFinalizeWorkingWithResource() throws IOException {

        String fileName = "resource_file.xls";

        URL url = Thread.currentThread().getContextClassLoader().getResource("resource/acba.png");

        assertNotNull("Resource url not found", url);

        File file = new File(url.getPath());
        ResourceType resourceType = ResourceType.LOGO;

        resourceManager.setUploadDir(System.getProperty("java.io.tmpdir") + File.separator + "uploads" + File.separator);

        Resource r = new Resource();

        when(resourceManagerRepository.save(any(Resource.class))).thenReturn(r);

        Resource result = resourceManager.finalizeWorkingWithResource(file, resourceType, fileName);

        verify(resourceManagerRepository, times(1)).save(any(Resource.class));
        assertEquals(r, result);

    }

    @Test
    public void testFinalizeWorkingWithResourceWhenResourceTypeIsLogo() throws IOException {

        String fileName = "location.png";

        URL url = Thread.currentThread().getContextClassLoader().getResource("resource/acba.png");

        assertNotNull("Resource url not found", url);

        File file = new File(url.getPath());
        ResourceType resourceType = ResourceType.LOGO;

        resourceManager.setUploadDir(System.getProperty("java.io.tmpdir") + File.separator + "uploads" + File.separator);

        Resource r = new Resource();

        when(resourceManagerRepository.save(any(Resource.class))).thenReturn(r);

        Resource result = resourceManager.finalizeWorkingWithResource(file, resourceType, fileName);

        verify(resourceManagerRepository, times(1)).save(any(Resource.class));
        assertEquals(r, result);

    }

    @Test
    public void testToAbsolutePathWhenPassedResource() throws Exception {

        String localPath = "local/path";
        String uploadDir = System.getProperty("java.io.tmpdir") + File.separator + "uploads" + File.separator;

        Resource resource = new Resource();
        resource.setPath(localPath);

        resourceManager.setUploadDir(uploadDir);

        String absolutePath = resourceManager.toAbsolutePath(resource);

        assertEquals(uploadDir+"/"+localPath, absolutePath);
    }

    @Test
    public void testToAbsolutePathWhenPassedStringResourcePath() throws Exception {

        String localPath = "local/path";
        String uploadDir = System.getProperty("java.io.tmpdir") + File.separator + "uploads" + File.separator;

        resourceManager.setUploadDir(uploadDir);

        String result = resourceManager.toAbsolutePath(localPath);

        assertEquals(uploadDir+"/"+localPath, result);

    }

}
