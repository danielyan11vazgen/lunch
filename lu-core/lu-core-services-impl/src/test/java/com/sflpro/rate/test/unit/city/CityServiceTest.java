package com.sflpro.rate.test.unit.city;

import com.sflpro.rate.models.dto.City;
import com.sflpro.rate.models.dto.Region;
import com.sflpro.rate.repository.city.CityRepository;
import com.sflpro.rate.services.city.CityService;
import com.sflpro.rate.services.city.impl.CityServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/21/14
 * Time: 11:15 AM
 */
@RunWith(MockitoJUnitRunner.class)
public class CityServiceTest {

    @Mock
    private CityRepository cityRepository;

    @InjectMocks
    private CityService cityService = new CityServiceImpl();

    @Test
    public void testFindCitiesByDeletedIsNull() throws Exception {
        City c = new City();

        List<City> cityList = new ArrayList<>();
        cityList.add(c);


        when(cityRepository.findByDeletedIsNull(new Sort(Sort.Direction.ASC, "name"))).thenReturn(cityList);

        List<City> result = cityService.findCitiesByDeletedIsNull();

        verify(cityRepository, times(1)).findByDeletedIsNull(new Sort(Sort.Direction.ASC, "name"));

        assertEquals(cityList.size(), result.size());
    }

    @Test
    public void testFindByRegionAndDeletedIsNull() throws Exception {

        Region r = new Region();

        City c = new City();

        List<City> cityList = new ArrayList<>();
        cityList.add(c);


        when(cityRepository.findByRegionAndDeletedIsNull(r)).thenReturn(cityList);

        List<City> result = cityService.findCitiesByRegionAndDeletedIsNull(r);

        verify(cityRepository, times(1)).findByRegionAndDeletedIsNull(r);

        assertEquals(cityList.size(), result.size());
    }
}
