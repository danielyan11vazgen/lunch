package com.sflpro.rate.test.unit.organization;

import com.sflpro.rate.models.datatypes.OrganizationStatus;
import com.sflpro.rate.models.datatypes.OrganizationType;
import com.sflpro.rate.models.dto.Organization;
import com.sflpro.rate.models.dto.Resource;
import com.sflpro.rate.repository.organization.OrganizationRepository;
import com.sflpro.rate.services.exception.OrganizationExistException;
import com.sflpro.rate.services.organization.OrganizationService;
import com.sflpro.rate.services.organization.impl.OrganizationServiceImpl;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/4/14
 * Time: 3:28 PM
 */
@RunWith(MockitoJUnitRunner.class)
public class OrganizationServiceTest {

    @Mock
    private OrganizationRepository organizationRepository;

    @Mock
    private EntityManager em;

    @Mock
    private TypedQuery typedQuery;

    @InjectMocks
    private OrganizationService organizationService = new OrganizationServiceImpl();


    @Test
    public void testFindById() throws Exception {
        long id = 1;

        Organization o = new Organization();
        o.setId(id);

        when(organizationRepository.findById(id)).thenReturn(o);

        Organization result = organizationService.findById(id);

        verify(organizationRepository, times(1)).findById(id);

        assertEquals(o.getId(), result.getId());
    }

    @Test
    public void testCreateOrganization() throws Exception {

        Organization o = new Organization();
        o.setName("name");

        when(organizationRepository.findByNameAndDeletedIsNull(o.getName())).thenReturn(null);
        when(organizationRepository.save(o)).thenReturn(o);

        Organization result = organizationService.create(o);

        verify(organizationRepository, times(1)).findByNameAndDeletedIsNull(o.getName());
        verify(organizationRepository, times(1)).save(o);

        assertEquals(result, o);
    }

    @Test(expected = OrganizationExistException.class)
    public void testCreateOrganizationWhenAlreadyExist() throws Exception {

        Organization o = new Organization();
        o.setName("name");

        when(organizationRepository.findByNameAndDeletedIsNull(o.getName())).thenReturn(o);

        organizationService.create(o);
    }

    @Test
    public void testCreateOrUpdateOrganization_Create() throws Exception {

        String name = "name";

        Organization o = new Organization();
        o.setName(name);
        o.setType(OrganizationType.BANK);
        o.setLicense("license");
        o.setUrl("url");
        o.setLogoUrl("logo url");
        o.setLogo(new Resource());
        o.setStatus(OrganizationStatus.ACTIVE);
        o.setIssuedOn(new DateTime());
        o.setCreated(new DateTime());
        o.setDeleted(null);

        when(organizationRepository.findByNameAndDeletedIsNotNull(name)).thenReturn(null);
        when(organizationRepository.save(any(Organization.class))).thenReturn(o);
        when(em.merge(any(Organization.class))).thenReturn(o);

        Organization result = organizationService.createOrUpdate(o);

        verify(organizationRepository, times(1)).findByNameAndDeletedIsNotNull(name);
        verify(organizationRepository, times(1)).save(any(Organization.class));
        verify(em, times(1)).merge(any(Organization.class));

        assertEquals(result.getName(), o.getName());
        assertEquals(result.getType(), o.getType());
        assertEquals(result.getLicense(), o.getLicense());
        assertEquals(result.getUrl(), o.getUrl());
        assertEquals(result.getLogoUrl(), o.getLogoUrl());
        assertEquals(result.getLogo(), o.getLogo());
        assertEquals(result.getStatus(), o.getStatus());
        assertEquals(result.getIssuedOn(), o.getIssuedOn());
        assertEquals(result.getCreated(), o.getCreated());
        assertEquals(result.getDeleted(), o.getDeleted());
    }

    @Test
    public void testCreateOrUpdateOrganization_Update() throws Exception {

        String name = "name";

        Organization o = new Organization();
        o.setId((long) 1);
        o.setName(name);
        o.setType(OrganizationType.BANK);
        o.setLicense("license");
        o.setUrl("url");
        o.setLogoUrl("logo url");
        o.setLogo(new Resource());
        o.setStatus(OrganizationStatus.ACTIVE);
        o.setIssuedOn(new DateTime());
        o.setCreated(new DateTime());


        when(organizationRepository.findById(o.getId())).thenReturn(o);
        when(em.merge(any(Organization.class))).thenReturn(o);

        organizationService.createOrUpdate(o);

        verify(organizationRepository, times(1)).findById(o.getId());
        verify(em, times(1)).merge(any(Organization.class));
    }

    @Test
    public void testGetAllOrganizationsByFilter() throws Exception {

        long start = 1;
        long count = 1;
        String name = "name";
        Integer branches = 2;
        OrganizationStatus status = OrganizationStatus.ACTIVE;
        OrganizationType organizationType = OrganizationType.BANK;

        String query =
                "SELECT \n" +
                        "   o \n" +
                        "FROM \n" +
                        "   Organization o \n" +
                        "WHERE \n" +
                        "   o.type = :organizationType and \n" +
                        "   (UPPER(o.name) LIKE UPPER(CONCAT('%', :name, '%'))) AND \n" +
                        "   ((:branches <> -1 AND o.branches.size = :branches) OR (:branches = -1 )) AND \n" +
                        "   ((:status = '') OR (UPPER(o.status) = UPPER(:status))) ORDER BY o.created DESC";

        typedQuery.setParameter("name", name);
        typedQuery.setParameter("branches", branches);
        typedQuery.setParameter("status", status.name());
        typedQuery.setParameter("organizationType", organizationType);

        typedQuery.setFirstResult((int) start);
        typedQuery.setMaxResults((int) count);

        when(em.createQuery(query, Organization.class)).thenReturn(typedQuery);

        organizationService.getAllOrganizationsByFilter(start, count, name, branches, status, organizationType);

        verify(em, times(1)).createQuery(query, Organization.class);

    }

    @Test
    public void testCount() throws Exception {

        String name = "name";
        Integer branches = 0;
        OrganizationStatus status = OrganizationStatus.INACTIVE;
        OrganizationType organizationType = OrganizationType.BANK;

        when(organizationRepository.findCount(name, branches, status.name(), organizationType)).thenReturn((long) 2);

        organizationService.count(name, branches, status, organizationType);

        verify(organizationRepository, times(1)).findCount(name, branches, status.name(), organizationType);

    }

}
