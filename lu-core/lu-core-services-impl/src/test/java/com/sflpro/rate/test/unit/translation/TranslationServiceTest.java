package com.sflpro.rate.test.unit.translation;

import com.sflpro.rate.admin.ui.dto.SystemTranslationDTO;
import com.sflpro.rate.admin.ui.dto.TranslationDTO;
import com.sflpro.rate.models.datatypes.LanguageKeyType;
import com.sflpro.rate.models.dto.*;
import com.sflpro.rate.repository.language.LanguageRepository;
import com.sflpro.rate.repository.language_key.LanguageKeyRepository;
import com.sflpro.rate.repository.language_value.LanguageValueRepository;
import com.sflpro.rate.repository.system_translation.SystemTranslationRepository;
import com.sflpro.rate.services.translation.TranslationService;
import com.sflpro.rate.services.translation.impl.TranslationServiceImpl;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Sort;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/10/14
 * Time: 4:36 PM
 */
@RunWith(MockitoJUnitRunner.class)
public class TranslationServiceTest {

    @Mock
    private LanguageRepository languageRepository;

    @Mock
    private LanguageValueRepository languageValueRepository;

    @Mock
    private LanguageKeyRepository languageKeyRepository;

    @Mock
    private SystemTranslationRepository systemTranslationRepository;

    @Mock
    private EntityManager em;


    @InjectMocks
    private TranslationService translationService = new TranslationServiceImpl();


    @Test
    public void testGetAllLanguages() throws Exception {

        List<Language> languageList = new ArrayList<>();
        languageList.add(new Language());


        when(languageRepository.findByDeletedIsNull(new Sort(Sort.Direction.DESC, "isDefault"))).thenReturn(languageList);

        List<Language> resultList = translationService.getAllLanguages();

        verify(languageRepository, times(1)).findByDeletedIsNull(new Sort(Sort.Direction.DESC, "isDefault"));

        assertEquals(languageList.size(), resultList.size());

    }

    @Test
    public void testFindLanguageById() throws Exception {

        long id = 1;

        Language l = new Language();
        l.setId(id);
        l.setCreated(new DateTime());

        when(languageRepository.findByIdAndDeletedIsNull(id)).thenReturn(l);

        Language result = translationService.findLanguageById(id);

        verify(languageRepository, times(1)).findByIdAndDeletedIsNull(id);

        assertEquals(l.getId(), result.getId());
        assertNull(result.getDeleted());

    }

    @Test
    public void testCreateLanguageKey() throws Exception {

        LanguageKey languageKey = new LanguageKey();
        languageKey.setEntityId((long) 1);
        languageKey.setEntityName("entity");
        languageKey.setEntityField("field");
        languageKey.setCreated(new DateTime());
        languageKey.setLanguageKeyType(LanguageKeyType.USER_GENERATED);

        when(languageKeyRepository.save(languageKey)).thenReturn(languageKey);

        LanguageKey result = translationService.createLanguageKey(languageKey);

        verify(languageKeyRepository, times(1)).save(languageKey);

        assertEquals(languageKey.getId(), result.getId());
        assertEquals(languageKey.getEntityName(), result.getEntityName());
        assertEquals(languageKey.getEntityField(), result.getEntityField());
        assertEquals(languageKey.getCreated(), result.getCreated());
        assertEquals(languageKey.getLanguageKeyType(), result.getLanguageKeyType());

    }

    @Test
    public void testCreateOrUpdateLanguageKey_Create() throws Exception {

        String entityName = "entity";
        Long entityId = (long) 1;
        String entityField = "field";
        LanguageKeyType languageKeyType = LanguageKeyType.USER_GENERATED;

        LanguageKey key = new LanguageKey();
        key.setEntityName(entityName);
        key.setEntityField(entityField);
        key.setEntityId(entityId);
        key.setLanguageKeyType(languageKeyType);

        when(languageKeyRepository.findByEntityNameAndEntityIdAndEntityFieldAndLanguageKeyTypeAndDeletedIsNull(entityName, entityId, entityField, languageKeyType)).thenReturn(null);
        when(languageKeyRepository.save(any(LanguageKey.class))).thenReturn(key);
        when(em.merge(key)).thenReturn(key);

        translationService.createOrUpdateLanguageKey(entityName, entityId, entityField, languageKeyType);

        verify(languageKeyRepository, times(1)).findByEntityNameAndEntityIdAndEntityFieldAndLanguageKeyTypeAndDeletedIsNull(entityName, entityId, entityField, languageKeyType);
        verify(languageKeyRepository, times(1)).save(any(LanguageKey.class));
        verify(em, times(1)).merge(key);

    }

    @Test
    public void testCreateOrUpdateLanguageKey_Update() throws Exception {

        String entityName = "entity";
        Long entityId = (long) 1;
        String entityField = "field";
        LanguageKeyType languageKeyType = LanguageKeyType.USER_GENERATED;

        LanguageKey key = new LanguageKey();
        key.setEntityName("entity_");
        key.setEntityField("field_");
        key.setEntityId((long) 2);
        key.setLanguageKeyType(LanguageKeyType.SYSTEM);

        when(languageKeyRepository.findByEntityNameAndEntityIdAndEntityFieldAndLanguageKeyTypeAndDeletedIsNull(entityName, entityId, entityField, languageKeyType)).thenReturn(key);
        when(em.merge(key)).thenReturn(key);

        translationService.createOrUpdateLanguageKey(entityName, entityId, entityField, languageKeyType);

        verify(languageKeyRepository, times(1)).findByEntityNameAndEntityIdAndEntityFieldAndLanguageKeyTypeAndDeletedIsNull(entityName, entityId, entityField, languageKeyType);
        verify(em, times(1)).merge(key);

    }

    @Test
    public void testCreateTranslation() throws Exception {

        long organizationId = 1;
        long languageId = 1;

        TranslationDTO translationDTO = new TranslationDTO();
        translationDTO.setLanguageId(1);
        translationDTO.setLanguageCode("code");
        translationDTO.setFieldValue("value");

        List<TranslationDTO> translationDTOList = new ArrayList<>();
        translationDTOList.add(translationDTO);

        LanguageKey languageKey = new LanguageKey();
        languageKey.setId((long) 1);
        languageKey.setCreated(new DateTime());
        languageKey.setEntityName(Organization.class.getName());
        languageKey.setEntityId(organizationId);
        languageKey.setEntityField("name");
        languageKey.setLanguageKeyType(LanguageKeyType.USER_GENERATED);

        Language language = new Language();
        language.setId(languageId);
        language.setName("language name");
        language.setDefault(false);
        language.setSystemDefault(true);
        language.setIconUrl("url");
        language.setCreated(new DateTime());
        language.setUpdated(new DateTime());


        LanguageValue languageValue = new LanguageValue();
        languageValue.setLanguage(language);
        languageValue.setLanguageKey(languageKey);
        languageValue.setValue(translationDTO.getFieldValue());


        when(languageKeyRepository.findByEntityNameAndEntityIdAndEntityFieldAndLanguageKeyTypeAndDeletedIsNull(Organization.class.getSimpleName().toLowerCase(), (long) 1, "name", LanguageKeyType.USER_GENERATED)).thenReturn(languageKey);
        when(em.merge(languageKey)).thenReturn(languageKey);

        when(languageRepository.findByIdAndDeletedIsNull(languageId)).thenReturn(language);

        when(languageValueRepository.findByLanguageKey_IdAndLanguage_Id(languageKey.getId(), languageId)).thenReturn(languageValue);
        when(em.merge(languageValue)).thenReturn(languageValue);


        translationService.createOrUpdateTranslation(translationDTOList, organizationId, Organization.class.getSimpleName().toLowerCase(), "name");

        verify(languageKeyRepository, times(1)).findByEntityNameAndEntityIdAndEntityFieldAndLanguageKeyTypeAndDeletedIsNull(Organization.class.getSimpleName().toLowerCase(), (long) 1, "name", LanguageKeyType.USER_GENERATED);
        verify(em, times(1)).merge(languageKey);

        verify(languageRepository, times(translationDTOList.size())).findByIdAndDeletedIsNull(languageId);

        verify(languageValueRepository, times(translationDTOList.size())).findByLanguageKey_IdAndLanguage_Id(languageKey.getId(), languageId);
        verify(em, times(translationDTOList.size())).merge(languageValue);

    }

    @Test
    public void testCreateSystemTranslation() throws Exception {

        long organizationId = 1;
        long languageId = 1;

        TranslationDTO translationDTO = new TranslationDTO();
        translationDTO.setLanguageId(1);
        translationDTO.setLanguageCode("code");
        translationDTO.setFieldValue("value");

        List<TranslationDTO> translationDTOList = new ArrayList<>();
        translationDTOList.add(translationDTO);

        LanguageKey languageKey = new LanguageKey();
        languageKey.setId((long) 1);
        languageKey.setCreated(new DateTime());
        languageKey.setEntityName(Organization.class.getName());
        languageKey.setEntityId(organizationId);
        languageKey.setEntityField("name");
        languageKey.setLanguageKeyType(LanguageKeyType.USER_GENERATED);

        Language language = new Language();
        language.setId(languageId);
        language.setName("language name");
        language.setDefault(false);
        language.setSystemDefault(true);
        language.setIconUrl("url");
        language.setCreated(new DateTime());
        language.setUpdated(new DateTime());


        LanguageValue languageValue = new LanguageValue();
        languageValue.setLanguage(language);
        languageValue.setLanguageKey(languageKey);
        languageValue.setValue(translationDTO.getFieldValue());


        when(languageKeyRepository.findByEntityNameAndEntityIdAndEntityFieldAndLanguageKeyTypeAndDeletedIsNull(SystemTranslation.class.getSimpleName().toLowerCase(), (long) 1, "name", LanguageKeyType.SYSTEM)).thenReturn(languageKey);
        when(em.merge(languageKey)).thenReturn(languageKey);

        when(languageRepository.findByIdAndDeletedIsNull(languageId)).thenReturn(language);

        when(languageValueRepository.findByLanguageKey_IdAndLanguage_Id(languageKey.getId(), languageId)).thenReturn(languageValue);
        when(em.merge(languageValue)).thenReturn(languageValue);


        translationService.createOrUpdateSystemTranslation(translationDTOList, organizationId, SystemTranslation.class.getSimpleName().toLowerCase(), "name");

        verify(languageKeyRepository, times(1)).findByEntityNameAndEntityIdAndEntityFieldAndLanguageKeyTypeAndDeletedIsNull(SystemTranslation.class.getSimpleName().toLowerCase(), (long) 1, "name", LanguageKeyType.SYSTEM);
        verify(em, times(1)).merge(languageKey);

        verify(languageRepository, times(translationDTOList.size())).findByIdAndDeletedIsNull(languageId);

        verify(languageValueRepository, times(translationDTOList.size())).findByLanguageKey_IdAndLanguage_Id(languageKey.getId(), languageId);
        verify(em, times(translationDTOList.size())).merge(languageValue);

    }

    @Test
    public void testGetEntityTranslatableValuesByField() throws Exception {

        String entityName = "entity";
        Long entityId = (long) 1;
        String entityField = "field";
        LanguageKeyType languageKeyType = LanguageKeyType.USER_GENERATED;

        LanguageKey key = new LanguageKey();
        key.setId((long) 1);
        key.setEntityName(entityName);
        key.setEntityField(entityField);
        key.setEntityId(entityId);
        key.setLanguageKeyType(languageKeyType);

        when(languageKeyRepository.findByEntityNameAndEntityIdAndEntityFieldAndLanguageKeyTypeAndDeletedIsNull(entityName, entityId, entityField, languageKeyType)).thenReturn(key);

        translationService.getEntityTranslatableValuesByField(entityName, entityId, entityField, languageKeyType);

        verify(languageKeyRepository, times(1)).findByEntityNameAndEntityIdAndEntityFieldAndLanguageKeyTypeAndDeletedIsNull(entityName, entityId, entityField, languageKeyType);

    }

    @Test
    public void testCreateLanguageValue() throws Exception {

        LanguageValue languageValue = new LanguageValue();

        when(languageValueRepository.save(languageValue)).thenReturn(languageValue);

        translationService.createLanguageValue(languageValue);

        verify(languageValueRepository, times(1)).save(languageValue);
    }

    @Test
    public void testCreateOrUpdateLanguageValue_Create() throws Exception {
        LanguageKey languageKey = new LanguageKey();
        languageKey.setId((long) 1);

        Language language = new Language();
        language.setId(1);

        String value = "value";

        LanguageValue languageValue = new LanguageValue();
        languageValue.setLanguage(language);
        languageValue.setLanguageKey(languageKey);
        languageValue.setValue(value);

        when(languageValueRepository.findByLanguageKey_IdAndLanguage_Id(languageKey.getId(), language.getId())).thenReturn(null);
        when(languageValueRepository.save(languageValue)).thenReturn(languageValue);
        when(em.merge(languageValue)).thenReturn(languageValue);

        translationService.createOrUpdateLanguageValue(languageKey, language, value);

        verify(languageValueRepository, times(1)).findByLanguageKey_IdAndLanguage_Id(languageKey.getId(), language.getId());
        verify(languageValueRepository, times(1)).save(any(LanguageValue.class));

        verify(em, times(1)).merge(any(LanguageValue.class));
    }

    @Test
    public void testCreateOrUpdateLanguageValue_Update() throws Exception {
        LanguageKey languageKey = new LanguageKey();
        languageKey.setId((long) 1);

        Language language = new Language();
        language.setId(1);

        String value = "value";

        LanguageValue languageValue = new LanguageValue();
        languageValue.setLanguage(language);
        languageValue.setLanguageKey(languageKey);
        languageValue.setValue("value_");

        when(languageValueRepository.findByLanguageKey_IdAndLanguage_Id(languageKey.getId(), language.getId())).thenReturn(languageValue);
        when(em.merge(languageValue)).thenReturn(languageValue);

        translationService.createOrUpdateLanguageValue(languageKey, language, value);

        verify(languageValueRepository, times(1)).findByLanguageKey_IdAndLanguage_Id(languageKey.getId(), language.getId());
        verify(em, times(1)).merge(languageValue);
    }

    @Test
    public void testGetAllSystemTranslationsWithFilteredResults() {

        long entityId = 1L;
        final List<SystemTranslation> systemTranslationList = new ArrayList<>();

        final SystemTranslation systemTranslation = new SystemTranslation();
        systemTranslation.setId(entityId);
        systemTranslation.setCreated(new DateTime());

        systemTranslationList.add(systemTranslation);

        final LanguageKey languageKey = new LanguageKey();

        languageKey.setId(1L);
        languageKey.setCreated(new DateTime());
        languageKey.setDeleted(null);
        languageKey.setEntityId(56L);
        languageKey.setLanguageKeyType(LanguageKeyType.SYSTEM);

        Language language1 = new Language();
        language1.setId(1L);
        language1.setName("language name1");
        language1.setDefault(false);
        language1.setSystemDefault(true);
        language1.setIconUrl("url");
        language1.setCreated(new DateTime());
        language1.setUpdated(new DateTime());
        language1.setCode("code1");

        LanguageValue languageValue1 = new LanguageValue();
        languageValue1.setLanguage(language1);
        languageValue1.setLanguageKey(languageKey);
        languageValue1.setValue("value1");

        Language language2 = new Language();
        language2.setId(2L);
        language2.setName("language name2");
        language2.setDefault(false);
        language2.setSystemDefault(true);
        language2.setIconUrl("url");
        language2.setCreated(new DateTime());
        language2.setUpdated(new DateTime());
        language2.setCode("code2");

        LanguageValue languageValue2 = new LanguageValue();
        languageValue2.setLanguage(language2);
        languageValue2.setLanguageKey(languageKey);
        languageValue2.setValue("value1");

        Language language3 = new Language();
        language3.setId(3L);
        language3.setName("language name3");
        language3.setDefault(false);
        language3.setSystemDefault(true);
        language3.setIconUrl("url");
        language3.setCreated(new DateTime());
        language3.setUpdated(new DateTime());
        language3.setCode("code3");

        LanguageValue languageValue3 = new LanguageValue();
        languageValue3.setLanguage(language3);
        languageValue3.setLanguageKey(languageKey);
        languageValue3.setValue("value3");

        final Set<LanguageValue> languageValueSet = new HashSet<>();

        languageValueSet.add(languageValue1);
        languageValueSet.add(languageValue2);
        languageValueSet.add(languageValue3);

        languageKey.setLanguageValues(languageValueSet);

        final Long translationNumber = 1L;
        final String translationNumberString = "1";
        final Map<String, String> translatableFilterValues = new HashMap<>();
        translatableFilterValues.put("code1", "value1");
        translatableFilterValues.put("code2", "XXX");

        when(systemTranslationRepository.findByDeletedIsNullAndContentId(translationNumber)).thenReturn(systemTranslationList);
        when(languageKeyRepository.findByEntityNameAndEntityIdAndEntityFieldAndLanguageKeyTypeAndDeletedIsNull(systemTranslation.getClass().getSimpleName().toLowerCase(), entityId, "name", LanguageKeyType.SYSTEM)).thenReturn(languageKey);

        List<SystemTranslationDTO> resultList = translationService.getAllSystemTranslations(translationNumberString, translatableFilterValues);

        verify(systemTranslationRepository, times(1)).findByDeletedIsNullAndContentId(translationNumber);

        assertEquals(systemTranslationList.size() - 1, resultList.size());
    }

    @Test
    public void testGetAllSystemTranslationsWithFilters() {

        long entityId = 1L;
        final List<SystemTranslation> systemTranslationList = new ArrayList<>();

        final SystemTranslation systemTranslation = new SystemTranslation();
        systemTranslation.setId(entityId);
        systemTranslation.setCreated(new DateTime());

        systemTranslationList.add(systemTranslation);

        final LanguageKey languageKey = new LanguageKey();

        languageKey.setId(1L);
        languageKey.setCreated(new DateTime());
        languageKey.setDeleted(null);
        languageKey.setEntityId(entityId);
        languageKey.setLanguageKeyType(LanguageKeyType.SYSTEM);

        Language language1 = new Language();
        language1.setId(1L);
        language1.setName("language name1");
        language1.setDefault(false);
        language1.setSystemDefault(true);
        language1.setIconUrl("url");
        language1.setCreated(new DateTime());
        language1.setUpdated(new DateTime());
        language1.setCode("code1");

        LanguageValue languageValue1 = new LanguageValue();
        languageValue1.setLanguage(language1);
        languageValue1.setLanguageKey(languageKey);
        languageValue1.setValue("value1");

        Language language2 = new Language();
        language2.setId(2L);
        language2.setName("language name2");
        language2.setDefault(false);
        language2.setSystemDefault(true);
        language2.setIconUrl("url");
        language2.setCreated(new DateTime());
        language2.setUpdated(new DateTime());
        language2.setCode("code2");

        LanguageValue languageValue2 = new LanguageValue();
        languageValue2.setLanguage(language2);
        languageValue2.setLanguageKey(languageKey);
        languageValue2.setValue("value1");

        Language language3 = new Language();
        language3.setId(3L);
        language3.setName("language name3");
        language3.setDefault(false);
        language3.setSystemDefault(true);
        language3.setIconUrl("url");
        language3.setCreated(new DateTime());
        language3.setUpdated(new DateTime());
        language3.setCode("code3");

        LanguageValue languageValue3 = new LanguageValue();
        languageValue3.setLanguage(language3);
        languageValue3.setLanguageKey(languageKey);
        languageValue3.setValue("value3");

        final Set<LanguageValue> languageValueSet = new HashSet<>();

        languageValueSet.add(languageValue1);
        languageValueSet.add(languageValue2);
        languageValueSet.add(languageValue3);

        languageKey.setLanguageValues(languageValueSet);

        final Long translationNumber = 1L;
        final String translationNumberString = "1";
        final Map<String, String> translatableFilterValues = new HashMap<>();
        translatableFilterValues.put("code1", "value1");

        when(systemTranslationRepository.findByDeletedIsNullAndContentId(translationNumber)).thenReturn(systemTranslationList);
        when(languageKeyRepository.findByEntityNameAndEntityIdAndEntityFieldAndLanguageKeyTypeAndDeletedIsNull(systemTranslation.getClass().getSimpleName().toLowerCase(), entityId, "name", LanguageKeyType.SYSTEM)).thenReturn(languageKey);

        List<SystemTranslationDTO> resultList = translationService.getAllSystemTranslations(translationNumberString, translatableFilterValues);

        verify(systemTranslationRepository, times(1)).findByDeletedIsNullAndContentId(translationNumber);

        assertEquals(systemTranslationList.size(), resultList.size());
    }

    @Test
    public void testGetAllSystemTranslationsWithoutFilters() {

        final List<SystemTranslation> systemTranslationList = new ArrayList<>();

        final String translationNumberString = "";
        final Map<String, String> translatableFilterValues = new HashMap<>();

        when(systemTranslationRepository.findByDeletedIsNull()).thenReturn(systemTranslationList);

        List<SystemTranslationDTO> resultList = translationService.getAllSystemTranslations(translationNumberString, translatableFilterValues);

        verify(systemTranslationRepository, times(1)).findByDeletedIsNull();

        assertEquals(systemTranslationList.size(), resultList.size());
    }

    @Test
    public void testGetSystemTranslationById() {

        long id = 1L;

        SystemTranslation systemTranslation = new SystemTranslation();
        systemTranslation.setId(id);
        systemTranslation.setCreated(new DateTime());

        when(systemTranslationRepository.findByIdAndDeletedIsNull(id)).thenReturn(systemTranslation);

        SystemTranslation result = translationService.getSystemTranslationById(id);

        verify(systemTranslationRepository, times(1)).findByIdAndDeletedIsNull(id);

        assertEquals(systemTranslation.getId(), result.getId());
        assertNull(result.getDeleted());
    }

    @Test
    public void testGetSystemTranslationsForLanguage() throws Exception {

        String languageCode = "EN";

        Object[] objects = new Object[]{"string", BigInteger.TEN};

        List<Object[]> systemTranslationsList = new ArrayList<>();
        systemTranslationsList.add(objects);
        systemTranslationsList.add(objects);

        when(systemTranslationRepository.getSystemTranslationsForLanguage(languageCode)).thenReturn(systemTranslationsList);

        translationService.getSystemTranslationsByLanguageCode(languageCode);

        verify(systemTranslationRepository, times(1)).getSystemTranslationsForLanguage(languageCode);
    }

    @Test
    public void testGetSystemDefaultLanguage() throws Exception {

        Language l = new Language();

        when(languageRepository.findByIsSystemDefaultAndDeletedIsNull(true)).thenReturn(l);

        translationService.getSystemDefaultLanguage();

        verify(languageRepository, times(1)).findByIsSystemDefaultAndDeletedIsNull(true);
    }

}
