package com.sflpro.rate.services.region.impl;

import com.sflpro.rate.models.dto.Region;
import com.sflpro.rate.repository.region.RegionRepository;
import com.sflpro.rate.services.region.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/20/14
 * Time: 1:10 AM
 */
@Service
public class RegionServiceImpl implements RegionService {

    @Autowired
    private RegionRepository regionRepository;

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    @Transactional(readOnly = true)
    public List<Region> findByDeletedIsNull() {
        return regionRepository.findByDeletedIsNull();
    }

}
