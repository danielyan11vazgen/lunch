package com.sflpro.rate.services.resource.impl;

import com.sflpro.rate.models.dto.Resource;
import com.sflpro.rate.models.util.ImageResize;
import com.sflpro.rate.repository.resource.ResourceManagerRepository;
import com.sflpro.rate.services.resource.ResourceManager;
import com.sflpro.rate.models.datatypes.ResourceType;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.io.*;
import java.util.UUID;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/3/14
 * Time: 10:53 AM
 */
@Service
public class ResourceManagerImpl implements ResourceManager {

    @Autowired
    private ResourceManagerRepository resourceManagerRepository;

    @Value("#{appProperties['upload.dir']}")
    private String uploadDir;

    private final Logger logger = LoggerFactory.getLogger(getClass());


    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public Resource saveDraftResource(@Nonnull InputStream inputStream, @Nonnull ResourceType type) throws IOException {

        File file = createTemporaryFile(inputStream);

        Resource resource = new Resource();
        resource.setType(type);
        resource.setCreated(new DateTime());
        resource.setPath(file.getPath());

        return resource;
    }


    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public File createTemporaryFile(@Nonnull InputStream inputStream) throws IOException {
        File tempFile;

        tempFile = File.createTempFile(generateRandomUUID(), ".tmp");
        tempFile.deleteOnExit();

        OutputStream outputStream = new FileOutputStream(tempFile);

        IOUtils.copy(inputStream, outputStream);

        outputStream.close();
        inputStream.close();

        return tempFile;
    }


    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public Resource finalizeWorkingWithResource(@Nonnull File file, @Nonnull ResourceType resourceType, String fileName) throws IOException {

        String baseFileName = FilenameUtils.getBaseName(fileName);
        String fileExtension = FilenameUtils.getExtension(fileName);

        if (baseFileName != null) {
            baseFileName = baseFileName.replace(" ", "_");
        }

        final String uploadInternalPath = createUploadFolderByResourceType(resourceType, baseFileName, fileExtension);
        final String absolutePath = getAbsoluteResourcePath(uploadInternalPath);

        writeFileIntoNewFile(file, absolutePath);

        if (resourceType.equals(ResourceType.LOGO)) {
            handleRealizedImagesCreation(file, uploadInternalPath);
        }

        Resource resource = new Resource();
        //save relative path
        resource.setPath(uploadInternalPath);
        resource.setType(resourceType);
        resource.setCreated(new DateTime());

        return resourceManagerRepository.save(resource);
    }


    /**
     * {@inheritDoc}
     */
    @Nonnull
    public String toAbsolutePath(@Nonnull Resource resource) {
        return getAbsoluteResourcePath(resource.getPath());
    }


    /**
     * {@inheritDoc}
     */
    @Nonnull
    public String toAbsolutePath(@Nonnull String path) {
        return getAbsoluteResourcePath(path);
    }

    /**
     * Set upload directory
     *
     * @param uploadDir upload directory
     */
    public void setUploadDir(String uploadDir) {
        this.uploadDir = uploadDir;
    }


    /**
     * Save file into uploads directory
     *
     * @param sourceFile          source file to save
     * @param destinationFilePath is destination path where source file must be saved
     * @return saved file
     */
    private File writeFileIntoNewFile(File sourceFile, String destinationFilePath) throws IOException {

        File destination = null;
        OutputStream outputStream = null;

        try {

            destination = new File(destinationFilePath);
            outputStream = new FileOutputStream(destinationFilePath);

            FileUtils.copyFile(sourceFile, outputStream);

        } finally {
            IOUtils.closeQuietly(outputStream);
        }

        return destination;
    }


    /**
     * @param resourceType  must be only DATA_FILE or REFERENCE_FILE
     * @param parameterType must be TaxType
     * @return Relative resource path in the resource repository
     */
    private String createUploadFolderByResourceType(ResourceType resourceType, String parameterType, String fileExtension) {

        String relativeFileName = generateInternalResourcePath(resourceType, parameterType, fileExtension);

        File uploadDirectory = new File(File.separator + FilenameUtils.getFullPathNoEndSeparator(getAbsoluteResourcePath(relativeFileName)));

        if (!uploadDirectory.exists()) {
            boolean b = uploadDirectory.mkdirs();
            if (!b) {
                logger.info("Upload directory already exists. Can't create new Upload directory.");
            }
        }

        return relativeFileName;
    }


    /**
     * Generates the file path relative to the storage repository root
     *
     * @param resourceType resource type
     * @param fileName     file name which must be created
     * @param extension    file extension
     * @return internal resource path
     */
    private String generateInternalResourcePath(ResourceType resourceType, String fileName, String extension) {

        if (fileName == null) {
            fileName = "";
        }

        return String.valueOf(resourceType) + "/" + new DateTime().toString("dd_MM_yy") + "/" + fileName + "_" + generateRandomUUID() + "." + extension;
    }


    /**
     * @return random UUID
     */
    private String generateRandomUUID() {
        return UUID.randomUUID().toString();
    }


    /**
     * Returns the absolute path of an internal resource path
     *
     * @param relativeResourcePath relative resource path
     * @return uploadDir + "/" + relativeResourcePath
     */
    private String getAbsoluteResourcePath(String relativeResourcePath) {
        return uploadDir + "/" + relativeResourcePath;
    }

    /**
     * Resize original image to fir front end sizes
     *
     * @param file         Original file
     * @param baseFilePath base file path
     */
    private void handleRealizedImagesCreation(File file, String baseFilePath) throws IOException {
        final String relativeFileNameForResized = ImageResize.getResizedImagePath(baseFilePath, ImageResize.IMG_WIDTH, ImageResize.IMG_HEIGHT);
        final String absolutePathForResized = getAbsoluteResourcePath(relativeFileNameForResized);

        ImageResize.createImageResized(file, absolutePathForResized);

        final String relativeFileNameForThumbnail = ImageResize.getResizedImagePath(baseFilePath, ImageResize.THUMB_WIDTH, ImageResize.THUMB_HEIGHT);
        final String absolutePathForThumbnail = getAbsoluteResourcePath(relativeFileNameForThumbnail);

        ImageResize.createThumbnail(file, absolutePathForThumbnail);

        final String relativeFileNameForAdminThumbnail = ImageResize.getResizedImagePath(baseFilePath, ImageResize.ADMIN_THUMB_WIDTH, ImageResize.ADMIN_THUMB_HEIGHT);
        final String absolutePathForAdminThumbnail = getAbsoluteResourcePath(relativeFileNameForAdminThumbnail);

        ImageResize.createAdminThumbnail(file, absolutePathForAdminThumbnail);
    }

}
