package com.sflpro.rate.services.branch.impl;

import com.sflpro.rate.models.datatypes.OrganizationStatus;
import com.sflpro.rate.models.dto.Branch;
import com.sflpro.rate.repository.branch.BranchRepository;
import com.sflpro.rate.services.branch.BranchService;
import com.sflpro.rate.services.exception.BranchAlreadyExistException;
import com.sflpro.rate.services.organization.OrganizationService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Iterator;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/20/14
 * Time: 3:40 PM
 */
@Service
public class BranchServiceImpl implements BranchService {

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private OrganizationService organizationService;

    @PersistenceContext
    private EntityManager em;


    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    @Transactional(readOnly = true)
    public Branch findBranchById(long branchId) {
        return branchRepository.findByIdAndDeletedIsNull(branchId);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Branch findHeadOffice(long organizationId) {
        return branchRepository.findByOrganization_IdAndIsHeadAndDeletedIsNull(organizationId, true);
    }


    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Branch createBranch(@Nonnull Branch branch) throws BranchAlreadyExistException {

        Branch b = branchRepository.findByNameAndOrganizationAndDeletedIsNotNull(branch.getName(), branch.getOrganization());

        if (b != null) {
            throw new BranchAlreadyExistException("'" + b.getOrganization().getName() + "' organization with '" + b.getName() + "' branch already exist.");
        }

        removeHeadOfficeWhenCreateOtherOne(branch);

        branch.setCreated(new DateTime());
        branch.setDeleted(null);

        return branchRepository.save(branch);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void removeHeadOfficeWhenCreateOtherOne(@Nonnull Branch branch) {
        if (branch.getIsHead()) {
            Branch headBranch = findHeadOffice(branch.getOrganization().getId());

            if (headBranch != null) {
                headBranch.setIsHead(false);
                em.merge(headBranch);
            }
        }
    }


    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Branch createOrUpdateBranch(@Nonnull Branch branch) throws BranchAlreadyExistException {

        Branch b = branchRepository.findByIdAndDeletedIsNull(branch.getId());

        if (b == null) {
            b = new Branch();
            b.setName(branch.getName());
            b.setLogoUrl(branch.getLogoUrl());
            b.setLogo(branch.getLogo());
            b.setIsHead(branch.getIsHead());
            b.setRegion(branch.getRegion());
            b.setCity(branch.getCity());
            b.setDistrict(branch.getDistrict());
            b.setZipCode(branch.getZipCode());
            b.setPhoneNumber1(branch.getPhoneNumber1());
            b.setPhoneNumber2(branch.getPhoneNumber2());
            b.setEmail(branch.getEmail());
            b.setIsFullDay(branch.getIsFullDay());
            b.setStatus(branch.getStatus());
            b.setLongitude(branch.getLongitude());
            b.setLatitude(branch.getLatitude());
            b.setOrganization(organizationService.findById(branch.getOrganization().getId()));

            b = createBranch(b);
        }

        removeHeadOfficeWhenCreateOtherOne(branch);

        if (b.getName() == null || !b.getName().equals(branch.getName())) {
            b.setName(branch.getName());
        }

        if (b.getLogo() == null || !b.getLogo().equals(branch.getLogo())) {
            b.setLogo(branch.getLogo());
        }

        if (b.getLogoUrl() == null || !b.getLogoUrl().equals(branch.getLogoUrl())) {
            b.setLogoUrl(branch.getLogoUrl());
        }

        if (b.getIsHead() == null || !b.getIsHead().equals(branch.getIsHead())) {
            b.setIsHead(branch.getIsHead());
        }

        if (b.getRegion() == null || !b.getRegion().equals(branch.getRegion())) {
            b.setRegion(branch.getRegion());
        }

        if (b.getCity() == null || !b.getCity().equals(branch.getCity())) {
            b.setCity(branch.getCity());
        }

        if (b.getDistrict() == null || !b.getDistrict().equals(branch.getDistrict())) {
            b.setDistrict(branch.getDistrict());
        }

        if (b.getZipCode() == null || !b.getZipCode().equals(branch.getZipCode())) {
            b.setZipCode(branch.getZipCode());
        }

        if (b.getPhoneNumber1() == null || !b.getPhoneNumber1().equals(branch.getPhoneNumber1())) {
            b.setPhoneNumber1(branch.getPhoneNumber1());
        }

        if (b.getPhoneNumber2() == null || !b.getPhoneNumber2().equals(branch.getPhoneNumber2())) {
            b.setPhoneNumber2(branch.getPhoneNumber2());
        }

        if (b.getEmail() == null || !b.getEmail().equals(branch.getEmail())) {
            b.setEmail(branch.getEmail());
        }

        if (b.getIsFullDay() == null || !b.getIsFullDay().equals(branch.getIsFullDay())) {
            b.setIsFullDay(branch.getIsFullDay());
        }

        if (b.getStatus() == null || !b.getStatus().equals(branch.getStatus())) {
            b.setStatus(branch.getStatus());
        }

        if (b.getLongitude() == null || !b.getLongitude().equals(branch.getLongitude())) {
            b.setLongitude(branch.getLongitude());
        }

        if (b.getLatitude() == null || !b.getLatitude().equals(branch.getLatitude())) {
            b.setLatitude(branch.getLatitude());
        }

        if (b.getOrganization() == null || !b.getOrganization().equals(branch.getOrganization())) {
            b.setOrganization(branch.getOrganization());
        }

        return em.merge(b);
    }


    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    @Transactional(readOnly = true)
    public Iterator<Branch> getAllOrganizationBranchesByFilter(long organizationId, long start, long count, String name, String region, String city, String district, String address, OrganizationStatus status) {

        String q =
                "select \n" +
                        "   b \n"+
                        "from \n"+
                        "   Branch b \n"+
                        "   LEFT JOIN b.region r \n"+
                        "   LEFT JOIN b.city c \n"+
                        "   LEFT JOIN b.district d \n"+
                        "where \n"+
                        "   b.organization.id = :organizationId and \n"+
                        "   upper(b.name) like upper(concat( '%', :name, '%')) and \n"+
                        "   ((b.address is null and :address ='') or upper(b.address) like upper(concat( '%', :address, '%'))) and \n"+
                        "   upper(r.name) like upper(concat( '%', :region, '%')) and \n"+
                        "   ((c.name is null and :city = '') or upper(c.name) like upper(concat( '%', :city, '%'))) and \n"+
                        "   ((d.name is null and :district ='') or upper(d.name) like upper(concat( '%', :district, '%'))) and \n"+
                        "   ((:status = '') OR (UPPER(b.status) = UPPER(:status))) \n"+
                        "ORDER BY b.created DESC";

        TypedQuery<Branch> qq = em.createQuery(q, Branch.class);
        qq.setParameter("organizationId", organizationId);
        qq.setParameter("name", name != null ? name : "");
        qq.setParameter("address", address != null ? address : "");
        qq.setParameter("region", region != null ? region : "");
        qq.setParameter("city", city != null ? city : "");
        qq.setParameter("district", district != null ? district : "");
        qq.setParameter("status", status != OrganizationStatus.ALL ? status.name() : "");

        qq.setFirstResult((int) start);
        qq.setMaxResults((int) count);

        return qq.getResultList().iterator();
    }


    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public long count(long organizationId, String name, String region, String city, String district, String address, OrganizationStatus status) {
        return branchRepository.findCount(organizationId, name, region, city, district, address, status != OrganizationStatus.ALL ? status.name() : "");
    }

}
