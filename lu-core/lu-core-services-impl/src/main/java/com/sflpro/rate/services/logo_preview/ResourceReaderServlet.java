package com.sflpro.rate.services.logo_preview;

import com.sflpro.rate.services.resource.ResourceManager;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLDecoder;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/7/14
 * Time: 7:23 PM
 */
public class ResourceReaderServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        WebApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
        ResourceManager resourceManager = applicationContext.getBean(ResourceManager.class);

        String filePath = resourceManager.toAbsolutePath(URLDecoder.decode(request.getRequestURI(), "UTF-8").replace("/resource/", ""));

        File file = new File(filePath);
        String fileName = file.getName();

        ServletOutputStream outStream = response.getOutputStream();
        response.setContentLength((int) file.length());
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

        int BUFFER_SIZE = 1024;

        byte[] byteBuffer = new byte[BUFFER_SIZE];
        DataInputStream in = new DataInputStream(new FileInputStream(file));


        int length;
        while ((length = in.read(byteBuffer)) != -1) {
            outStream.write(byteBuffer, 0, length);
        }

        in.close();
        outStream.close();

    }
}
