package com.sflpro.rate.services.city.impl;

import com.sflpro.rate.models.dto.City;
import com.sflpro.rate.models.dto.Region;
import com.sflpro.rate.repository.city.CityRepository;
import com.sflpro.rate.services.city.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/20/14
 * Time: 3:19 AM
 */
@Service
public class CityServiceImpl implements CityService {

    @Autowired
    private CityRepository cityRepository;


    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    @Transactional(readOnly = true)
    public List<City> findCitiesByDeletedIsNull() {
        return cityRepository.findByDeletedIsNull(new Sort(Sort.Direction.ASC, "name"));
    }



    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    @Transactional(readOnly = true)
    public List<City> findCitiesByRegionAndDeletedIsNull(@Nonnull Region region) {
        return cityRepository.findByRegionAndDeletedIsNull(region);
    }
}
