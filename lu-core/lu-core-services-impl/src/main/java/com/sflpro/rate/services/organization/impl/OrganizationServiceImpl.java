package com.sflpro.rate.services.organization.impl;

import com.sflpro.rate.models.datatypes.OrganizationStatus;
import com.sflpro.rate.models.datatypes.OrganizationType;
import com.sflpro.rate.models.dto.Organization;
import com.sflpro.rate.repository.organization.OrganizationRepository;
import com.sflpro.rate.services.exception.OrganizationExistException;
import com.sflpro.rate.services.organization.OrganizationService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Iterator;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/27/14
 * Time: 6:42 PM
 */
@Service
public class OrganizationServiceImpl implements OrganizationService {

    @Autowired
    private OrganizationRepository organizationRepository;

    @PersistenceContext
    private EntityManager em;


    /**
     * {@inheritDoc}
     */
    @Override
    public Organization findById(long organizationId) {
        return organizationRepository.findById(organizationId);
    }


    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Organization create(@Nonnull Organization organization) throws OrganizationExistException {

        Organization o = organizationRepository.findByNameAndDeletedIsNull(organization.getName());

        if (o != null) {
            throw new OrganizationExistException("Organization with '" + o.getName() + "' already exist!");
        }


        organization.setCreated(new DateTime());
        organization.setDeleted(null);

        organizationRepository.save(organization);

        return organization;
    }


    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Organization createOrUpdate(@Nonnull Organization organization) throws OrganizationExistException {

        Organization o;

        if (organization.getId() != null) {
            o = findById(organization.getId());
        } else {
            o = organizationRepository.findByNameAndDeletedIsNotNull(organization.getName());
        }

        if (o == null) {
            o = new Organization();
            o.setName(organization.getName());
            o.setLicense(organization.getLicense());
            o.setCentralBankDecision(organization.getCentralBankDecision());
            o.setUrl(organization.getUrl());
            o.setLogoUrl(organization.getLogoUrl());
            o.setStatus(organization.getStatus());
            o.setIssuedOn(organization.getIssuedOn());

            o = create(o);
        }

        if (o.getName() == null || organization.getName() != null) {
            o.setName(organization.getName());
        }

        if (o.getType() == null || organization.getType() != null) {
            o.setType(organization.getType());
        }

        if (o.getLicense() == null || organization.getLicense() != null) {
            o.setLicense(organization.getLicense());
        }

        if (o.getUrl() == null || organization.getUrl() != null) {
            o.setUrl(organization.getUrl());
        }

        if (o.getLogoUrl() == null || organization.getLogoUrl() != null) {
            o.setLogoUrl(organization.getLogoUrl());
        }

        if (o.getLogo() == null || organization.getLogo() != null) {
            o.setLogo(organization.getLogo());
        }

        if (o.getStatus() == null || organization.getStatus() != null) {
            o.setStatus(organization.getStatus());
        }

        if (o.getIssuedOn() == null || organization.getIssuedOn() != null) {
            o.setIssuedOn(organization.getIssuedOn());
        }

        return em.merge(o);
    }


    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Iterator<Organization> getAllOrganizationsByFilter(long start, long count, String name, Integer branches, OrganizationStatus status, OrganizationType organizationType) {
        String query =
                "SELECT \n" +
                        "   o \n" +
                        "FROM \n" +
                        "   Organization o \n" +
                        "WHERE \n" +
                        "   o.type = :organizationType and \n"+
                        "   (UPPER(o.name) LIKE UPPER(CONCAT('%', :name, '%'))) AND \n" +
                        "   ((:branches <> -1 AND o.branches.size = :branches) OR (:branches = -1 )) AND \n" +
                        "   ((:status = '') OR (UPPER(o.status) = UPPER(:status))) " +
                        "ORDER BY o.created DESC";

        TypedQuery<Organization> qq = em.createQuery(query, Organization.class);
        qq.setParameter("organizationType", organizationType);
        qq.setParameter("name", name != null ? name : "");
        qq.setParameter("branches", branches != null ? branches : -1);
        qq.setParameter("status", status != OrganizationStatus.ALL ? status.name() : "");

        qq.setFirstResult((int) start);
        qq.setMaxResults((int) count);

        return qq.getResultList().iterator();
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public long count(String name, Integer branches, OrganizationStatus status, OrganizationType organizationType) {
        return organizationRepository.findCount(name, branches, status != OrganizationStatus.ALL ? status.name() : "", organizationType);
    }


}
