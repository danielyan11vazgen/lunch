package com.sflpro.rate.services.translation.impl;

import com.sflpro.rate.admin.ui.dto.SystemTranslationDTO;
import com.sflpro.rate.admin.ui.dto.TranslationDTO;
import com.sflpro.rate.models.datatypes.LanguageKeyType;
import com.sflpro.rate.models.dto.Language;
import com.sflpro.rate.models.dto.LanguageKey;
import com.sflpro.rate.models.dto.LanguageValue;
import com.sflpro.rate.models.dto.SystemTranslation;
import com.sflpro.rate.repository.system_translation.SystemTranslationRepository;
import com.sflpro.rate.repository.language.LanguageRepository;
import com.sflpro.rate.repository.language_key.LanguageKeyRepository;
import com.sflpro.rate.repository.language_value.LanguageValueRepository;
import com.sflpro.rate.services.translation.TranslationService;
import com.sflpro.rate.services.util.SystemTranslationFields;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;
import java.util.*;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/27/14
 * Time: 6:58 PM
 */
@Service
public class TranslationServiceImpl implements TranslationService {

    @Autowired
    private LanguageRepository languageRepository;

    @Autowired
    private LanguageValueRepository languageValueRepository;

    @Autowired
    private LanguageKeyRepository languageKeyRepository;

    @Autowired
    private SystemTranslationRepository systemTranslationRepository;

    @PersistenceContext
    private EntityManager em;


    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    @Transactional(readOnly = true)
    public List<Language> getAllLanguages() {
        return languageRepository.findByDeletedIsNull(new Sort(Sort.Direction.DESC, "isDefault"));
    }


    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public Language findLanguageById(long id) {
        return languageRepository.findByIdAndDeletedIsNull(id);
    }


    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    @Transactional(propagation = Propagation.REQUIRED, noRollbackFor = Exception.class)
    public LanguageKey createLanguageKey(@Nonnull LanguageKey languageKey) {
        return languageKeyRepository.save(languageKey);
    }


    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    @Transactional(propagation = Propagation.REQUIRED, noRollbackFor = Exception.class)
    public LanguageKey createOrUpdateLanguageKey(@Nonnull String entityName, @Nonnull Long entityId, @Nonnull String entityField, @Nonnull LanguageKeyType languageKeyType) {

        LanguageKey key = languageKeyRepository.findByEntityNameAndEntityIdAndEntityFieldAndLanguageKeyTypeAndDeletedIsNull(entityName, entityId, entityField, languageKeyType);

        if (key == null) {
            key = new LanguageKey();
            key.setEntityName(entityName);
            key.setEntityId(entityId);
            key.setEntityField(entityField);
            key.setLanguageKeyType(languageKeyType);
            key.setCreated(new DateTime());

            key = createLanguageKey(key);
        }

        if (!key.getEntityName().equals(entityName)) {
            key.setEntityName(entityName);
        }

        if (!key.getEntityId().equals(entityId)) {
            key.setEntityId(entityId);
        }

        if (!key.getEntityField().equals(entityField)) {
            key.setEntityField(entityField);
        }

        if (!key.getLanguageKeyType().equals(languageKeyType)) {
            key.setLanguageKeyType(languageKeyType);
        }

        return em.merge(key);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, noRollbackFor = Exception.class)
    public void createOrUpdateTranslation(@Nonnull List<TranslationDTO> translationDTOList, @Nonnull Long organizationId, @Nonnull String entityName, @Nonnull String fieldName) {

        LanguageKey languageKey = createOrUpdateLanguageKey(entityName, organizationId, fieldName, LanguageKeyType.USER_GENERATED);

        for (TranslationDTO translationDTO : translationDTOList) {

            Language language = findLanguageById(translationDTO.getLanguageId());

            createOrUpdateLanguageValue(languageKey, language, translationDTO.getFieldValue());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, noRollbackFor = Exception.class)
    public void createOrUpdateSystemTranslation(@Nonnull List<TranslationDTO> translationDTOList, @Nonnull Long entityId, @Nonnull String entityName, @Nonnull String fieldName) {

        LanguageKey languageKey = createOrUpdateLanguageKey(entityName, entityId, fieldName, LanguageKeyType.SYSTEM);

        for (TranslationDTO translationDTO : translationDTOList) {

            Language language = findLanguageById(translationDTO.getLanguageId());

            createOrUpdateLanguageValue(languageKey, language, translationDTO.getFieldValue());
        }
    }


    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    @Transactional(propagation = Propagation.REQUIRED, noRollbackFor = Exception.class)
    public LanguageKey getEntityTranslatableValuesByField(String entityName, long entityId, String entityField, LanguageKeyType languageKeyType) {
        return languageKeyRepository.findByEntityNameAndEntityIdAndEntityFieldAndLanguageKeyTypeAndDeletedIsNull(entityName, entityId, entityField, languageKeyType);
    }


    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    @Transactional(propagation = Propagation.REQUIRED, noRollbackFor = Exception.class)
    public LanguageValue createLanguageValue(@Nonnull LanguageValue languageValue) {
        return languageValueRepository.save(languageValue);
    }


    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    @Transactional(propagation = Propagation.REQUIRED, noRollbackFor = Exception.class)
    public LanguageValue createOrUpdateLanguageValue(@Nonnull LanguageKey languageKey, @Nonnull Language language, @Nonnull String value) {

        LanguageValue languageValue = languageValueRepository.findByLanguageKey_IdAndLanguage_Id(languageKey.getId(), language.getId());

        if (languageValue == null) {
            languageValue = new LanguageValue();
            languageValue.setLanguage(language);
            languageValue.setLanguageKey(languageKey);
            languageValue.setValue(value);

            createLanguageValue(languageValue);
        }

        if (!languageValue.getValue().equals(value)) {
            languageValue.setValue(value);
        }

        return em.merge(languageValue);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public List<SystemTranslationDTO> getAllSystemTranslations(String translationNumber, Map<String, String> translatableFilterValues) {

        final List<SystemTranslation> systemTranslationList;

        if (translationNumber != null && !translationNumber.isEmpty() && translationNumber.matches("-?\\d+(\\.\\d+)?")) {

            final Long filterNumber = Long.parseLong(translationNumber);

            systemTranslationList = systemTranslationRepository.findByDeletedIsNullAndContentId(filterNumber);
        } else {
            systemTranslationList = systemTranslationRepository.findByDeletedIsNull();
        }

        final List<SystemTranslationDTO> systemTranslationDTOList = new ArrayList<>(systemTranslationList.size());

        for (SystemTranslation systemTranslation : systemTranslationList) {

            final SystemTranslationDTO systemTranslationDTO = new SystemTranslationDTO();
            boolean filteredOut = false;

            systemTranslationDTO.setContentId(systemTranslation.getContentId());

            final LanguageKey languageKey = getEntityTranslatableValuesByField(systemTranslation.getClass().getSimpleName().toLowerCase(), systemTranslation.getId(), "name", LanguageKeyType.SYSTEM);

            if (languageKey.getLanguageValues() != null) {

                for (LanguageValue languageValue : languageKey.getLanguageValues()) {

                    if (translatableFilterValues != null && !translatableFilterValues.isEmpty()) {

                        final String langCode = languageValue.getLanguage().getCode();
                        final String translatableValue = translatableFilterValues.get(langCode);
                        if (translatableFilterValues.containsKey(langCode) && !languageValue.getValue().toUpperCase().contains(translatableValue.toUpperCase()) && !translatableValue.isEmpty()) {
                            filteredOut = true;
                            break;
                        }
                    }
                    systemTranslationDTO.addTranslationValue(languageValue.getLanguage().getCode(), languageValue.getValue());
                }
            }
            if (!filteredOut) {
                systemTranslationDTOList.add(systemTranslationDTO);
            }

        }

        return systemTranslationDTOList;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public SystemTranslation getSystemTranslationById(long systemTranslationId) {
        return systemTranslationRepository.findByIdAndDeletedIsNull(systemTranslationId);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Map<BigInteger, String> getSystemTranslationsByLanguageCode(String languageCode) {

        Assert.notNull(languageCode);

        final List systemTranslationsList = systemTranslationRepository.getSystemTranslationsForLanguage(languageCode);

        final Iterator resultListIterator = systemTranslationsList.iterator();

        final Map<BigInteger, String> systemTranslationsMap = new HashMap<>(systemTranslationsList.size());

        while (resultListIterator.hasNext()) {
            final Object[] systemTranslationRow = (Object[]) resultListIterator.next();

            final String translationValue = (String) systemTranslationRow[SystemTranslationFields.TRANSLATION_VALUE.getFieldNumber()];
            final BigInteger translationContentId = (BigInteger) systemTranslationRow[SystemTranslationFields.CONTENT_ID.getFieldNumber()];

            systemTranslationsMap.put(translationContentId, translationValue);
        }

        return systemTranslationsMap;
    }


    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public Language getSystemDefaultLanguage() {
        return languageRepository.findByIsSystemDefaultAndDeletedIsNull(true);
    }

}
