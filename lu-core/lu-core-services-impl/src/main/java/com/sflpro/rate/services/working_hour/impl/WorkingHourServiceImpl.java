package com.sflpro.rate.services.working_hour.impl;

import com.sflpro.rate.models.dto.Branch;
import com.sflpro.rate.models.dto.WorkingHour;
import com.sflpro.rate.repository.working_hour.WorkingHourRepository;
import com.sflpro.rate.services.branch.BranchService;
import com.sflpro.rate.services.exception.WorkingHourExistException;
import com.sflpro.rate.services.working_hour.WorkingHourService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/20/14
 * Time: 3:43 PM
 */
@Service
public class WorkingHourServiceImpl implements WorkingHourService {

    @Autowired
    private WorkingHourRepository workingHourRepository;

    @Autowired
    private BranchService branchService;

    @PersistenceContext
    private EntityManager em;


    /**
     * {@inheritDoc}
     */
    @Override
    @Nonnull
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public WorkingHour createWorkingHour(@Nonnull WorkingHour workingHour, @Nonnull Long branchId) throws WorkingHourExistException {

        Branch b = branchService.findBranchById(branchId);

        WorkingHour w = workingHourRepository.findByCodeAndBranchAndDeletedIsNull(workingHour.getCode(), b);

        if (w != null) {
            throw new WorkingHourExistException("Working hour of " + workingHour.getBranch().getName() + " and " + workingHour.getCode().name() + " code already exist! ");
        }

        workingHour.setBranch(b);
        workingHour.setCreated(new DateTime());
        workingHour.setDeleted(null);

        return workingHourRepository.save(workingHour);
    }


    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public WorkingHour createOrUpdateWorkingHour(@Nonnull WorkingHour workingHour, @Nonnull Long branchId) throws WorkingHourExistException {

        WorkingHour w = workingHourRepository.findByCodeAndBranch_IdAndDeletedIsNull(workingHour.getCode(), branchId);

        if (w == null) {
            w = createWorkingHour(workingHour, branchId);
        }

        if (w.getStartTime() == null || !w.getStartTime().equals(workingHour.getStartTime())) {
            w.setStartTime(workingHour.getStartTime());
        }

        if (w.getEndTime() == null || !w.getEndTime().equals(workingHour.getEndTime())) {
            w.setEndTime(workingHour.getEndTime());
        }

        if (w.isWork() == null || !w.isWork().equals(workingHour.isWork())) {
            w.setWork(workingHour.isWork());
        }

        return em.merge(w);
    }


    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public List<WorkingHour> findWorkingHoursByBranch(@Nonnull Long branchId) {
        return workingHourRepository.findByBranch_IdAndDeletedIsNull(branchId);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void deleteWorkingHoursByBranch(@Nonnull Long branchId) {

        List<WorkingHour> workingHourList = findWorkingHoursByBranch(branchId);

        for (WorkingHour workingHour : workingHourList) {
            workingHour.setDeleted(new DateTime());

            em.merge(workingHour);
        }
    }

}
