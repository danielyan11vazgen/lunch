package com.sflpro.rate.services.district.impl;

import com.sflpro.rate.models.dto.District;
import com.sflpro.rate.repository.district.DistrictRepository;
import com.sflpro.rate.services.district.DistrictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/20/14
 * Time: 4:27 AM
 */
@Service
public class DistrictServiceImpl implements DistrictService {

    @Autowired
    private DistrictRepository districtRepository;


    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    @Transactional(readOnly = true)
    public List<District> findDistrictByDeletedIsNull() {
        return districtRepository.findByDeletedIsNull(new Sort(Sort.Direction.ASC, "name"));
    }
}
