package com.sflpro.rate.services.resource;

import com.sflpro.rate.models.datatypes.ResourceType;
import com.sflpro.rate.models.dto.Resource;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/3/14
 * Time: 10:49 AM
 */
public interface ResourceManager {

    /**
     * Save resource
     *
     * @param inputStream stream to save
     * @param type        resource type
     * @return saved resource instance
     */
    @Nonnull
    Resource saveDraftResource(@Nonnull InputStream inputStream, @Nonnull ResourceType type) throws IOException;


    /**
     * Create temp file and save stream into it
     *
     * @param inputStream stream to save
     * @return saved temp file instance
     */
    @Nonnull
    File createTemporaryFile(@Nonnull InputStream inputStream) throws IOException;


    /**
     * Finalize resource file saving
     *
     * @param file         temp file instance to save
     * @param resourceType must be only LOGO
     * @return saved resource
     */
    @Nonnull
    Resource finalizeWorkingWithResource(@Nonnull File file, @Nonnull ResourceType resourceType, String fileName) throws IOException;


    /**
     * Make resource file path absolute
     *
     * @param resource resource which path changes to absolute
     * @return changed resource
     */
    @Nonnull
    String toAbsolutePath(@Nonnull Resource resource);


    /**
     * Make path absolute
     *
     * @param path path which changes to absolute
     * @return changed file path
     */
    @Nonnull
    String toAbsolutePath(@Nonnull String path);

}
