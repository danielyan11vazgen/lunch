package com.sflpro.rate.services.exception;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/18/14
 * Time: 10:36 AM
 */
public class OrganizationExistException extends Exception {

    public OrganizationExistException(String message) {
        super(message);
    }
}
