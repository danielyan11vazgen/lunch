package com.sflpro.rate.services.organization;

import com.sflpro.rate.models.datatypes.OrganizationStatus;
import com.sflpro.rate.models.datatypes.OrganizationType;
import com.sflpro.rate.models.dto.Organization;
import com.sflpro.rate.services.exception.OrganizationExistException;

import javax.annotation.Nonnull;
import java.util.Iterator;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/27/14
 * Time: 6:41 PM
 */
public interface OrganizationService {

    /**
     * Find Organization by id
     *
     * @param organizationId organization id
     * @return found Organization
     */
    Organization findById(long organizationId);


    /**
     * Create new organization
     *
     * @param organization Organization entity
     * @return created Organization
     */
    @Nonnull
    Organization create(@Nonnull Organization organization) throws OrganizationExistException;


    /**
     * Create or update Organization
     *
     * @param organization Organization entity
     * @return updated Organization
     */
    @Nonnull
    Organization createOrUpdate(@Nonnull Organization organization) throws OrganizationExistException;


    /**
     * Get all organizations
     *
     * @param start result start index
     * @param count result count
     * @param name  organization name
     * @return iterator over getting result
     */
    @Nonnull
    Iterator<Organization> getAllOrganizationsByFilter(long start, long count, String name, Integer branches, OrganizationStatus status, OrganizationType organizationType);

    /**
     * Get count by search term
     *
     * @param name organization name
     * @return result count
     */
    long count(String name, Integer branches, OrganizationStatus status, OrganizationType organizationType);


}
