package com.sflpro.rate.services.exception;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/20/14
 * Time: 7:53 PM
 */
public class BranchAlreadyExistException extends Exception {

    public BranchAlreadyExistException(String message) {
        super(message);
    }
}
