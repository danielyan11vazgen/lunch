package com.sflpro.rate.services.district;

import com.sflpro.rate.models.dto.District;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/20/14
 * Time: 4:26 AM
 */
public interface DistrictService {

    /**
     * Get all not deleted cities
     *
     * @return list of cities
     */
    @Nonnull
    List<District> findDistrictByDeletedIsNull();


}
