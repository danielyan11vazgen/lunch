package com.sflpro.rate.services.working_hour;

import com.sflpro.rate.models.dto.WorkingHour;
import com.sflpro.rate.services.exception.WorkingHourExistException;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/20/14
 * Time: 3:43 PM
 */
public interface WorkingHourService {

    /**
     * Create working hour
     *
     * @param workingHour working hour
     * @return found working hour
     * @throws WorkingHourExistException when working hour exist
     */
    @Nonnull
    WorkingHour createWorkingHour(@Nonnull WorkingHour workingHour, @Nonnull Long branchId) throws WorkingHourExistException;


    /**
     * Create or update working hour
     *
     * @param workingHour working hour
     * @return found working hour
     */
    @Nonnull
    WorkingHour createOrUpdateWorkingHour(@Nonnull WorkingHour workingHour, @Nonnull Long branchId) throws WorkingHourExistException;


    /**
     * Find working hour by branch id
     *
     * @param branchId branch id
     * @return found working hours
     */
    @Nonnull
    List<WorkingHour> findWorkingHoursByBranch(@Nonnull Long branchId);


    /**
     * Delete working hours by branch
     *
     * @param branchId branch id
     */
    void deleteWorkingHoursByBranch(@Nonnull Long branchId);
}
