package com.sflpro.rate.services.branch;

import com.sflpro.rate.models.datatypes.OrganizationStatus;
import com.sflpro.rate.models.dto.Branch;
import com.sflpro.rate.services.exception.BranchAlreadyExistException;

import javax.annotation.Nonnull;
import java.util.Iterator;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/20/14
 * Time: 3:40 PM
 */
public interface BranchService {


    /**
     * Find Branch by id
     *
     * @param branchId branch id
     * @return found branch
     */
    @Nonnull
    Branch findBranchById(long branchId);


    /**
     * Find Head Office
     *
     * @return null if no head office found otherwise HeadOffice
     */
    Branch findHeadOffice(long organizationId);


    /**
     * Create Branch
     *
     * @param branch Branch object
     * @return created branch
     * @throws BranchAlreadyExistException when branch already exist
     */
    @Nonnull
    Branch createBranch(@Nonnull Branch branch) throws BranchAlreadyExistException;


    /**
     * Remove Head office when Create Other One
     *
     * @param branch new Branch
     */
    void removeHeadOfficeWhenCreateOtherOne(@Nonnull Branch branch);


    /**
     * Create or update Branch
     *
     * @param branch Branch object
     * @return created branch
     */
    @Nonnull
    Branch createOrUpdateBranch(@Nonnull Branch branch) throws BranchAlreadyExistException;


    /**
     * Get all branches for organization matching filter
     *
     * @param organizationId organization id
     * @param start          start param for pagination
     * @param count          number of items per page
     * @param name           name filter
     * @param region         region name filter
     * @param city           city name filter
     * @param district       district name filter
     * @param address        branch address
     * @param status         branch status filter
     * @return Iterator of branch list
     */
    @Nonnull
    Iterator<Branch> getAllOrganizationBranchesByFilter(long organizationId, long start, long count, String name, String region, String city, String district, String address, OrganizationStatus status);


    /**
     * Get count of all branches for organization matching filter
     *
     * @param organizationId organization id
     * @param name           name filter
     * @param region         region name filter
     * @param city           city name filter
     * @param district       district name filter
     * @param address        branch address
     * @param status         branch status filter
     * @return number of branches
     */
    long count(long organizationId, String name, String region, String city, String district, String address, OrganizationStatus status);

}
