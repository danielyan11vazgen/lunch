package com.sflpro.rate.services.city;

import com.sflpro.rate.models.dto.City;
import com.sflpro.rate.models.dto.Region;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/20/14
 * Time: 3:17 AM
 */
public interface CityService {


    /**
     * Get all not deleted cities
     *
     * @return list of cities
     */
    @Nonnull
    List<City> findCitiesByDeletedIsNull();


    /**
     * Find all not deleted cities by region
     *
     * @param region region
     * @return list of cities
     */
    @Nonnull
    List<City> findCitiesByRegionAndDeletedIsNull(@Nonnull Region region);

}
