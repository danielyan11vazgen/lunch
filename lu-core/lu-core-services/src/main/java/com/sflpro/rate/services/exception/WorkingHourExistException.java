package com.sflpro.rate.services.exception;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/20/14
 * Time: 8:07 PM
 */
public class WorkingHourExistException extends Exception {

    public WorkingHourExistException(String message) {
        super(message);
    }
}
