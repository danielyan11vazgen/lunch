package com.sflpro.rate.services.region;

import com.sflpro.rate.models.dto.Region;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 7/20/14
 * Time: 1:09 AM
 */
public interface RegionService {


    /**
     * Get all Regions
     *
     * @return List of region
     */
    @Nonnull
    List<Region> findByDeletedIsNull();


}
