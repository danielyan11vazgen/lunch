package com.sflpro.rate.services.translation;

import com.sflpro.rate.admin.ui.dto.SystemTranslationDTO;
import com.sflpro.rate.admin.ui.dto.TranslationDTO;
import com.sflpro.rate.models.datatypes.LanguageKeyType;
import com.sflpro.rate.models.dto.Language;
import com.sflpro.rate.models.dto.LanguageKey;
import com.sflpro.rate.models.dto.LanguageValue;
import com.sflpro.rate.models.dto.SystemTranslation;

import javax.annotation.Nonnull;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/27/14
 * Time: 6:57 PM
 */
public interface TranslationService {

    /**
     * Get all languages sorted by is_default column DESC
     *
     * @return list of Languages
     */
    @Nonnull
    List<Language> getAllLanguages();


    /**
     * Find Language by id and deleted is null
     *
     * @param id Language id
     * @return found Language
     */
    Language findLanguageById(long id);


    /**
     * Create new Language Key
     *
     * @param languageKey Language Key
     * @return created  Language Key
     */
    @Nonnull
    LanguageKey createLanguageKey(@Nonnull LanguageKey languageKey);


    /**
     * Create or Update Language key
     *
     * @param entityName      entity name
     * @param entityId        entity id
     * @param entityField     entity field
     * @param languageKeyType language key type
     * @return created or updated LanguageKey
     */
    @Nonnull
    LanguageKey createOrUpdateLanguageKey(@Nonnull String entityName, @Nonnull Long entityId, @Nonnull String entityField, @Nonnull LanguageKeyType languageKeyType);


    /**
     * Create new Translation
     *
     * @param translationDTOList translation dto list
     * @param organizationId     organization Id
     * @param entityName         entity name
     * @param fieldName          field name
     */
    void createOrUpdateTranslation(@Nonnull List<TranslationDTO> translationDTOList, @Nonnull Long organizationId, @Nonnull String entityName, @Nonnull String fieldName);

    /**
     * Create new Translation
     *
     * @param translationDTOList translation dto list
     * @param entityId           entity Id
     * @param entityName         entity name
     * @param fieldName          field name
     */
    void createOrUpdateSystemTranslation(@Nonnull List<TranslationDTO> translationDTOList, @Nonnull Long entityId, @Nonnull String entityName, @Nonnull String fieldName);


    /**
     * Get translation key
     *
     * @param entityName      entity name
     * @param entityId        entity id
     * @param field           field
     * @param languageKeyType language key type
     * @return language key
     */
    @Nonnull
    LanguageKey getEntityTranslatableValuesByField(String entityName, long entityId, String field, LanguageKeyType languageKeyType);


    /**
     * Create Language Value
     *
     * @param languageValue language value
     * @return created Language value
     */
    @Nonnull
    LanguageValue createLanguageValue(@Nonnull LanguageValue languageValue);


    /**
     * Create or update language value
     *
     * @param languageKey language key
     * @param language    language
     * @param value       value
     * @return created or updated language value
     */
    @Nonnull
    LanguageValue createOrUpdateLanguageValue(@Nonnull LanguageKey languageKey, @Nonnull Language language, @Nonnull String value);


    /**
     * Get list of all system translation values
     *
     * @return list of all system translations
     */
    List<SystemTranslationDTO> getAllSystemTranslations(String translationNumber, Map<String, String> translatableFilterValues);


    /**
     * Get system translation by unique is
     *
     * @param systemTranslationId system translation unique id
     * @return system translation entity
     */
    SystemTranslation getSystemTranslationById(long systemTranslationId);


    /**
     * Get system translations for language
     *
     * @param languageCode language code
     * @return map of system translations
     */
    Map<BigInteger, String> getSystemTranslationsByLanguageCode(String languageCode);


    /**
     * Get System default language
     *
     * @return Language
     */
    @Nonnull
    Language getSystemDefaultLanguage();
}
