package com.sflpro.rate.test.integration;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * User: Yervand Aghababyan
 * Company: SFL LLC
 * Date: 6/17/14
 * Time: 1:40 PM
 */
public class AbstractIntegrationTest extends AbstractTest {

    private final AtomicBoolean initialized = new AtomicBoolean(false);

    @Autowired
    protected ComboPooledDataSource dataSource;

    @Before
    public void init() throws IOException, DatabaseUnitException, SQLException, URISyntaxException {
        initDb(false);
    }

    protected void initDb(boolean force) throws SQLException, DatabaseUnitException, URISyntaxException, IOException {
        if (force || initialized.compareAndSet(false, true)) {

            logger.info("Initializing mock database!");

            ClassLoader cl = Thread.currentThread().getContextClassLoader();

            IDatabaseConnection dbUnitCon = getConnection();
            IDataSet organization = new FlatXmlDataSet(cl.getResource("testdata/organization.xml"), false);
            IDataSet language = new FlatXmlDataSet(cl.getResource("testdata/language.xml"), false);
            IDataSet languageKey = new FlatXmlDataSet(cl.getResource("testdata/language_key.xml"), false);
            IDataSet languageValue = new FlatXmlDataSet(cl.getResource("testdata/language_value.xml"), false);
            IDataSet systemTranslation = new FlatXmlDataSet(cl.getResource("testdata/system_translation.xml"), false);

            IDataSet region = new FlatXmlDataSet(cl.getResource("testdata/region.xml"), false);
            IDataSet city = new FlatXmlDataSet(cl.getResource("testdata/city.xml"), false);
            IDataSet district = new FlatXmlDataSet(cl.getResource("testdata/district.xml"), false);
            IDataSet branch = new FlatXmlDataSet(cl.getResource("testdata/branch.xml"), false);

            Statement statement = null;

            try {

                statement = dbUnitCon.getConnection().createStatement();

                statement.execute("SET DATABASE REFERENTIAL INTEGRITY FALSE");

                DatabaseOperation.REFRESH.execute(dbUnitCon, organization);
                DatabaseOperation.REFRESH.execute(dbUnitCon, language);
                DatabaseOperation.REFRESH.execute(dbUnitCon, languageKey);
                DatabaseOperation.REFRESH.execute(dbUnitCon, languageValue);
                DatabaseOperation.REFRESH.execute(dbUnitCon, systemTranslation);
                DatabaseOperation.REFRESH.execute(dbUnitCon, region);
                DatabaseOperation.REFRESH.execute(dbUnitCon, city);
                DatabaseOperation.REFRESH.execute(dbUnitCon, district);

                logger.info("Mock database successfully initialized!");

                statement.execute("SET DATABASE REFERENTIAL INTEGRITY TRUE");

            } finally {

                if (statement != null) {
                    statement.close();
                }

                DataSourceUtils.releaseConnection(dbUnitCon.getConnection(), dataSource);
            }
        }
    }

    protected IDatabaseConnection getConnection() {
        IDatabaseConnection connection = new DatabaseConnection(DataSourceUtils.getConnection(dataSource));

        DatabaseConfig config = connection.getConfig();
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                new DbUnitDataTypeFactory());

        return connection;
    }
}
