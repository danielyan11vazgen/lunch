package com.sflpro.rate.test.integration;

import java.io.File;

/**
 * User: Vazgen Danielyan
 * Company: SFL LLC
 * Date: 6/17/14
 * Time: 1:40 PM
 */
public class RateTestUtils {

    private RateTestUtils() {
    }

    public static String getUploadsDir() {
        return System.getProperty("java.io.tmpdir") + File.separator + "uploads" + File.separator;
    }
}
