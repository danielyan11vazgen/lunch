package com.sflpro.rate.test.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * User: Yervand Aghababyan
 * Company: SFL LLC
 * Date: 6/17/14
 * Time: 1:40 PM
 */
public abstract class AbstractTest {
    protected final Logger logger = LoggerFactory.getLogger(getClass());
}
